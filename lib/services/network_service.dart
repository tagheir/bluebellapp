import 'dart:io';
import 'package:bluebellapp/models/response_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class NetworkService {
  static BuildContext context;
  static Future<ResponseModel<dynamic>> post(
      {String endPoint, dynamic model, String authToken}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    if (authToken != null) {
      headers.putIfAbsent("Authorization", () => "Bearer " + authToken);
    }
    final url = endPoint;
    //print(headers.toString());
    //print(url);
    //print(model.toString());
    // ////print(model);
    try {
      //print("===========  BEFORE POST =====================");
      final response = await http.post(endPoint, body: model, headers: headers);
      //print("===========  AFTER POST =====================");
      //print(response.statusCode);
      // //print(response?.body);
      if (response.body != null) {
        // ////print(response.body.toString());
        ////print("Response body not null");
        return Future.value(ResponseModel(data: response.body));
      } else {
        //////print("====================some other error=================");

      }
    } on SocketException catch (e) {
      //print("==========================exception ========================");
      //////print(e.toString());
      //////print("==========================exception ========================");
      if (e.osError.errorCode == 7 ||
          e.osError.errorCode == 111 ||
          e.osError.errorCode == 113) {
        return (Future.value(ResponseModel(data: null, isServerError: true)));
      }
    } catch (e) {
      //print(e.toString());
    }

    ////////////print(response.body);
  }

  static Future<ResponseModel<dynamic>> get(
      {String endPoint, String authToken}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    // String timezone;
    // // Platform messages may fail, so we use a try/catch PlatformException.
    // try {
    //   timezone = await FlutterNativeTimezone.getLocalTimezone();
    // } catch (e) {
    //   //print(e);
    //   timezone = 'Failed to get the timezone.';
    // }
    // //print(timezone);
    final url = endPoint;
    if (authToken != null) {
      //print("Auth token ====--->>>" + authToken + ", URL => " + url);
      headers.putIfAbsent("Authorization", () => "Bearer " + authToken);
    }
    //print(url);
    ////////////print(headers);
    // HttpClient client = new HttpClient();
    // client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    try {
      final response = await http.get(url, headers: headers);
      // //print("------------------------response------------------------");
      // //print(response.statusCode);
      // //print(url);
      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON.
        if (response.body != null) {
          return Future.value(ResponseModel(data: response.body));
        }
      } else {
        //print(response.statusCode);
        //print(response.body);
        //print("--------------------------exception----------------------");
        return (Future.value(ResponseModel(isServerError: true)));
        // If that response was not OK, throw an error.
        //throw CustomException(response.body);
      }
    } on SocketException catch (e) {
      // CustomSnackBar(message: "Network Error",);
      //print("--------------------------socket exception----------------------");
      //print(e);
      if (e.osError.errorCode == 7 ||
          e.osError.errorCode == 111 ||
          e.osError.errorCode == 113) {
        return (Future.value(ResponseModel(isServerError: true)));
      }

      // BlocProvider.of<AuthenticationBloc>(context).add();
    } catch (e) {
      //print("--------------------------other exception----------------------");
      //print(e.toString());
      return (Future.value(ResponseModel(isServerError: true)));
    }
  }

  static getRaw({String endPoint, String authToken, bool isJson = true}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    final url = endPoint;
    if (authToken != null) {
      //print("Auth token ====>>>" + authToken + " url => " + url);
      headers.putIfAbsent("Authorization", () => "Bearer " + authToken);
    }
    ////print(headers.toString());
    ////print(url);
    final response = await http.get(url, headers: headers);
    ////print(response.statusCode);
    ////////print(response);
    return response;
  }

  static Future<ResponseModel<dynamic>> getUpdated(
      {String url, Map<String, String> headers}) async {
    return httpRequestHelper(() {
      ////print(url);
      return http.get(url, headers: headers);
    });
  }

  static Future<ResponseModel<dynamic>> postUpdated(
      {String url, dynamic model, Map<String, String> headers}) async {
    print(url);
    //print(model);
    return httpRequestHelper(() {
      return http.post(url, body: model, headers: headers);
    });
  }

  static Future<ResponseModel<dynamic>> httpRequestHelper(
      Future<Response> request()) async {
    try {
      final response = await request();
      return Future.value(response.getResponseModel());
    } on SocketException catch (e) {
      if (e.osError.errorCode == 7 ||
          e.osError.errorCode == 111 ||
          e.osError.errorCode == 113) {
        return (Future.value(
          ResponseModel(networkError: true),
        ));
      }
    } catch (e) {
      return (Future.value(ResponseModel(isServerError: true)));
    }
    return (Future.value(
      ResponseModel(networkError: true),
    ));
  }
}

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LocalStorageService {
  static var storage = FlutterSecureStorage();

  static Future<bool> save(String key, String value) async {
    if (storage == null) return false;
    try {
      await storage.write(key: key, value: value);
      return true;
    } catch (ex) {
      return false;
    }
  }

  static Future<String> get(String key) async {
    if (storage == null) return null;
    try {
      var value = await storage.read(key: key);
      return value;
    } catch (ex) {
      return null;
    }
  }

  static Future<void> delete(String key) async {
    if (storage == null) return;
    try {
      await storage.delete(key: key);
    } catch (ex) {}
  }

  static Future<void> clear() async {
    if (storage == null) return;
    try {
      await storage.deleteAll();
    } catch (ex) {}
  }
}

import 'package:bluebellapp/models/response_model.dart';
import 'package:bluebellapp/repos/app_repo.dart';
import 'package:bluebellapp/resources/constants/helper_constants/network_request_type.dart';
import 'package:bluebellapp/services/network_service.dart';
import 'package:flutter/material.dart';

class AuthorizeNetworkService {
  final AppRepo appRepo;

  AuthorizeNetworkService(this.appRepo);

  Future<ResponseModel<T>> process<T>({
    @required String endPoint,
    @required NetworkRequestType networkRequestType,
    dynamic model,
    bool tryRefreshToken = true,
    bool log = false,
    T Function(dynamic) parser,
  }) async {
    print(endPoint);
    var headers = getHeaders(networkRequestType);
    print(model);
    ResponseModel<dynamic> response;
    if (networkRequestType == null) return null;
    if (networkRequestType.isGet() || networkRequestType.isGetRaw()) {
      response =
          await NetworkService.getUpdated(url: endPoint, headers: headers);
    } else if (networkRequestType.isJson() || networkRequestType.isPostRaw()) {
      response = await NetworkService.postUpdated(
        url: endPoint,
        headers: headers,
        model: model,
      );
    }

    if (response == null) return null;

    //print("response.isAuthorizationTokenError || response.isAuthorizationError");
    //print(response.isAuthorizationTokenError || response.isAuthorizationError);

    T parsedData;
    if (networkRequestType.isGet() || networkRequestType.isPost()) {
      try {
        parsedData = parser(response.data);
      } catch (ex) {
        parsedData = null;
      }
    }
    var parsedResponse =
        ResponseModel.copyBase<dynamic, T>(response, parsedData);
    if ((response.isAuthorizationTokenError || response.isAuthorizationError) &&
        tryRefreshToken == true) {
      var refreshTokenStatus = await appRepo.getUserRepository().refreshToken();
      if (refreshTokenStatus != true) {
        return parsedResponse;
      }
      return process(
        endPoint: endPoint,
        networkRequestType: networkRequestType,
        model: model,
        tryRefreshToken: false,
      );
    } else {
      return parsedResponse;
    }
  }

  Future<bool> refreshToken() async {
    return appRepo.getUserRepository().refreshToken();
  }

  Map<String, String> getHeaders(NetworkRequestType networkRequestType) {
    Map<String, String> headers = {};
    if (networkRequestType == null) return headers;

    if (networkRequestType.isJson()) {
      headers.putIfAbsent('Content-Type', () => 'application/json');
      headers.putIfAbsent('Accept', () => 'application/json');
    }
    if (networkRequestType.isAuthorized()) {
      var token = appRepo.getToken();
      // //print("token ===============>");
      print(token.toString());
      headers.putIfAbsent("Authorization", () => "Bearer " + token);
    }
    return headers;
  }
}

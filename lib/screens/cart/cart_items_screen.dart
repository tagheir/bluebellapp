import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/shoppingCart_dto.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/attribute_control_type.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/cart/cart_item_card.dart';
import 'package:bluebellapp/screens/shared/_layout_screen.dart';
import 'package:bluebellapp/screens/widgets/fixed_bottom_button.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartItems extends StatefulWidget {
  @override
  _CartItemsState createState() => _CartItemsState();
}

class _CartItemsState extends State<CartItems> {
  ShoppingCartDto cart = ShoppingCartDto();
  CustomProgressDialog pr;
  bool isDataLoaded = false;
  double price = 0;
  var attrVal;
  var productPrice;
  var appBloc;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  getCartItems() {
    appBloc.getRepo().getCartRepository().getMyCart(callBack: (data) {
      setState(() {
        isDataLoaded = true;
      });
      Future.delayed(const Duration(milliseconds: 500), () {
        showInSnackBar(data);
      });
    }).then((data) {
      setState(() {
        cart = data;
        isDataLoaded = true;
      });
    });
  }

  removeItem(int id) async {
    await pr.showDialog();
    appBloc
        .getRepo()
        .getCartRepository()
        .removeItemFromCart(
            cartItemId: id,
            callBack: (data) {
              setState(() async {
                await pr.hideDialog();
                showInSnackBar(data);
                isDataLoaded = true;
              });
            })
        .then((data) async {
      if (data != null) {
        await pr.hideDialog();
        setState(() {
          cart = data;
          isDataLoaded = true;
          showInSnackBar('Item removed from cart');
        });
      }
    });
  }

  Widget _title() {
    return Container(
      margin: AppTheme.padding,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Shopping', style: TextConstants.P5),
              Text('Cart', style: TextConstants.H4),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _cartItems() {
    
    if (cart?.shoppingCartItems == null || cart.shoppingCartItems.length == 0) {
      return Column(
        children: <Widget>[
         
        ],
      );
    }
   
    return Column(
        children: cart.shoppingCartItems
            .map(
              (x) => CartItemCard(
                item: x,
                onDelClick: () {
                  removeItem(x.id);
                },
                onEditClick: () {
                  (context).addEvent(
                    ProductDetailViewEvent(
                      productId: x.product.id,
                      isCardProduct: true,
                      cartItemId: x.id,
                    ),
                  );
                },
              ),
            )
            .toList());
  }

  Widget _price() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          '${cart.totalQuantity} Items',
          style: TextConstants.H6.apply(color: LightColor.grey),
        ),
        Text(
          'Rs. ${cart.totalCost}',
          style: TextConstants.H4,
        ),
      ],
    ).padding(AppTheme.padding);
  }

  Widget _submitButton(BuildContext context) {
    return FlatButton(
        onPressed: () {
          context.addEvent(CartCheckoutViewEvent());
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: LightColor.orange,
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(vertical: 12),
          width: AppTheme.fullWidth(context) * .7,
          child: Text(
            'Checkout',
            style: TextStyle(
              color: LightColor.background,
              fontWeight: FontWeight.w500,
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    appBloc = BlocProvider.of<AppBloc>(context);
    if (pr == null) {
      pr = CustomProgressDialog(context: context);
    }
    if (isDataLoaded == false) {
      //print("get my cart");
      getCartItems();
    }
    var body = List<Widget>();

    body.add(_title());
    if (!isDataLoaded) {
      body.add(LoadingIndicator().center());
    } else {
      if (cart == null ||
          cart.shoppingCartItems == null ||
          cart.shoppingCartItems.length == 0) {
        body.add(Text('Cart Is Empty').center());
      } else {
        body.add(_cartItems());
        body.add(Divider(thickness: 1, height: 70));
        body.add(_price());
        body.add(LayoutConstants.sizedBox40H);
      }
    }

    return LayoutScreen(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: body,
        ),
      ),
      showDefaultBackButton: true,
      defaultAppBar: false,
      scaffoldKey: scaffoldKey,
      bottomBar: FixedBottomButton(
        onTap: () => context.addEvent(
          CartCheckoutViewEvent(),
        ),
        text: "Check Out",
      ),
    );
  }

}


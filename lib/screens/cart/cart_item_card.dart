import 'package:bluebellapp/models/shoppingCart_dto.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/attribute_control_type.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/network_cache_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CartItemCard extends StatelessWidget {
  ShoppingCartItem item;
  final Function onEditClick;
  final Widget attributeWidget;
  final Function onDelClick;
  CartItemCard(
      {this.item, this.onDelClick, this.onEditClick, this.attributeWidget});
  var attrVal;
  String getDropDownValue() {
    var dropAttr = item?.productAttributes?.productAttribute
        ?.firstWhere(
            (a) => a.productAttributeName == AttributeType.DropdownList,
            orElse: () => null)
        ?.attributeValues;
    if (dropAttr != null) {
      if (dropAttr?.attributeValue?.length != 0) {
        attrVal = dropAttr.attributeValue[0].name;
        return attrVal;
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    //return Text(item?.product?.name ?? "");
    getDropDownValue();
    var list = List<Widget>();
    list.add(Expanded(flex: 2, child: _getImage()));
    list.add(
      Expanded(
        flex: 5,
        child: Padding(
          padding: const EdgeInsets.only(left: 8, top: 5, bottom: 8),
          child: _getInfo(),
        ),
      ),
    );
    if (onDelClick != null) {
      list.add(
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: 12,
                  right: 0,
                ),
                child: _icon(
                  context,
                  Icons.delete,
                  color: Colors.white,
                  backgroundColor: LightColor.orange,
                  onPressed: onDelClick,
                ),
              ),
            ],
          ),
        ),
      );
    }
    return Card(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: list,
        ),
      ),
    );
  }

  Widget _getInfo() {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Wrap(
            children: <Widget>[
              Text(
                item.product.name,
                style: TextConstants.H6.apply(
                  color: LightColor.darkGrey,
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 20, bottom: 5, top: 2),
            child: InkWell(
              onTap: onEditClick,
              child: attributeWidget != null
                  ? attributeWidget
                  : Text(
                      attrVal ?? '',
                      style: TextConstants.H7.apply(
                        color: LightColor.black,
                      ),
                    ),
            ),
          ),
          Wrap(
            children: <Widget>[
              Text(
                'Rs. ' + item.totalPrice.toString() ?? '',
                style: TextConstants.H6_5.apply(
                  color: LightColor.orange,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _getImage() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      child: NetworkCacheImage(
        altImageUrl: "https://placehold.it/130",
        imageUrl: item.product.pictureThumb,
        width: 110.0,
        height: 90.0,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget _icon(
    BuildContext context,
    IconData icon, {
    Color color = LightColor.iconColor,
    Color backgroundColor,
    double size,
    void Function() onPressed,
  }) {
    backgroundColor = backgroundColor ?? Theme.of(context).backgroundColor;
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(13)),
            color: backgroundColor,
            boxShadow: AppTheme.shadow),
        child: size == null
            ? Icon(icon, color: color)
            : Icon(icon, color: color, size: size),
      ),
    );
  }
}

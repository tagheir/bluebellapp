import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/signup_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/button_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/screens/account/account_widgets/account_layout.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/email_field.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/password_field.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  SignUpDto signUp = SignUpDto();

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  bool _autovalidate = false;
  bool _formWasEdited = false;
  CustomProgressDialog pr;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormFieldState<String>> _nameFieldKey =
      GlobalKey<FormFieldState<String>>();
  final GlobalKey<FormFieldState<String>> _phoneFieldKey =
      GlobalKey<FormFieldState<String>>();
  final GlobalKey<FormFieldState<String>> _emailFieldKey =
      GlobalKey<FormFieldState<String>>();
  final GlobalKey<FormFieldState<String>> _passwordFieldKey =
      GlobalKey<FormFieldState<String>>();
  //final _UsNumberTextInputFormatter _phoneNumberFormatter = _UsNumberTextInputFormatter();
  Future<void> _handleSubmitted() async {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      form.save();
      await pr.showDialog();
      BlocProvider.of<AppBloc>(context).add(SignUpButtonPressedEvent(
          signUpDto: signUp,
          callBack: (data) async {
            await pr.hideDialog();
            if (data != null) {
              showInSnackBar(data.toString());
            }
          }));

      //showInSnackBar('${signUp.email}\'s');
    }
  }

  String validateName(String value) {
    _formWasEdited = true;
    if (value.isEmpty) return 'Name is required.';
    final RegExp nameExp = RegExp(r'^[A-Za-z ]+$');
    if (!nameExp.hasMatch(value))
      return 'Please enter only alphabetical characters.';
    return null;
  }

  String validatePassword(String value) {
    if (value == null || value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }

    return null;
  }

  String validatePhoneNumber(String value) {
    if (int.parse(value).toString().length < 11) {
      return 'Phone number invalid';
    }
    return null;
  }

  String validateEmail(String value) {
    // If empty value, the isEmail function throw a error.
    // So I changed this function with try and catch.
    try {
      if (value.length < 5) {
        return "Email Invalid";
      }
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }
  // String _validatePassword(String value) {
  //   _formWasEdited = true;
  //   final FormFieldState<String> passwordField = _passwordFieldKey.currentState;
  //   if (passwordField.value == null || passwordField.value.isEmpty)
  //     return 'Please enter a password.';
  //   if (passwordField.value != value) return 'The passwords don\'t match';
  //   return null;
  // }

  Future<bool> _warnUserAboutInvalidData() async {
    final FormState form = _formKey.currentState;
    if (form == null || !_formWasEdited || form.validate()) return true;

    return await showDialog<bool>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('This form has errors'),
              content: const Text('Really leave this form?'),
              actions: <Widget>[
                FlatButton(
                  child: const Text('YES'),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
                FlatButton(
                  child: const Text('NO'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
              ],
            );
          },
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    pr = CustomProgressDialog(context: context);
    return AccountScreenLayoutWidget(
      body: getSignUpForm(),
      pageDescription: GeneralStrings.SIGNUP_DESCRIPTION,
      actionText: GeneralStrings.SKIP,
      onActionPressed: () {
        BlocProvider.of<AppBloc>(context).add(DashboardScreenEvent());
      },
      appBarText: GeneralStrings.SIGNUP,
      onBackPressed: () {
        BlocProvider.of<AppBloc>(context).moveBack();
      },
      footerText: GeneralStrings.AlreadyHaveAccount,
      onFooterPressed: () {
        BlocProvider.of<AppBloc>(context).add(LoginScreenEvent());
      },
      scaffoldKey: _scaffoldKey,
      autoValidate: _autovalidate,
      formKey: _formKey,
    );
  }

  Form getSignUpForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          LayoutConstants.sizedBox10H,
          CustomTextField(
            fieldKey: _nameFieldKey,
            helperText: GeneralStrings.FIRST_NAME_FIELD_HELPER,
            prefixIcon: Icons.person,
            validator: (value) {
              return validateName(value);
            },
            labelText: GeneralStrings.FIRST_NAME_FIELD_LABEL,
            onChanged: (String value) {
              setState(() {
                signUp.firstName = value;
              });
            },
          ),
          LayoutConstants.sizedBox25H,
          CustomTextField(
            //fieldKey: _nameFieldKey,
            helperText: GeneralStrings.LAST_NAME_FIELD_HELPER,
            prefixIcon: Icons.person,
            validator: (value) {
              return validateName(value);
            },
            labelText: GeneralStrings.LAST_NAME_FIELD_LABEL,
            onChanged: (String value) {
              setState(() {
                signUp.lastName = value;
              });
            },
          ),
          LayoutConstants.sizedBox25H,
          CustomTextField(
            fieldKey: _phoneFieldKey,
            prefixIcon: Icons.phone,
            // validator: (value) {
            //   return validatePhoneNumber(value);
            // },
            helperText: GeneralStrings.PHONE_NUMBER_FIELD_HELPER,
            labelText: GeneralStrings.PHONE_NUMBER_FIELD_LABEL,
            onChanged: (String value) {
              setState(() {
                signUp.phoneNumber = value;
              });
            },
          ),
          LayoutConstants.sizedBox25H,
          EmailField(
            fieldKey: _emailFieldKey,
            isEmail: true,
            validator: (value) {
              return validateEmail(value);
            },
            onChanged: (String value) {
              setState(() {
                signUp.email = value;
              });
            },
          ),
          LayoutConstants.sizedBox25H,
          PasswordField(
            fieldKey: _passwordFieldKey,
            validator: (value) {
              return validatePassword(value);
            },
            onChanged: (String value) {
              setState(() {
                signUp.password = value;
              });
            },
          ),
          LayoutConstants.sizedBox25H,
          ButtonConstants.primaryBlockButton(
            onPressed: () {
              _handleSubmitted();
            }, //_handleSubmitted, //since this is only a UI app
            child: Text(
              GeneralStrings.SIGNUP,
              style: TextConstants.H6,
            ),
            shape: LayoutConstants.shapeBorderRadius10,
          ).center(),
          LayoutConstants.sizedBox25H,
          FlatButton(
            onPressed: () {
              BlocProvider.of<AppBloc>(context).add(ForgotPasswordEvent());
            },
            child: Text(
              GeneralStrings.FORGOT_PASSWORD_LINK_TEXT,
              style: TextConstants.H6,
            ).center().padding(LayoutConstants.edgeInsetsTop20),
          ),
        ],
      ),
    );
  }
}

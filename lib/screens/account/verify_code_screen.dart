import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/login_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/button_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/screens/account/account_widgets/account_layout.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';

class VerifyCodeScreen extends StatefulWidget {
  @override
  _VerifyCodeScreenState createState() => _VerifyCodeScreenState();
}

class _VerifyCodeScreenState extends State<VerifyCodeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  LoginDto login = LoginDto();
  String code;
  bool _autovalidate = false;
  CustomProgressDialog customProgressDialog;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  submitPin() async {
    if (code == null || code.isEmpty || code.length < 6) {
      showInSnackBar('Pin is invalid');
    } else {
      await customProgressDialog.showDialog();
      _scaffoldKey.addEvent(
        VerifyCodeButtonPressed(
          code: code,
          callBack: (data) async {
            await customProgressDialog.hideDialog();
            if (data != null) {
              showInSnackBar(data);
            }
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    customProgressDialog = CustomProgressDialog(context: context);
    return AccountScreenLayoutWidget(
      pageDescription: GeneralStrings.VERIFY_CODE_DESCRIPTION,
      actionText: GeneralStrings.SKIP,
      onActionPressed: () {
        _scaffoldKey.addEvent(DashboardScreenEvent());
      },
      appBarText: GeneralStrings.VERIFY_CODE,
      onBackPressed: () {
        _scaffoldKey.moveBack();
      },
      scaffoldKey: _scaffoldKey,
      body: getForm(),
      autoValidate: _autovalidate,
      formKey: _formKey,
    );
  }

  Column getForm() {
    return Column(
      children: <Widget>[
        LayoutConstants.sizedBox50H,
        Container(
          height: 150,
          child: Padding(
            padding: const EdgeInsets.all(0.0),
            child: Center(
              child: PinEntryTextField(
                fields: 6,
                showFieldAsBox: true,
                onSubmit: (String pin) {
                  code = pin;
                }, // end onSubmit
              ),
            ), // end PinEntryTextField()
          ), // end Padding()
        ),
        LayoutConstants.sizedBox30H,
        ButtonConstants.primaryBlockButton(
          onPressed: submitPin,
          child: Text(
            GeneralStrings.VERIFY,
            style: TextConstants.H6,
          ),
          shape: LayoutConstants.shapeBorderRadius10,
        ).center(),
      ],
    );
  }
}

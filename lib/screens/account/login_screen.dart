import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/login_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/button_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/screens/account/account_widgets/account_layout.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/email_field.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/password_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LogInScreen extends StatefulWidget {
  @override
  _LogInScreenState createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  LoginDto login = LoginDto();

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  bool _autovalidate = false;
  bool _formWasEdited = false;
  bool isEmail = false;
  CustomProgressDialog pr;
  //BuildContext cntxt;
  FocusNode emailFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  //final _UsNumberTextInputFormatter _phoneNumberFormatter = _UsNumberTextInputFormatter();
  Future<void> _handleSubmitted() async {
    final FormState form = _formKey.currentState;
    if (!_formKey.currentState.validate()) {
      _autovalidate = true; // Start validating on every change.
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      form.save();
      await pr.showDialog();
      context.addEvent(
        LoginButtonPressedEvent(
          loginDto: login,
          callBack: (data) async {
            await pr.hideDialog();
            if (data != null) {
              showInSnackBar(data.toString());
            }
          },
        ),
      );
      // setState(() {
      //   emailFocus.unfocus();
      //   passwordFocus.unfocus();
      // });
      // showInSnackBar('${login.email}\'s');
    }
  }

  String validatePassword(String value) {
    if (value == null || value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }

    return null;
  }

  String validateEmail(String value) {
    // If empty value, the isEmail function throw a error.
    // So I changed this function with try and catch.

    try {
      if (value.length < 5) {
        return "Email invalid";
      }
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  void handleSignUp() {
    context.addEvent(SignUpEvent());
  }

  @override
  Widget build(BuildContext context) {
    pr = CustomProgressDialog(context: context);
    // cntxt = context;
    return AccountScreenLayoutWidget(
      pageDescription: GeneralStrings.LOGIN_DESCRIPTION,
      actionText: GeneralStrings.SKIP,
      onActionPressed: () {
        BlocProvider.of<AppBloc>(context).add(DashboardScreenEvent());
      },
      appBarText: GeneralStrings.LOGIN,
      onBackPressed: () {
        BlocProvider.of<AppBloc>(context).moveBack();
      },
      footerText: GeneralStrings.DoNotHaveAccount,
      onFooterPressed: () {
        BlocProvider.of<AppBloc>(context).add(SignUpEvent());
      },
      scaffoldKey: _scaffoldKey,
      body: getLoginForm(),
      autoValidate: _autovalidate,
      formKey: _formKey,
    );
  }

  Form getLoginForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          const SizedBox(height: 10.0),
          EmailField(
            focusNode: emailFocus,
            onChanged: (value) {
              setState(() {
                login.email = value;
                isEmail = value.contains('@') ? true : false;
              });
            },
            validator: (value) {
              return validateEmail(value);
            },
            onFieldSubmitted: (String value) {
              setState(() {
                login.email = value;
              });
            },
          ),
          LayoutConstants.sizedBox10H,
          isEmail == true
              ? PasswordField(
                  focusNode: passwordFocus,
                  onChanged: (value) {
                    setState(() {
                      login.password = value;
                    });
                  },
                  validator: (value) {
                    return validatePassword(value);
                  },
                  onFieldSubmitted: (String value) {
                    setState(() {
                      login.password = value;
                    });
                  },
                )
              : Text(''),
          LayoutConstants.sizedBox25H,
          ButtonConstants.primaryBlockButton(
            onPressed: _handleSubmitted,
            child: Text(
              GeneralStrings.LOGIN_2,
              style: TextConstants.H6,
            ),
            shape: LayoutConstants.shapeBorderRadius10,
          ).center(),
          FlatButton(
            onPressed: () {
              context.addEvent(ForgotPasswordEvent());
            },
            child: Text(
              GeneralStrings.FORGOT_PASSWORD_LINK_TEXT,
              style: TextConstants.H6,
            ).center().padding(LayoutConstants.edgeInsetsTop20),
          ),
        ],
      ),
    );
  }

  // Future<bool> _warnUserAboutInvalidData() async {
  //   final FormState form = _formKey.currentState;
  //   if (form == null || !_formWasEdited || form.validate()) return true;

  //   return await showDialog<bool>(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return AlertDialog(
  //             title: const Text('This form has errors'),
  //             content: const Text('Really leave this form?'),
  //             actions: <Widget>[
  //               FlatButton(
  //                 child: const Text(GeneralStrings.YES),
  //                 onPressed: () {
  //                   Navigator.of(context).pop(true);
  //                 },
  //               ),
  //               FlatButton(
  //                 child: const Text(GeneralStrings.NO),
  //                 onPressed: () {
  //                   Navigator.of(context).pop(false);
  //                 },
  //               ),
  //             ],
  //           );
  //         },
  //       ) ??
  //       false;
  // }
}

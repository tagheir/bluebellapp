import 'package:bluebellapp/resources/constants/colors.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/screens/widgets/fixed_bottom_button.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class AccountScreenLayoutWidget extends StatefulWidget {
  final void Function() onBackPressed;
  final void Function() onActionPressed;
  final void Function() onFooterPressed;
  final String actionText;
  final String appBarText;
  final String footerText;
  final String pageDescription;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final GlobalKey<FormState> formKey;
  final Widget body;
  final bool autoValidate;

  const AccountScreenLayoutWidget(
      {Key key,
      this.onBackPressed,
      this.onActionPressed,
      this.actionText,
      this.appBarText,
      this.scaffoldKey,
      this.body,
      this.onFooterPressed,
      this.footerText,
      this.pageDescription,
      this.formKey,
      this.autoValidate})
      : super(key: key);

  @override
  _AccountScreenLayoutWidgetState createState() =>
      _AccountScreenLayoutWidgetState();
}

class _AccountScreenLayoutWidgetState extends State<AccountScreenLayoutWidget> {
  @override
  Widget build(BuildContext context) {
    var pageDescription = Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: Text(
        widget.pageDescription,
        style: TextConstants.H6_5.primary(),
      ),
    );
    var body = Padding(
      padding: LayoutConstants.edgeInsetsH25,
      child: widget.body,
    );

    // var safeArea = SafeArea(
    //   //top: false,
    //   //bottom: false,
    //   maintainBottomViewPadding: true,
    //   child: Form(
    //     key: widget.formKey,
    //     autovalidate: widget.autoValidate,
    //     //onWillPop: _warnUserAboutInvalidData,
    //     child: Scrollbar(
    //       child: SingleChildScrollView(
    //         dragStartBehavior: DragStartBehavior.down,
    //         padding: LayoutConstants.edgeInsetsH25,
    //         child: Container(
    //           child: widget.body,
    //         ),
    //       ),
    //     ),
    //   ),
    // );
    var columnWidgets = List<Widget>();
    columnWidgets.add(pageDescription);
    columnWidgets.add(LayoutConstants.sizedBox30H);
    columnWidgets.add(body);
    columnWidgets.add(LayoutConstants.sizedBox20H);
    Widget mainBody = ListView(
      children: columnWidgets,
    );
    if (widget.footerText != null) {
      var footerButton = FixedBottomButton(
        onTap: widget.onFooterPressed,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(widget.footerText, style: TextConstants.P6_5.white()),
            ],
          ),
        ),
      );
      mainBody = Stack(
        children: <Widget>[
          mainBody,
          Positioned(
            bottom: 0,
            right: 0,
            child: footerButton,
          )
        ],
      );
      // var footer = MaterialButton(
      //   padding: LayoutConstants.edgeInsets0,
      //   onPressed: widget.onFooterPressed,
      //   child: Container(
      //     width: MediaQuery.of(context).size.width,
      //     height: 50,
      //     color: BBColors.primaryColor,
      //     child: Center(
      //         child: Row(
      //       mainAxisAlignment: MainAxisAlignment.center,
      //       children: <Widget>[
      //         Text(widget.footerText, style: TextConstants.P6_5.white()),
      //       ],
      //     )),
      //   ),
      // );
      // columnWidgets
      //     .add(SizedBox(height: MediaQuery.of(context).size.height - 550));
      // columnWidgets.add(footer);
    }

    return Scaffold(
      resizeToAvoidBottomPadding: true,
      backgroundColor: BBColors.primaryColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        leading: FlatButton(
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: widget.onBackPressed,
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: widget.onActionPressed,
            child: Text(
              widget.actionText,
              style: TextConstants.H7.white(),
            ),
          )
        ],
        elevation: 0,
        backgroundColor: BBColors.primaryColor,
        // centerTitle: true,
        title: Text(
          widget.appBarText,
          style: TextStyle().white(),
        ),
      ),
      drawerDragStartBehavior: DragStartBehavior.down,
      key: widget.scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40.0),
            topRight: Radius.circular(40.0),
          ),
        ), //MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 30.0),
        child: mainBody,
      ),
    );
  }
}

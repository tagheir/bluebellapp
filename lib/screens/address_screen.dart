import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/address_enum.dart';
import 'package:bluebellapp/models/customerAddress_dto.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/button_widgets/tab_back_button.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/empty_data_widget.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/text_widgets/general-text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'widgets/views_widgets/addressListTile_widget.dart';

class CustomerAddress extends StatefulWidget {
  final bool showTabBar;
  final AddressType addressType;
  CustomerAddress({this.showTabBar, this.addressType});
  @override
  _AddressesListState createState() => _AddressesListState();
}

class _AddressesListState extends State<CustomerAddress> {
  @override
  void initState() {
    super.initState();
  }

  int selectedRadioTile = 0;
  List<CustomerAddressDto> addresses;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool isAddressUpdated = false;
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  int groupValue = -1;
  bool isDataLoaded = false;
  CustomProgressDialog pd;
  AppBloc appBloc;

  updateBillingAddress(CustomerAddressDto address) async {
    await pd.showDialog(
          message: "Updating Billing Address",
          currentContext: scaffoldKey.currentContext,
        );
    var addressType =
        widget.addressType == null ? AddressType.None : widget.addressType;
    var repo = appBloc.getRepo().getUserRepository();
    var data = await repo.editCustomerAddress(
      address: address,
      addressType: addressType,
      callBack: (data) async {
        if (data != null) {
          showInSnackBar(data);
          await pd.hideDialog();
        }
      },
    );
    if (data != null) {
      if (data != null) {
        await pd.hideDialog();
        if (data == true) {
          appBloc.moveBack();
        }
      }
    }
    //   .then(
    // (data) {
    //   if (data != null) {
    //     appBloc.moveBack();
    //   }

    //   // Navigator.of(cntxt).pop();
    // },
    // );
  }
  // int customselectedRadioTile = 0;

  @override
  Widget build(BuildContext context) {
    appBloc = BlocProvider.of<AppBloc>(context);
    pd = CustomProgressDialog(context: context);
    if (isDataLoaded == false && addresses == null) {
      appBloc.getRepo().getUserRepository().getCustomerAddress(
          callBack: (data) {
        setState(() {
          isDataLoaded = true;
        });
        showInSnackBar(data);
      }).then((data) {
        setState(() {
          isDataLoaded = true;
          addresses = appBloc.getRepo().getUserRepository().myAddresses;
        });
      });
    }

    return Scaffold(
        //    backgroundColor: Colors.white,
        floatingActionButton: getAddAddressButton(context),
        key: scaffoldKey,
        appBar: widget.showTabBar == false ? null : getAppBar(context),
        body: isDataLoaded == false
            ? LoadingIndicator()
            : getAddressTile(context));
  }

  AppBar getAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: ThemeScheme.iconColorDrawer,
      title: GeneralText(
        'Choose Address',
        fontSize: 20,
      ),
      leading: TabBackButton(
        callback: () {
          appBloc.moveBack();
          //Navigator.pop(context);
        },
      ),
    );
  }

  FloatingActionButton getAddAddressButton(BuildContext cntxt) {
    return FloatingActionButton(
      onPressed: () {
        appBloc.add(CreateOrEditAddressViewEvent(
            returnState: widget.addressType == AddressType.BillingAddress
                ? CartCheckoutViewState()
                : CustomerProfileViewState(tabIndex: 1),
            addressType: widget.addressType == null
                ? AddressType.None
                : widget.addressType));
      },
      child: Icon(
        Icons.add,
        color: Colors.white,
      ),
      heroTag: 'Add Address',
    );
  }

  Widget getAddressTile(BuildContext cntxt) {
    if (addresses == null || addresses.length <= 0)
      return DataEmpty(message: GeneralStrings.NO_ADDRESSES);
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: addresses.length,
      itemBuilder: (context, int i) => GestureDetector(
        onTap: () async {
          if (widget.addressType != null) {
            if (widget.addressType == AddressType.BillingAddress) {
              await updateBillingAddress(addresses[i]);
            }
          }
        },
        child: AddressListTile(
          add: addresses[i],
          value: i,
          groupValue: groupValue,
          callBack: () {
            appBloc.add(CreateOrEditAddressViewEvent(
                address: addresses[i],
                returnState: widget.addressType == AddressType.BillingAddress
                    ? CartCheckoutViewState()
                    : CustomerProfileViewState(tabIndex: 1),
                addressType: widget.addressType == null
                    ? AddressType.None
                    : widget.addressType));
          },
        ),
      ),
    );
  }
}

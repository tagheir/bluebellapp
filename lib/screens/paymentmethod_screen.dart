
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/cardPayement_screen.dart';
import 'package:flutter/material.dart';

class PaymentSelectionMethod extends StatefulWidget {
  @override
  _PaymentSelectionMethodState createState() => _PaymentSelectionMethodState();
}

class _PaymentSelectionMethodState extends State<PaymentSelectionMethod> {
  int selectedRadio;
  int selectedRadioTile;
  @override
  void initState() {
    super.initState();
    selectedRadio = 0;
    selectedRadioTile = 0;
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.iconColorDrawer,
        title: Text(
          'Payment method',
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          InkWell(
            onTap: () {
             
            },
            child: Container(
              height: 70,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: RadioListTile(
                      value: 1,
                      groupValue: selectedRadioTile,
                      title: Text('By Cash'),
                      onChanged: (val) {
                        print('Radio tile pressed  $val');
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                  Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Image.network(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQpa4bprhI2pLmtQK6mtyRiSYsQerVS3q-k1CvDgFfEGenvn7xV'),
                      ))
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
               Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => CardPayment()));
              //TODO
            },
            child: Container(
              height: 70,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: RadioListTile(
                      value: 2,
                      groupValue: selectedRadioTile,
                      title: Text('Credit or debit card'),
                      onChanged: (val) {
                        print('Radia tile pressed  $val');
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                  Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Image.network(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWjS0hD66RXw1AhcZL2SK2prpk9Cl0hMeMbRtDmlQlYZ0qfXb6'),
                      ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/address_enum.dart';
import 'package:bluebellapp/models/customerAddress_dto.dart';
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'widgets/helper_widgets/button_widgets/tab_back_button.dart';
import 'widgets/helper_widgets/simple_form_field.dart';
import 'widgets/helper_widgets/text_widgets/general-text.dart';

class CreateOrEditAddressScreen extends StatefulWidget {
  CustomerAddressDto address;
  final AddressType addressType;
  final AppState returnState;
  CreateOrEditAddressScreen({
    this.address,
    this.addressType,
    this.returnState,
  });
  @override
  _CreateOrEditAddressScreenState createState() =>
      _CreateOrEditAddressScreenState();
}

class _CreateOrEditAddressScreenState extends State<CreateOrEditAddressScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    //await pd.hideDialog();
    super.dispose();
  }

  var maskFormatter = new MaskTextInputFormatter(
      mask: '+92 ##########', filter: {"#": RegExp(r'[0-9]')});

  var appBloc;
  bool isDataLoaded = false;
  bool isAddressUpdated = false;
  CustomProgressDialog pd;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  addAddress() async {
    print(widget.address.firstName);
    await pd.showDialog();
    appBloc
        .getRepo()
        .getUserRepository()
        .addCustomerAddress(
            address: widget.address,
            addressType: widget.addressType == null
                ? AddressType.None
                : widget.addressType,
            callBack: (data) async {
              showInSnackBar(data);
              await pd.hideDialog();
            })
        .then((data) async {
      print('=======data =======');
      await pd.hideDialog();
      appBloc.moveBack(returnState: widget.returnState);
      print(widget.address.firstName);
    });
  }

  editAddress() async {
    await pd.showDialog();
    print(widget.address.firstName);
    appBloc
        .getRepo()
        .getUserRepository()
        .editCustomerAddress(
            address: widget.address,
            addressType: widget.addressType == null
                ? AddressType.None
                : widget.addressType,
            callBack: (data) async {
              showInSnackBar(data);
              setState(() {
                isAddressUpdated = true;
              });
              await pd.hideDialog();
            })
        .then((data) {
      setState(() {
        isAddressUpdated = true;
      });
      pd.hideDialog();
      print('=======data =======');
      appBloc.moveBack(returnState: widget.returnState);
    });
  }

  @override
  Widget build(BuildContext context) {
    pd = new CustomProgressDialog(context: context);
    appBloc = BlocProvider.of<AppBloc>(context);
    if (widget.address == null) {
      print("=adress null===========");
      widget.address = CustomerAddressDto();
    }
    return Scaffold(
      key: scaffoldKey,
      appBar: getAppBar(context),
      body: getForm(),
    );
  }

  AppBar getAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: ThemeScheme.iconColorDrawer,
      title: GeneralText(
        'Address',
        fontSize: 20,
      ),
      leading: TabBackButton(
        callback: () {
          appBloc.moveBack();
        },
      ),
    );
  }

  ListView getForm() {
    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: SimpleFormField(
                      initialValue: widget.address?.firstName,
                      inputType: TextInputType.text,
                      label: 'Firstname *',
                      maxLength: 10,
                      onChanged: (value) {
                        widget.address.firstName = value;
                      })),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: SimpleFormField(
                      initialValue: widget.address?.lastName,
                      inputType: TextInputType.text,
                      label: 'Lastname *',
                      maxLength: 10,
                      onChanged: (value) {
                        widget.address.lastName = value;
                      })),
            )
          ],
        ),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: SimpleFormField(
                initialValue: widget?.address?.phoneNumber == null
                    ? '+92 '
                    : widget?.address?.phoneNumber,
                inputType: TextInputType.number,
                label: 'Phone number *',
                maxLength: 14,
                onChanged: (value) {
                  widget.address.phoneNumber = value;
                })),
        // Padding(
        //     padding: const EdgeInsets.symmetric(horizontal: 16.0),
        //     child: getFormField(
        //         initialValue: widget.address?.countryName,
        //         inputType: TextInputType.number,
        //         label: 'Country *',
        //         maxLength: 15,
        //         onChanged: (value) {
        //           widget.address.countryName = value;
        //         })),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: SimpleFormField(
                initialValue: widget.address?.city,
                inputType: TextInputType.text,
                label: 'City *',
                maxLength: 15,
                onChanged: (value) {
                  widget.address.city = value;
                })),
        // Padding(
        //     padding: const EdgeInsets.symmetric(horizontal: 16.0),
        //     child: getFormField(
        //         initialValue: widget.address?.stateProvinceName,
        //         inputType: TextInputType.text,
        //         label: 'State/province *',
        //         maxLength: 15,
        //         onChanged: (value) {
        //           widget.address.stateProvinceName = value;
        //         })),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: SimpleFormField(
                maxLines: 2,
                initialValue: widget.address?.address1,
                inputType: TextInputType.text,
                label: 'Address *',
                maxLength: 50,
                onChanged: (value) {
                  widget.address.address1 = value;
                })),
        getSaveButton(),
      ],
    );
  }

  Padding getSaveButton() {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 38.0, horizontal: 18),
        child: Card(
          clipBehavior: Clip.antiAlias,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          child: ButtonTheme(
            height: 50,
            minWidth: 400,
            child: RaisedButton(
              onPressed: () {
                widget.address.id != null ? editAddress() : addAddress();
                print(widget.address.firstName);
              },
              textColor: Colors.white,
              child: Text(
                'Save Address',
                style: TextStyle(fontSize: 20),
              ),
              color: ThemeScheme.lightTheme.primaryColor,
            ),
          ),
        ));
  }

}

import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/shared/_layout_screen.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/empty_data_widget.dart';
import 'package:bluebellapp/screens/widgets/views_widgets/product_compact_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SubCategories extends StatelessWidget {
  final List<ProductDto> products;
  final String title;
  SubCategories({this.products, this.title});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    //final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    final double itemHeight = itemWidth * 1.35;

    var body = List<Widget>();
    body.add(_title());
    body.add(
      products == null || products?.length == 0
          ? DataEmpty(message: 'No products to show')
          : Container(
            height: AppTheme.fullHeight(context) - 150,
            child: GridView.count(
                mainAxisSpacing: 20.0,
                crossAxisSpacing: 20.0,
                padding: EdgeInsets.all(10.0),
                crossAxisCount: 2,
                childAspectRatio: (itemWidth / itemHeight),
                children: products
                    .map(
                      (product) => ProductCompactView(
                        product: product,
                        height: itemHeight,
                      ),
                    )
                    .toList(),
              ),
          ),
    );
    return LayoutScreen(
      showDefaultBackButton: true,
      showBottomNav: false,
      body: Column(children: body),
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.iconColorDrawer,
        leading: new IconButton(
            icon: new Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              BlocProvider.of<AppBloc>(context).moveBack();
            }),
        title: Text(
          "Service Products",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: products == null || products?.length == 0
          ? DataEmpty(message: 'No products to show')
          : GridView.count(
              mainAxisSpacing: 20.0,
              crossAxisSpacing: 20.0,
              padding: EdgeInsets.all(10.0),
              crossAxisCount: 2,
              childAspectRatio: (itemWidth / itemHeight),
              children: List.generate(products?.length, (index) {
                return ProductCompactView(
                  product: products[index],
                  height: itemHeight,
                );
              }),
            ),
    );
  }

  Widget _title() {
    return Container(
      margin: AppTheme.padding,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(title, style: TextConstants.H5),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }
}

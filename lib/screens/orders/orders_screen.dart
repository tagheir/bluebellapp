import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/order_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/orders/order_card.dart';
import 'package:bluebellapp/screens/shared/_layout_screen.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/empty_data_widget.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomerOrders extends StatefulWidget {
  final bool showTabBar;
  final AppState returnState;
  CustomerOrders({this.showTabBar, this.returnState});
  @override
  _CustomerOrdersState createState() => _CustomerOrdersState();
}

class _CustomerOrdersState extends State<CustomerOrders> {
  List<OrderDto> orderlist;
  var appBloc;
  bool isDataLoaded = false;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  getCustomerOrders() {
    appBloc.getRepo().getCartRepository().getMyOrders(callBack: (data) {
      setState(() {
        isDataLoaded = true;
      });
      Future.delayed(const Duration(milliseconds: 500), () {
        showInSnackBar(data);
      });
    }).then((data) {
      setState(() {
        orderlist = data;
        isDataLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    appBloc = BlocProvider.of<AppBloc>(context);
    if (isDataLoaded == false) {
      getCustomerOrders();
    }
    var list = List<Widget>();
    list.add(_title());
    if (!isDataLoaded) {
      list.add(LoadingIndicator());
    } else {
      list.add(
        orderlist == null || orderlist.length == 0
            ? DataEmpty(message: GeneralStrings.NO_ORDERS)
            : Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                width: AppTheme.fullWidth(context) ,
                height: AppTheme.fullHeight(context) - 150,
                child: ListView(
                  children: orderlist
                      .map((order) => MyOrderCard(obj: order,))
                      .toList(),
                ),
              ),
      );
    }
    return LayoutScreen(
      scaffoldKey: scaffoldKey,
      defaultAppBar: false,
      showDefaultBackButton: true,
      showBottomNav: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: list,
      ),
    );
  }

  Widget _title() {
    return Container(
      margin: AppTheme.padding,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(GeneralStrings.MY_ORDERS, style: TextConstants.H5),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }

}

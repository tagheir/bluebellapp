import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/address_enum.dart';
import 'package:bluebellapp/models/customerAddress_dto.dart';
import 'package:bluebellapp/models/shoppingCart_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/attribute_control_type.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/cart/cart_item_card.dart';
import 'package:bluebellapp/screens/loading_screen.dart';
import 'package:bluebellapp/screens/paymentmethod_screen.dart';
import 'package:bluebellapp/screens/widgets/fixed_bottom_button.dart';
import 'package:bluebellapp/services/getDate_service.dart';
import 'package:bluebellapp/screens/shared/_layout_screen.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckoutPage extends StatefulWidget {
  ShoppingCartDto cartDto;
  Function(int, CustomProgressDialog) onPlaceOrder;
  bool isOrderPlaced;
  GlobalKey<ScaffoldState> scaffoldKey;
  bool isDataLoaded;

  CheckoutPage({
    @required this.cartDto,
    this.scaffoldKey,
    this.isOrderPlaced,
    @required this.onPlaceOrder,
    this.isDataLoaded = false,
  });
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  AppBloc appBloc;
  bool isOrderPlaced = false;
  //final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  // ShoppingCartDto cart = ShoppingCartDto();
  CustomerAddressDto billingAddress = CustomerAddressDto();
  CustomProgressDialog pr;
  BuildContext cntxt;
  bool isAddressLoaded = false;
  bool isAddressRequestInProgress = false;
  String attrVal;
  String dateVal;
  var productPrice;
  void showInSnackBar(String value) {
    widget.scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  getBillingAddress() {
    if (isAddressRequestInProgress == true) return;
    isAddressRequestInProgress = true;
    appBloc
        .getRepo()
        .getUserRepository()
        .getBillingOrShippingAddress(
            addressType: AddressType.BillingAddress,
            callBack: (data) {
              // setState(() {
              //   isAddressLoaded = true;
              // });
              showInSnackBar(data);
            })
        .then((data) {
      setState(() {
        isAddressLoaded = true;
        if (data != null) {
          billingAddress = data;
        }
      });
      isAddressRequestInProgress = false;
    });
  }

  getDropDownValue(ShoppingCartItem item) {
    //   if (item?.productAttributes != null) {
    //     appBloc
    //         .getRepo()
    //         .getCartRepository()
    //         .getDropDownAttributeValue(productId: item.product.id)
    //         .then((data) {
    //       setState(() {
    //         if (attrVal == null) {
    //           attrVal = data.name;
    //         }
    //       });
    //     });
    //   }
  }

  getDateValue(ShoppingCartItem item) {
    if (item?.productAttributes != null) {
      appBloc
          .getRepo()
          .getCartRepository()
          .getDateAttributeValue(productId: item?.product?.id)
          .then((data) {
        setState(() {
          if (dateVal == null) {
            dateVal = data;
          }
        });
      });
    }
  }

  getCartItemAttributes(ShoppingCartItem cartItem) {
    getDropDownValue(cartItem);
    getDateValue(cartItem);
  }

  placeOrder() {
    widget.onPlaceOrder(billingAddress.id, pr);
  }

  Widget _title() {
    return Container(
      //margin: AppTheme.padding,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Checkout', style: TextConstants.H4),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appBloc = context.getAppBloc();
    pr = CustomProgressDialog(context: context);
    cntxt = context;
    if (billingAddress.id == null && isAddressLoaded == false) {
      getBillingAddress();
    }
    Widget body = LoadingScreen();
    if (widget.isDataLoaded == true &&
        widget.cartDto?.shoppingCartItems != null) {
      body = Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: ListView(
          children: <Widget>[
            _title(),
            LayoutConstants.sizedBox30H,
            itemsCard(context),
            LayoutConstants.sizedBox10H,
            getPriceCard(context),
            LayoutConstants.sizedBox10H,
            getAddressInfo(context),
            getPaymentInfoCard(context),
            getTermsCard(context),
            LayoutConstants.sizedBox40H,
          ],
        ),
      );
    }
    // body = widget.isDataLoaded == false &&
    //         widget.cartDto?.shoppingCartItems == null
    //     ? LoadingIndicator()
    //     : Stack(
    //         children: <Widget>[
    //           SingleChildScrollView(
    //             child: Column(
    //               mainAxisSize: MainAxisSize.max,
    //               children: <Widget>[
    //                 _title(),
    //                 LayoutConstants.sizedBox30H,
    //                 itemsCard(context),
    //                 LayoutConstants.sizedBox10H,
    //                 getPriceCard(context),
    //                 LayoutConstants.sizedBox10H,
    //                 getAddressInfo(context),
    //                 getPaymentInfoCard(context),
    //                 getTermsCard(context),
    //               ],
    //             ), //.padding(AppTheme.padding),
    //           ),
    //           getBottomTab(context),
    //         ],
    //       );
    return LayoutScreen(
      body: body,
      showDefaultBackButton: true,
      showBottomNav: true,
      defaultAppBar: false,
      scaffoldKey: widget.scaffoldKey,
      bottomBar: FixedBottomButton(
        onTap: placeOrder,
        text: "Place your order",
      ),
    );
  }

  Padding getTermsCard(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 90.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                  'By Completing the order the order ,I hereby accepts to the terms and condition',
                  style: TextConstants.H8.apply(color: LightColor.lightBlack)),
            ],
          ),
        ),
      ),
    );
  }

  Container getPaymentInfoCard(BuildContext context) {
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Payment Method',
                  style: TextConstants.H6.apply(color: LightColor.lightBlack)),
              SizedBox(
                height: 5,
              ),
              FlatButton(
                onPressed: () {
                  Navigator.of(context).push(
                    PageRouteBuilder(
                        opaque: false,
                        pageBuilder: (BuildContext context, _, __) =>
                            PaymentSelectionMethod()),
                  );
                },
                child: Text('CASH',
                    style: TextConstants.H7.apply(color: LightColor.orange)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Card getAddressInfo(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Contact Info',
                    style:
                        TextConstants.H7.apply(color: LightColor.lightBlack)),
                Text('Please Enter Contact Info',
                    style:
                        TextConstants.H8.apply(color: LightColor.lightBlack)),
              ],
            ),
            Divider(),
            SizedBox(
              height: 10,
            ),
            InkWell(
                onTap: () {
                  appBloc.add(CustomerAddressesViewEvent(
                      addressType: AddressType.BillingAddress));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Billing Address',
                            style: TextConstants.H8.apply(
                              color: LightColor.lightBlack,
                            ),
                          ),
                          LayoutConstants.sizedBox5H,
                          Text(
                            billingAddress?.address1 == null
                                ? 'Choose Address'
                                : billingAddress?.address1,
                            style: TextConstants.H7.apply(
                              color: LightColor.black,
                            ),
                          ),
                          LayoutConstants.sizedBox15H,
                        ],
                      ),
                    ),
                    // Expanded(
                    //   flex: 1,
                    //   child: Column(
                    //     children: <Widget>[Icon(Icons.location_on)],
                    //   ),
                    // )
                  ],
                )),
            // Divider(),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: <Widget>[
            //     Text(
            //       'Delivery Time',
            //       style: TextStyle(fontSize: 15),
            //     ),
            //     Text(
            //       'NOW(55 min)',
            //       style: TextStyle(fontSize: 15),
            //     )
            //   ],
            // )
          ],
        ),
      ),
    );
  }

  Card getPriceCard(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Subtotal',
                    style:
                        TextConstants.H6.apply(color: LightColor.lightBlack)),
                Text("Rs. " + widget.cartDto?.totalCost?.toString() ?? "0.0",
                    style: TextConstants.H7.apply(color: LightColor.black)),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Delivery Fee',
                    style:
                        TextConstants.H7.apply(color: LightColor.lightBlack)),
                Text('Rs. 0.0',
                    style: TextConstants.H6.apply(color: LightColor.black)),
              ],
            ),
            LayoutConstants.sizedBox10H,
            Text(
              'Do you have a free voucher?',
              style: TextStyle(color: ThemeScheme.lightTheme.primaryColor),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Total',
                    style:
                        TextConstants.H5.apply(color: LightColor.lightBlack)),
                Text("Rs. "  + widget.cartDto?.totalCost?.toString() ?? "0.0",
                    style: TextConstants.H5.apply(color: LightColor.black)),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget getItemCardWidget(ShoppingCartItem cartItem) {
    getCartItemAttributes(cartItem);
    return CartItemCard(
      item: cartItem,
      attributeWidget: dateVal == null ? Text(''): Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(Icons.calendar_today, color: LightColor.orange, size: 15),
          SizedBox(
            width: 5,
          ),
          Text(
            dateVal ?? '',
            style: TextConstants.H8.apply(
              color: LightColor.black,
            ),
          )
        ],
      ),
    );
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Wrap(
                    children: <Widget>[
                      Text(cartItem.product.name,
                          style:
                              TextConstants.H6.apply(color: LightColor.orange))
                    ],
                  ),
                  Wrap(
                    children: <Widget>[
                      Text(attrVal ?? '',
                          style: TextConstants.H7
                              .apply(color: LightColor.lightBlack))
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  dateVal == null
                      ? Text('')
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.calendar_today,
                                color: LightColor.orange, size: 15),
                            SizedBox(
                              width: 5,
                            ),
                            Text(dateVal,
                                style: TextConstants.H8
                                    .apply(color: LightColor.black))
                          ],
                        ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Wrap(
                    children: <Widget>[
                      Text('Rs ' + cartItem.totalPrice.toString(),
                          style:
                              TextConstants.H7.apply(color: LightColor.black))
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget itemsCard(BuildContext context) {
    //   print(widget.cartDto?.shoppingCartItems?.length ?? 0);
    //if (cart?.shoppingCartItems?.length ?? 0 == 0) return Text('');
    return Column(
        children: widget.cartDto.shoppingCartItems
            .map((x) => getItemCardWidget(x))
            .toList());
  }
}

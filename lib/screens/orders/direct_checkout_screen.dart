import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_bloc.dart';
import 'package:bluebellapp/models/addToCart_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/models/shoppingCart_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/screens/orders/checkout_screen.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custome_alert_dialog.dart';
import 'package:flutter/material.dart';

class DirectCheckoutScreen extends StatefulWidget {
  AddToCartDto addToCartDto;
  ProductDto product;
  DirectCheckoutScreen({@required this.addToCartDto, @required this.product});

  @override
  _DirectCheckoutScreenState createState() => _DirectCheckoutScreenState();
}

class _DirectCheckoutScreenState extends State<DirectCheckoutScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool isDataLoaded = false;
  bool isOrderPlaced;
  Function onError;
  ProductBloc productBloc;
  AppBloc appBloc;
  ShoppingCartDto cartDto;
  List<ShoppingCartItem> cartItems = List<ShoppingCartItem>();
  ProductDto product = ProductDto();
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  getCartItems() {
    var productAttribute =  widget.addToCartDto.directOrderProductAttribute;
    product.pictureThumb = productAttribute.pictureThumb;
    product.name = productAttribute.name;
    cartDto = ShoppingCartDto(
        totalQuantity: 1,
        totalCost: productAttribute.price,
        shoppingCartItems: cartItems);
    setState(() {
      cartDto.shoppingCartItems.add(ShoppingCartItem(
        product: product,
        totalPrice: productAttribute.price,
      ));
      isDataLoaded = true;
    });
  }

  placeOrder(int billingAddressId, CustomProgressDialog pr) async {
    //print(billingAddressId);
    if (billingAddressId == null) {
      CustomAlertDialog.showNew(
          cntxt: scaffoldKey.currentContext,
          text: 'Warning !' + '\nEnter address info');
    } else if (billingAddressId != null &&
        cartDto.shoppingCartItems.length >= 1) {
      await pr.showDialog();
      scaffoldKey.currentContext
          .getRepo()
          .getCartRepository()
          .placeDirectOrder(
              cart: widget.addToCartDto,
              billingAddressId: billingAddressId,
              callBack: (data) async {
                setState(() {
                  isOrderPlaced = true;
                });
                await pr.hideDialog();
                Future.delayed(const Duration(milliseconds: 500), () {
                  showInSnackBar(data);
                });
              })
          .then((data) async {
        if (data.orderId > 0) {
          print("DATA => " + data.toString());
          setState(() {
            isOrderPlaced = true;
          });
          appBloc.add(
            OrderDetailViewEvent(
              orderId: data.orderId,
              returnState: AppStateAuthenticated(),
            ),
          );
        }

        await pr.hideDialog();
        appBloc.add(OrderDetailViewEvent(
            orderId: data.orderId, returnState: AppStateAuthenticated()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    appBloc = context.getAppBloc();
    //productBloc = context.getProductBloc();
    if (cartDto?.shoppingCartItems == null) {
      getCartItems();
    }
    return CheckoutPage(
      cartDto: cartDto,
      scaffoldKey: scaffoldKey,
      onPlaceOrder: placeOrder,
      isDataLoaded: isDataLoaded,
      isOrderPlaced: isOrderPlaced,
    );
  }
}

import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/order_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/constants/helper_constants/order_status.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/services/getDate_service.dart';
import 'package:flutter/material.dart';

class MyOrderCard extends StatelessWidget {
  final OrderDto obj;
  final AppState returnState;
  const MyOrderCard({this.obj, Key key, this.returnState}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      child: InkWell(
        onTap: () {
          context.addEvent(OrderDetailViewEvent(
              orderId: obj.id,
              returnState: returnState != null ? returnState : null));
        },
        child: Container(
          height: 100,
          padding: AppTheme.padding,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  LayoutConstants.sizedBox5H,
                  Text(
                    "Order #" + obj.id.toString(),
                    style: TextConstants.H5,
                  ),
                  Text(
                    "Date: " + getDateTime(date: obj.createdOnUtc),
                    style: TextConstants.P7,
                  ),
                  LayoutConstants.sizedBox10H,
                  RichText(
                    text: TextSpan(
                      text: 'Order Status: ',
                      style: TextConstants.H7.apply(color: Colors.black87),
                      children: <TextSpan>[
                        TextSpan(
                          text: OrderStatus.getName(obj.orderStatusId),
                          style:
                              TextConstants.H7.apply(color: LightColor.orange),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Icon(Icons.money_off),
                      LayoutConstants.sizedBox5H,
                      Text(GeneralStrings.PAYMENT_METHOD_CASH,
                          style: TextConstants.H6_5)
                    ],
                  ), //.padding(LayoutConstants.edgeInsets8),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Rs. " + obj.orderSubtotalInclTax.toString(),
                            style: TextConstants.H6
                                .apply(color: LightColor.orange),
                          )
                        ],
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

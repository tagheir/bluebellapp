import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/shoppingCart_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/screens/orders/checkout_screen.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custome_alert_dialog.dart';
import 'package:flutter/material.dart';

class CartCheckoutScreen extends StatefulWidget {
  @override
  _CartCheckoutScreenState createState() => _CartCheckoutScreenState();
}

class _CartCheckoutScreenState extends State<CartCheckoutScreen> {
  ShoppingCartDto cartDto = ShoppingCartDto();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool isDataLoaded = false;
  bool isOrderPlaced;
  Function onError;
  AppBloc appBloc;
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value ?? ''),
    ));
  }

  placeOrder(int billingAddressId, CustomProgressDialog pr) async {
    if (billingAddressId == null) {
      CustomAlertDialog.showNew(
          cntxt: scaffoldKey.currentContext,
          text: 'Warning !' + '\nEnter address info');
    } else if (billingAddressId != null &&
        cartDto.shoppingCartItems.length >= 1) {
      await pr.showDialog();
      scaffoldKey.currentContext
          .getRepo()
          .getCartRepository()
          .placeOrder(
              billingAddressId: billingAddressId,
              callBack: (data) async {
                setState(() {
                  isOrderPlaced = true;
                });
                await pr.hideDialog();
                Future.delayed(const Duration(milliseconds: 500), () {
                  showInSnackBar(data);
                });
              })
          .then((data) async {
        setState(() {
          isOrderPlaced = true;
        });
        await pr.hideDialog();
        appBloc.add(
          OrderDetailViewEvent(
            orderId: data,
            returnState: AppStateAuthenticated(),
          ),
        );
      });
    }
  }

  getCartItems() {
    appBloc.getRepo().getCartRepository().getMyCart(callBack: (data) {
      isDataLoaded = true;
      Future.delayed(const Duration(milliseconds: 500), () {
        showInSnackBar(data);
      });
    }).then((data) {
      if (data != null) {
        setState(() {
          cartDto = data;
          print("Cart items---------");
          print(cartDto.shoppingCartItems.length.toString());
          isDataLoaded = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    appBloc = context.getAppBloc();
    if (cartDto.shoppingCartItems == null) {
      getCartItems();
    }
    return CheckoutPage(
      cartDto: cartDto,
      scaffoldKey: scaffoldKey,
      onPlaceOrder: placeOrder,
      isDataLoaded: isDataLoaded,
      isOrderPlaced: isOrderPlaced,
    );
  }
}

import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/attribute_dto.dart';
import 'package:bluebellapp/models/order_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/repos/cart_repo.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/cart/cart_item_card.dart';
import 'package:bluebellapp/screens/shared/_layout_screen.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/network_cache_image.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:bluebellapp/services/getDate_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bluebellapp/services/getOrderStatus_service.dart';

class MyOrdersDetail extends StatefulWidget {
  final int orderId;
  final AppState returnState;
  MyOrdersDetail({this.orderId, this.returnState});
  @override
  _MyOrdersDetailState createState() => _MyOrdersDetailState();
}

class _MyOrdersDetailState extends State<MyOrdersDetail> {
  bool isDataLoaded = false;
  OrderDto orderDetail;
  AppBloc appBloc;
  CartRepository cartRepository;
  ProductAttribute dropAttr;
  ProductDto product = ProductDto();
  bool productLoaded = false;
  var attrVal;
  var dateVal;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  getOrderDetail() {
    print(widget.orderId);
    cartRepository
        .getOrderDetail(
            orderId: widget.orderId,
            callBack: (data) {
              setState(() {
                isDataLoaded = true;
              });
              Future.delayed(const Duration(milliseconds: 500), () {
                showInSnackBar(data);
              });
            })
        .then((data) {
      setState(() {
        orderDetail = data;
        isDataLoaded = true;
      });
    });
  }

  getDropDownAttribureValue(OrderItem orderItem) {
    cartRepository.getOrderItemDropDownValue(orderItem: orderItem).then((data) {
      setState(() {
        if (attrVal == null) {
          attrVal = data;
        }
      });
    });
  }

  getDateAttributeValue(OrderItem orderItem) {
    cartRepository.getOrderItemDateValue(orderItem: orderItem).then((data) {
      setState(() {
        if (dateVal == null) {
          dateVal = data;
        }
      });
    });
  }

  getProduct(OrderItem orderItem) {
    if (productLoaded == false) {
      appBloc
          .getRepo()
          .getServiceRepository()
          .getProductById(productId: orderItem.productId)
          .then((data) {
        setState(() {
          product = data;
          print(product.pictureThumb);
          productLoaded = true;
        });
      });
    }
  }

  Widget _title() {
    return Container(
      margin: AppTheme.h2Padding,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Order #' + orderDetail.id.toString(),
                  style: TextConstants.H4),
            ],
          ),
        ],
      ),
    );
  }

  Widget getInfoRow({Widget leading, Widget trailing}) {
    var childs = List<Widget>();
    if (leading != null) {
      childs.add(Expanded(
        flex: 1,
        child: leading,
      ));
    }
    if (trailing != null) {
      childs.add(Expanded(
        flex: 1,
        child: trailing,
      ));
    }
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 5),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: childs),
    );
  }

  Widget orderBasicDetailCard() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getInfoRow(
              trailing: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                Icon(
                  Icons.settings,
                  size: 20,
                  color: LightColor.orange,
                ),
                Text(getOrderStatus(statusId: orderDetail.orderStatusId),
                    style: TextConstants.H7.apply(color: LightColor.black)),
              ])),
          getInfoRow(
              leading: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.calendar_today, size: 15),
              SizedBox(
                width: 3,
              ),
              Text(getDateTime(date: orderDetail.createdOnUtc),
                  style: TextConstants.H8.apply(color: LightColor.black)),
            ],
          )),
          SizedBox(
            height: 15,
          ),
          getInfoRow(
            leading: Text('Delivery Address',
                style: TextConstants.H7.apply(color: LightColor.lightBlack)),
          ),
          getInfoRow(
            leading: Text(orderDetail.billingAddress.address1,
                style: TextConstants.H8.apply(color: LightColor.black)),
          ),
          SizedBox(
            height: 15,
          ),
          getInfoRow(
            leading: Text('Payment Method',
                style: TextConstants.H7.apply(color: LightColor.lightBlack)),
          ),
          getInfoRow(
              leading: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.money_off, size: 15),
              SizedBox(
                width: 3,
              ),
              Text('Cash',
                  style: TextConstants.H8.apply(color: LightColor.black)),
            ],
          )),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  Widget getOrderItemRow(OrderItem orderItem) {
    //CartItemCard()

    //getDropDownAttribureValue(orderItem);
    //getDateAttributeValue(orderItem);
    var product = orderItem.productDto;
    // var listAttributes = List<Widget>();
    // for (var attribute in orderItem.attributes.productAttribute) {
    //   attribute.
    // }
    // await scaffoldKey.currentContext
    //     .getRepo()
    //     .getServiceRepository()
    //     .getProductById(productId: orderItem.productId);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: _getImage(product?.pictureThumb),
          ),
          Expanded(
            flex: 7,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, top: 5, bottom: 8),
              child: _getInfo(orderItem.productName, orderItem.priceInclTax),
            ),
          ),
        ],
      ),
    );

    return ListTile(
      title: Wrap(
        children: <Widget>[
          Text(orderItem.productName,
              style: TextConstants.H7.apply(
                color: LightColor.black,
              )),
        ],
      ),
      subtitle: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(attrVal ?? '',
              style: TextConstants.H9.apply(
                color: LightColor.lightBlack,
              )),
          SizedBox(
            height: 10,
          ),
          dateVal == null
              ? Text('')
              : Wrap(
                  children: <Widget>[
                    Icon(Icons.calendar_today,
                        color: LightColor.orange, size: 12),
                    SizedBox(
                      width: 3,
                    ),
                    Text(dateVal ?? '',
                        style:
                            TextConstants.H9.apply(color: LightColor.orange)),
                  ],
                ),
        ],
      ),
      trailing: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Wrap(
            children: <Widget>[
              Text('Rs ' + orderItem.priceInclTax.toString(),
                  style: TextConstants.H8.apply(color: LightColor.orange)),
            ],
          ),
        ],
      ),
      leading: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: NetworkCacheImage(
          imageUrl: product?.pictureThumb ?? "https://placehold.it/100",
          altImageUrl: "https://placehold.it/100",
          width: 70,
          height: 90,
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
    ).padding(EdgeInsets.only(bottom: 10.0));
  }

  Widget _getInfo(String name, double price) {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Wrap(
            children: <Widget>[
              Text(
                name ?? '',
                style: TextConstants.H6.apply(
                  color: LightColor.darkGrey,
                ),
              )
            ],
          ),
          Wrap(
            children: <Widget>[
              Text(
                'Rs. ' + price?.toString() ?? '0',
                style: TextConstants.H6_5.apply(
                  color: LightColor.orange,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _getImage(String pictureThumb) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      child: NetworkCacheImage(
        altImageUrl: "https://placehold.it/130",
        imageUrl: pictureThumb ?? "https://placehold.it/130",
        width: 110.0,
        height: 90.0,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget getCartItemRows() {
    var cartItemsWidgets = List<Widget>();
    cartItemsWidgets.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text('Order Items',
          style: TextConstants.H6.apply(color: LightColor.lightBlack)),
    ));

    orderDetail.orderItems.orderItem.forEach((o) async {
      cartItemsWidgets.add(getOrderItemRow(o));
    });
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: cartItemsWidgets);
  }

  Widget getCartItemsCard() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 5),
      child: Card(child: getCartItemRows()),
    );
  }

  Widget orderPriceInfo() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              getInfoRow(
                leading: Text('Subtotal',
                    style:
                        TextConstants.H7.apply(color: LightColor.lightBlack)),
                trailing: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text('Rs ' + orderDetail.orderSubtotalExclTax.toString(),
                        style: TextConstants.H7.apply(color: LightColor.black)),
                  ],
                ),
              ),
              getInfoRow(
                leading: Text('Tax',
                    style:
                        TextConstants.H7.apply(color: LightColor.lightBlack)),
                trailing: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                        'Rs ' +
                            (orderDetail.orderSubtotalInclTax -
                                    orderDetail.orderSubtotalExclTax)
                                .toString(),
                        style: TextConstants.H7.apply(color: LightColor.black)),
                  ],
                ),
              ),
              getInfoRow(
                leading: Text('Total',
                    style:
                        TextConstants.H7.apply(color: LightColor.lightBlack)),
                trailing: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text('Rs ' + orderDetail.orderSubtotalInclTax.toString(),
                        style:
                            TextConstants.H5.apply(color: LightColor.orange)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appBloc = context.getAppBloc();
    cartRepository = cartRepository == null
        ? appBloc.getRepo().getCartRepository()
        : cartRepository;
    if (isDataLoaded == false) {
      getOrderDetail();
    }
    var body = List<Widget>();
    if (isDataLoaded != true) {
      body.add(LoadingIndicator().center());
    } else if (isDataLoaded == true) {
      body.add(_title());
      body.add(orderBasicDetailCard());
      body.add(getCartItemsCard());
      body.add(orderPriceInfo());
    }

    return LayoutScreen(
      defaultAppBar: false,
      showDefaultBackButton: true,
      showBottomNav: false,
      scaffoldKey: scaffoldKey,
      body: Container(child: ListView(children: body)),
    );
  }
}

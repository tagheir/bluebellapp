import 'package:bluebellapp/screens/shared/_layout_screen.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:flutter/material.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutScreen(
      defaultAppBar: false,
      showBottomNav: false,
      body: Center(
        child: LoadingIndicator(),
      ),
    );
  }
}

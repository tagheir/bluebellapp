import 'package:bluebellapp/models/customer_address.dart';
import 'package:bluebellapp/screens/home/junaid/address_list.dart';
import 'package:flutter/material.dart';

class ChooseAddressesList extends StatefulWidget {
  @override
  _AddressesListState createState() => _AddressesListState();
}

class _AddressesListState extends State<ChooseAddressesList> {
  int selectedRadioTile = 0;
  // List<Address> addresses = List<Address>();
  //Address obj=address;
  List<Address> address = [
    Address(
        address1: "Home", address2: "Fast University near Faisal Town\nLahore"),
    Address(address1: "Home", address2: "Defence Housing Authority\nLahore"),
    Address(address1: "Home", address2: "Dha Phase 6,Sector C\nLahore"),
    Address(
        address1: "Home",
        address2: "Lahore University near Thokar Niaz Baig\nLahore"),
    Address(
        address1: "Home", address2: "Fast University near Faisal Town\nLahore"),
    Address(address1: "Home", address2: "Defence Housing Authority\nLahore"),
    Address(address1: "Home", address2: "Dha Phase 6,Sector C\nLahore"),
    Address(
        address1: "Home",
        address2: "Lahore University near Thokar Niaz Baig\nLahore"),
  ];

  //addresses.add(this.groupValue),

  int groupValue = -1;

  // int customselectedRadioTile = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: address.length,
      itemBuilder: (context, int i) => AddressListTile(
        add: address[i],
        value: i,
        groupValue: groupValue,
      ),
    ));
  }
}

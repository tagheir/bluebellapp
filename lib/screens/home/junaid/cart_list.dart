import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'checkout_page.dart';

class CartList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "My Cart",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Stack(children: <Widget>[
        ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                CartListCard(),
                CartListCard(),
                CartListCard(),
                CartListCard(),
                CartListCard(),
                SizedBox(
                  height: 60,
                )
              ],
            ),
          ],
        ),
        new Positioned(
            //left: 2.0,
            top: MediaQuery.of(context).size.height - 140,
            //top: MediaQuery.of(context).size.height - 128,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 60,
                  width: MediaQuery.of(context).size.width,
                  color: ThemeScheme.lightTheme.primaryColor,
                ),
                Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        "Price: Rs 1400",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                    SizedBox(
                      width: 80,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    CheckoutPage()));
                      },
                      child: Container(
                        margin: EdgeInsets.all(8),
                        height: 40,
                        width: 120,
                        color: Colors.black,
                        child: Center(
                          child: Text(
                            'Checkout',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )),
      ]),
    );
  }

  Container CartListCard() {
    return Container(
      height: 150,
      width: double.infinity,
      // color: Colors.yellow,
      child: Card(
        elevation: 8,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                color: Colors.pink,
                child: Image.network(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVpTipbz8cRwVkCRvZEDpci2aFKQwE_NnHgkAcsGH7g58teuAs'),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Deep Cleaning',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        'Price:Rs1400',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Text('3 Bedroom Apartements'),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('05-03-2020    10 A.M-12 P.M'),
                        Icon(
                          Icons.delete,
                          color: Colors.grey,
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

//  Container PackagesListCompactView(String package_name) {
//    return Container(
//      height: 220,
//      //color: Colors.red,
//      child: Card(
//        elevation: 8,
//        clipBehavior: Clip.antiAlias,
//        shape:
//            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
//        child: Column(
//          children: <Widget>[
//            SizedBox(
//              height: 16,
//            ),
//            Image.network(
//              "assets/images/package.svg",
//              color: Colors.black,
//              width: 40.0,
//              height: 40.0,
//            ),
//            SizedBox(
//              height: 16,
//            ),
//            Text(
//              package_name,
//              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40.0),
//            ),
//            Padding(
//              padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
//              child: Divider(
//                color: Color(0XFFEA7623),
//                height: 5,
//                thickness: 3,
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
//              child: Text(
//                'Starting at',
//                style: TextStyle(fontSize: 20.0),
//              ),
//            ),
//            SizedBox(
//              height: 5,
//            ),
//            RichText(
//              text: TextSpan(
//                  text: 'AED 121',
//                  style: TextStyle(color: Color(0XFFEA7623)),
//                  children: <TextSpan>[
//                    TextSpan(
//                        text: '/month', style: TextStyle(color: Colors.black))
//                  ]),
//            )
//
//            //Image.network(
//            //  'https://cdn.pixabay.com/photo/2013/07/21/13/00/rose-165819__340.jpg'),
//          ],
//        ),
//      ),
//    );
//  }
}
//jpg

//width: MediaQuery.of(context).size.width,

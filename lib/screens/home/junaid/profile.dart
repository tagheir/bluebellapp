import 'dart:io';

import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  File _selectedfile;
  FileImage _image;

  Widget getImageWidget() {
    if (_selectedfile != null) {
      return Image.file(
        _selectedfile,
        height: 100,
        width: 100,
        fit: BoxFit.cover,
      );
    } else {
      return Image.network(
          'https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80');
    }
  }

  var maskFormatter = new MaskTextInputFormatter(
      mask: ' ##########', filter: {"#": RegExp(r'[0-9]')});

  getImage(ImageSource source) async {
    File image = await ImagePicker.pickImage(source: source);
    //This function is for getting the image from
    // camera or gallery
    if (image != null) {
      File cropped = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatio: CropAspectRatio(ratioX: 0.5, ratioY: 0.5),
        //compressQuality: 100,
        compressQuality: 50,
        maxHeight: 100,
        maxWidth: 100,
        cropStyle: CropStyle.rectangle,
        compressFormat: ImageCompressFormat.jpg,
        androidUiSettings: AndroidUiSettings(
            toolbarWidgetColor: Colors.white,
            toolbarColor: ThemeScheme.lightTheme.primaryColor,
            toolbarTitle: "Bluebell",
            statusBarColor: ThemeScheme.lightTheme.primaryColor,
            backgroundColor: Colors.white),
      );
      this.setState(() {
        _selectedfile = cropped;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        // margin: EdgeInsets.all(8),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(left: 24, right: 24, top: 24),
          child: ListView(
            children: [
              Container(
                  child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        //color: Colors.red,
                        width: 100,
                        height: 100,
                        child: getImageWidget(),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                          // color: Colors.red
                        ),
                      ),
                      Positioned(
                        left: 80,
                        top: 77,
                        child: InkWell(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Container(
                                    height: 190,
                                    width: 100,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          height: 50,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          // color: Colors.deepOrange,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8, top: 16),
                                            child: Text(
                                              "Choose Picture From",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                  color: ThemeScheme
                                                      .lightTheme.primaryColor),
                                            ),
                                          ),
                                        ),
                                        Divider(
                                          thickness: 3,
                                          color: ThemeScheme
                                              .lightTheme.primaryColor,
                                        ),
                                        Container(
                                          height: 40,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: InkWell(
                                              onTap: () {
                                                getImage(ImageSource.gallery);
                                              },
                                              child: Row(
                                                children: <Widget>[
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Image.network(
                                                      "https://purepng.com/public/uploads/large/purepng.com-photos-icon-android-kitkatsymbolsiconsapp-iconsandroid-kitkatandroid-44-721522597661gzej2.png"),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Text(
                                                    "From Gallery",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  )
                                                ],
                                              )),
                                        ),
                                        Divider(
                                          color: ThemeScheme
                                              .lightTheme.primaryColor,
                                        ),
                                        Container(
                                          height: 40,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: InkWell(
                                            onTap: () {
                                              getImage(ImageSource.camera);
                                            },
                                            child: Row(
                                              children: <Widget>[
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Image.network(
                                                    "https://icons.iconarchive.com/icons/dtafalonso/android-lollipop/256/Camera-icon.png"),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  "Camera",
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                          child: Container(
                            // height: 55,
                            decoration: const ShapeDecoration(
                              color: Color(0xFFEA7623),
                              shape: CircleBorder(),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Icon(
                                Icons.edit,
                                color: Colors.white,

                                //color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                    overflow: Overflow.visible,
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      TextFormField(
                        autofocus: true,
                        keyboardType: TextInputType.text,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(15),
                        ],
                        decoration: InputDecoration(labelText: 'Full Name'),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(16),
                        ],
                        decoration: InputDecoration(labelText: 'Email'),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        // maxLength: 2,
                        autofocus: true,
                        initialValue: "+92 ",
                        maxLength: 14,
                        decoration: InputDecoration(
                          labelText: 'Phone number',
                        ),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.numberWithOptions(),
                        obscureText: true,
                        autofocus: true,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(15),
                        ],
                        decoration: InputDecoration(labelText: 'Password'),
                      ),
                    ],
                  ),
                  SizedBox(
                    // height: MediaQuery.of(context).size.height - 600,
                    height: 35,
                  ),
                  Card(
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: ButtonTheme(
                      height: 50,
                      minWidth: 400,
                      child: RaisedButton(
                        onPressed: () {},
                        textColor: Colors.white,
                        child: Text(
                          'Save',
                          style: TextStyle(fontSize: 20),
                        ),
                        color: ThemeScheme.lightTheme.primaryColor,
                      ),
                    ),
                  )
                ],
              )),
            ],
//            child: Column(
//              mainAxisSize: MainAxisSize.max,
//              children: <Widget>[
//                TextFormField(
//                  decoration: InputDecoration(labelText: 'Name on card'),
//                ),
//                SizedBox(
//                  height: 15,
//                ),
//                TextFormField(
//                  decoration: InputDecoration(labelText: 'Enter card number'),
//                ),
//                SizedBox(
//                  height: 5,
//                ),
//                Padding(
//                  padding: const EdgeInsets.only(right: 8.0),
//                  child: Row(
//                    children: <Widget>[
//                      Expanded(
//                        child: Container(
//                          child: TextFormField(
//                            decoration: InputDecoration(labelText: 'MM-YY'),
//                          ),
//                        ),
//                      ),
//                      Expanded(
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 32),
//                          child: TextFormField(
//                            decoration: InputDecoration(labelText: 'CVC'),
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//                Align(
//                  alignment: Alignment.bottomRight,
//                  child: RaisedButton(
//                    onPressed: () {},
//                    textColor: Colors.white,
//                    child: Text(
//                      'Apply',
//                      style: TextStyle(fontSize: 20),
//                    ),
//                    color: ThemeScheme.lightTheme.primaryColor,
//                  ),
//                )
//              ],
//            ),
          ),
        ),
      ),
    );
  }
}

//https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'),
//

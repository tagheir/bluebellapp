import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/material.dart';

class HomeAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeScheme.lightTheme.primaryColor,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(8),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              children: [
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        TextFormField(
                          onTap: () {
                            FocusScope.of(context).unfocus();
                          },
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(labelText: 'Building'),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Street'),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Area'),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: '(Option)Floor Unit'),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: '(Option)Note to rider'),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: RaisedButton(
                        onPressed: () {},
                        textColor: Colors.white,
                        child: Text('Apply'),
                        color: ThemeScheme.lightTheme.primaryColor,
                      ),
                    )
                  ],
                ))
              ],
//            child: Column(
//              mainAxisSize: MainAxisSize.max,
//              children: <Widget>[
//                TextFormField(
//                  decoration: InputDecoration(labelText: 'Name on card'),
//                ),
//                SizedBox(
//                  height: 15,
//                ),
//                TextFormField(
//                  decoration: InputDecoration(labelText: 'Enter card number'),
//                ),
//                SizedBox(
//                  height: 5,
//                ),
//                Padding(
//                  padding: const EdgeInsets.only(right: 8.0),
//                  child: Row(
//                    children: <Widget>[
//                      Expanded(
//                        child: Container(
//                          child: TextFormField(
//                            decoration: InputDecoration(labelText: 'MM-YY'),
//                          ),
//                        ),
//                      ),
//                      Expanded(
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 32),
//                          child: TextFormField(
//                            decoration: InputDecoration(labelText: 'CVC'),
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//                Align(
//                  alignment: Alignment.bottomRight,
//                  child: RaisedButton(
//                    onPressed: () {},
//                    textColor: Colors.white,
//                    child: Text(
//                      'Apply',
//                      style: TextStyle(fontSize: 20),
//                    ),
//                    color: ThemeScheme.lightTheme.primaryColor,
//                  ),
//                )
//              ],
//            ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/home/junaid/cart_list.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_html/flutter_html.dart';

class ProductDetailPage extends StatefulWidget {
  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  var cateogaries = ['1 Rooms', '2 Rooms', '3 Rooms', '4 Rooms', '5 Rooms'];

  var _currentItemSelected = '1 Rooms';
  var selecteddate = "Schedule a task on time";

  DateTime _dateTime;

  DateTime _currentdate = new DateTime.now();

  Future<Null> _selectdate() async {
    //print('Agea hu ma');
    // print(context.toString());
    final DateTime _seldate = await showDatePicker(
        //   context: context,
        initialDate: _currentdate,
        firstDate: DateTime(1990),
        lastDate: DateTime(2022));
  }

  @override
  Widget build(BuildContext context) {
    {
      // String formated_date = new DateFormat.yMMMd().format(selecteddate);
      return Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeScheme.lightTheme.primaryColor,
          title: Text(
            "AC Service",
            style: TextStyle(color: Colors.white),
          ),
          iconTheme: IconThemeData(color: Colors.white),
        ),
        backgroundColor: Colors.blueGrey[50],
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  color: Colors.red,
                  width: double.infinity,
                  height: 250,
                  child: FadeInImage.assetNetwork(
                    alignment: Alignment.topCenter, // add this
                    placeholder: 'dummyimage',
                    image:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVpTipbz8cRwVkCRvZEDpci2aFKQwE_NnHgkAcsGH7g58teuAs',
                    fit: BoxFit.fitWidth,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(color: Colors.white),
                  // width: MediaQuery.of(context),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8, right: 16, bottom: 16, top: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Deep Cleaning Occupied',
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(height: 20),
                        Text(
                          'Deep Cleaning Variation',
                          style: TextStyle(color: Colors.grey),
                        ),
                        Container(
                          //width: 300,
                          child: DropdownButton<String>(
                            isExpanded: true,
                            items: cateogaries.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem));
                            }).toList(),
                            onChanged: (String newValueSelected) {
                              setState(() {
                                this._currentItemSelected = newValueSelected;
                              });
                              //Your code to execute when menu item is selected by the user
                            },
                            value: _currentItemSelected,
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          'Selected Slot',
                          style: TextStyle(color: Colors.grey),
                        ),
                        FlatButton(
                          onPressed: () async {
                            DateTime userselecteddate = await showDatePicker(
                                context: context,
                                initialDate: _currentdate,
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2022));
                            setState(() {
                              //selecteddate.d
                              String formated_date = new DateFormat.yMMMd()
                                  .format(userselecteddate);
                              selecteddate = formated_date.toString();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            //color: Colors.red,
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  side:
                                      BorderSide(color: Colors.grey, width: 1)),
                              child: Center(
                                child: Text(
                                  selecteddate,
                                  style: TextStyle(fontSize: 15),
                                ),
                              ),
                            ),
//
                          ),
                        ),
                        // Text('Date: $formated_date')
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, top: 8, bottom: 80),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Descriptipon',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        Html(padding: EdgeInsets.all(12.0), data: """
             <div>
            <p>
           </p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">What’s In?</p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">• Dust/damp-wipe all doors, switches.<br>• Bathrooms and toilets: Scrub floors, tiled walls, sanitary wares and fittings, wash down mirrors.<br>• Kitchen: scrub floors, tiled walls, sink and worktops surfaces, wipe down cabinets.<br>• Clean balcony and damp-wipe handrails if applicable.<br>• Clean A/C vents and skirting.<br>• Wash down internal windows and frames (up to a maximum height of 1.6 meters).<br>• Scrub the hard floors in all the rooms.</p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">Note:</p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">Standard Prices are valid for the following areas only</p><ul style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 14px;"><li>Arabian Ranches</li><li>Jumeirah Circle</li><li>Jumeirah Park</li><li>Palm Jumeirah</li><li>Meadows</li><li>Springs</li><li>Hattan</li></ul><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">Prices for other areas might vary and sight survey is required for below areas/on customized villas</p><ul style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 14px;"><li>Umm Suqueim</li><li>Jumeirah</li><li>Mirdif</li><li>Al Barsha</li><li>Emirates Hills</li></ul>
             <p></p>
										  </div>
                                      
            """),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            new Positioned(
              //left: 2.0,
              top: MediaQuery.of(context).size.height - 140,
              child: new Stack(
                children: <Widget>[
                  Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width,
                    color: ThemeScheme.lightTheme.primaryColor,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          'Rs 2800',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 110,
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      CartList()),
                            );
                          },
                          child: Container(
                            margin: EdgeInsets.all(8),
                            height: 60,
                            width: 120,
                            color: Colors.black,
                            child: Center(
                              child: Text(
                                'Checkout',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
  }
}

import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/home/junaid/homescreen.dart';
import 'package:bluebellapp/screens/widgets/views_widgets/drawer.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'cart_list.dart';
import 'packages_list.dart';
import 'facilities_management_services_grid.dart';
import 'landscape_services_grid.dart';
import 'products_grid.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _page = 2;
  @override
  Widget build(BuildContext context) {
    //***my code****//
    List<Widget> ScreenDisplay = [
      PackageList(),
      FacilitiesManagementServicesGrid(),
      HomeWidget(call: () {
        setState(() {
          _page = 0;
          //index=0;
        });
      }, call1: () {
        setState(() {
          _page = 1;
          //index=0;
        });
      }, call2: () {
        setState(() {
          _page = 3;
        });
      }, call3: () {
        setState(() {
          _page = 4;
        });
      }),
      LandscapeServicesGrid(),
      ProductsGrid(),
    ];

    //*****************App Bar***************//
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        title: Text('Bluebell', style: TextStyle(color: Colors.white)),
        iconTheme: IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.add_shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CartList()),
                );
              })
        ],
      ),

      // body: HomeWidget(),
      body: ScreenDisplay[_page],
      floatingActionButton: Padding(
        padding: EdgeInsets.only(bottom: 30.0),
        child: FloatingActionButton(
          tooltip: 'problem',
          backgroundColor: Colors.white,
          child: Icon(
            Icons.add_shopping_cart,
            color: ThemeScheme.lightTheme.primaryColor,
          ),
          onPressed: () {},
        ),
      ),

      //***********Bottom Navigation Bar**************//

      bottomNavigationBar: CurvedNavigationBar(
        height: 55.0,
        index: _page,
        color: ThemeScheme.lightTheme.primaryColor,
        backgroundColor: Colors.white,
        items: <Widget>[
          Icon(
            Icons.search,
            size: 30.0,
            color: ThemeScheme.iconColorNavBar,
          ),
          Icon(
            Icons.sentiment_dissatisfied,
            size: 30.0,
            color: ThemeScheme.iconColorNavBar,
          ),
          Icon(Icons.home, size: 40.0, color: ThemeScheme.iconColorNavBar),
          Icon(Icons.sentiment_very_satisfied,
              size: 40.0, color: ThemeScheme.iconColorNavBar),
          Icon(Icons.exit_to_app,
              size: 40.0, color: ThemeScheme.iconColorNavBar),
        ],
        animationDuration: Duration(milliseconds: 600),
        animationCurve: Curves.easeInOutCirc,
        onTap: (index) {
          setState(() {
            _page = index;
          });
        },
      ),
      drawer: Drawer(child: AppDrawer()),
//
//
    );
  }
}

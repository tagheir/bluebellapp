import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UploadImage {
  File file;

  Map Function(File) callback;

  UploadImage(BuildContext cntxt, Map Function(File) func) {
    this.callback = func;
    this.file = null;
    this.selectImageSource(cntxt);
  }

  selectImageSource(BuildContext cntxt) {
    this.file = null;
    showModalBottomSheet(
        context: cntxt,
        builder: (BuildContext cntxt) {
          return Container(
            height: 150.0,
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text(
                  'Pick an image',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10.0,
                ),
                FlatButton(
                  child: Text('Use Camera'),
                  onPressed: () {
                    getImage(cntxt, ImageSource.camera);
                  },
                ),
                FlatButton(
                  child: Text('Use Gallery'),
                  onPressed: () {
                    getImage(cntxt, ImageSource.gallery);
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<dynamic> getImage(BuildContext context, ImageSource source) async {
    File file = await ImagePicker.pickImage(source: source);
    this.file = file;
    this.callback(file);
    Navigator.pop(context);
  }
}

import 'package:bluebellapp/screens/widgets/project_widgets/product_compact_view.dart';

import 'package:bluebellapp/screens/widgets/project_widgets/card_for_grid.dart';
import 'package:flutter/material.dart';

class ProductSubCateogaryView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Products Cateogaries",
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Sub Cateogaries",
              style: TextStyle(fontSize: 20),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              height: 150.0,
              //   color: Colors.pinkAccent,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  ProductCompactView(),
                  ProductCompactView(),
                  ProductCompactView(),
                  ProductCompactView(),
                  ProductCompactView(),
                  ProductCompactView(),
                  ProductCompactView()
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Products",
              style: TextStyle(fontSize: 20),
            ),
          ),
          Expanded(
            flex: 3,
            child: GridView.count(
              mainAxisSpacing: 20.0,
              crossAxisSpacing: 20.0,
              padding: EdgeInsets.all(10.0),
              crossAxisCount: 2,
              children: <Widget>[
                CardForGrid(
                    title: 'Electrical Service',
                    url:
                        'https://i.ytimg.com/vi/Hd3iBpUpgeg/maxresdefault.jpg'),
                CardForGrid(
                    title: 'Plumber',
                    url:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSThrrIk0krJXM5AybXB0pZJmQQZpzyPjWrRE1XyXq_lQ-fZj5N'),
                CardForGrid(
                    title: 'Pest Control',
                    url:
                        'https://image.shutterstock.com/image-vector/pest-control-harmful-insects-rodents-260nw-465458375.jpg'),
                CardForGrid(
                    title: 'Ac Service',
                    url:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVpTipbz8cRwVkCRvZEDpci2aFKQwE_NnHgkAcsGH7g58teuAs'),
                CardForGrid(
                    title: 'Handy Man ',
                    url:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT4GFi7kMwy6uXdxlFOPv1qvPKXXo0GiuAh_vFriHeuqI2Bb4tV'),
                CardForGrid(
                  title: 'Cleaning Services',
                  url:
                      'https://png.pngtree.com/element_our/sm/20180415/sm_5ad3472717767.jpg',
                ),
                CardForGrid(
                    title: 'Ac Service',
                    url:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVpTipbz8cRwVkCRvZEDpci2aFKQwE_NnHgkAcsGH7g58teuAs'),
                CardForGrid(
                    title: 'Handy Man ',
                    url:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT4GFi7kMwy6uXdxlFOPv1qvPKXXo0GiuAh_vFriHeuqI2Bb4tV'),
                CardForGrid(
                  title: 'Cleaning Services',
                  url:
                      'https://png.pngtree.com/element_our/sm/20180415/sm_5ad3472717767.jpg',
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

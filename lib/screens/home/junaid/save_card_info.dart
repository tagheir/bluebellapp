import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class SaveCardInfo extends StatelessWidget {
  var maskFormatter = new MaskTextInputFormatter(
      mask: '##/##', filter: {"#": RegExp(r'[0-9]')});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeScheme.lightTheme.primaryColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        elevation: 0.0,
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        title: Text(
          'Save Card info',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(8),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              Expanded(
                  child: Container(
                      // color: Colors.green,
                      child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          keyboardType: TextInputType.number,
                          autofocus: true,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(15),
                          ],
                          decoration:
                              InputDecoration(labelText: 'Name on Card'),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.phone,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(16),
                          ],
                          decoration: InputDecoration(labelText: 'Card number'),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 24.0),
                                child: TextFormField(
                                  inputFormatters: [maskFormatter],
                                  keyboardType: TextInputType.datetime,
                                  decoration:
                                      InputDecoration(labelText: 'MM/YY'),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 32.0),
                                child: TextFormField(
                                  keyboardType: TextInputType.phone,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(3),
                                  ],
                                  decoration: InputDecoration(labelText: 'CVC'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: RaisedButton(
                      onPressed: () {},
                      textColor: Colors.white,
                      child: Text('Apply'),
                      color: ThemeScheme.lightTheme.primaryColor,
                    ),
                  )
                ],
              )))
            ],
//            child: Column(
//              mainAxisSize: MainAxisSize.max,
//              children: <Widget>[
//                TextFormField(
//                  decoration: InputDecoration(labelText: 'Name on card'),
//                ),
//                SizedBox(
//                  height: 15,
//                ),
//                TextFormField(
//                  decoration: InputDecoration(labelText: 'Enter card number'),
//                ),
//                SizedBox(
//                  height: 5,
//                ),
//                Padding(
//                  padding: const EdgeInsets.only(right: 8.0),
//                  child: Row(
//                    children: <Widget>[
//                      Expanded(
//                        child: Container(
//                          child: TextFormField(
//                            decoration: InputDecoration(labelText: 'MM-YY'),
//                          ),
//                        ),
//                      ),
//                      Expanded(
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 32),
//                          child: TextFormField(
//                            decoration: InputDecoration(labelText: 'CVC'),
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//                Align(
//                  alignment: Alignment.bottomRight,
//                  child: RaisedButton(
//                    onPressed: () {},
//                    textColor: Colors.white,
//                    child: Text(
//                      'Apply',
//                      style: TextStyle(fontSize: 20),
//                    ),
//                    color: ThemeScheme.lightTheme.primaryColor,
//                  ),
//                )
//              ],
//            ),
          ),
        ),
      ),
    );
  }
}

import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';

class SignUpCodeScreen extends StatefulWidget {
  @override
  _SignUpCodeScreenState createState() => _SignUpCodeScreenState();
}

class _SignUpCodeScreenState extends State<SignUpCodeScreen> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: ThemeScheme.lightTheme.primaryColor));
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        leading: Icon(Icons.arrow_back_ios),
        elevation: 0,
        title: Text(
          "Pin Code",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Container(
            height: 150,
            //width: 350,
            //color: Colors.grey[300],
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: PinEntryTextField(
                  fields: 6,
                  showFieldAsBox: true,
                  onSubmit: (String pin) {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Pin"),
                            content: Text('Pin entered is $pin'),
                          );
                        }); //end showDialog()
                  }, // end onSubmit
                ),
              ), // end PinEntryTextField()
            ), // end Padding()
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            height: 50,
            width: 250,
            decoration: BoxDecoration(
              color: Color(0xffEA7623),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Center(
              child: Text(
                "Verify",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ],
      ), // end Co,
    );
  }
}

import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyOrdersDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
//        leading: Icon(
//          Icons.arrow_back,
//          color: Colors.white,
//        ),
        title: Text(
          "Order Details",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Card(
              elevation: 8,
              child: Container(
                //constraints: new BoxConstraints(minHeight: 150.0, maxHeight: 200),
                height: 250,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 6,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, left: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Wrap(
                              children: <Widget>[
                                Text(
                                  "Order ID: ",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 3.0),
                                  child: Text(
                                    "001",
                                    style: TextStyle(fontSize: 15),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Wrap(
                              children: <Widget>[
                                Text(
                                  "Date: ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Colors.grey[500]),
                                ),
                                Text(
                                  "2-03-2020",
                                  style: TextStyle(color: Colors.grey[500]),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 60,
                            ),
                            RichText(
                              text: TextSpan(
                                text: 'Order Status: ',
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                    color: ThemeScheme.lightTheme.primaryColor),
                                children: <TextSpan>[
                                  TextSpan(
                                    text: "Completed",
                                    style: TextStyle(
                                        //fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                ],
                              ),
                            ),
                            Divider(),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Shipping Address: ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[500]),
                            ),
                            Text(
                              "182 DD Dha,Phase 4,Lahore",
                              style: TextStyle(color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Billing Address: ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[500]),
                            ),
                            Text(
                              "182 DD Dha,Phase 4,Lahore",
                              style: TextStyle(color: Colors.grey[500]),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Row(
                              children: <Widget>[
//                                SizedBox(
//                                  height: 40,
//                                ),
//                                SizedBox(
//                                  width: 15,
//                                ),
                                Container(
                                  height: 25,
                                  child: Image.network(
                                      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWjS0hD66RXw1AhcZL2SK2prpk9Cl0hMeMbRtDmlQlYZ0qfXb6'),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  "Credit Card",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 100,
                            //color: Colors.green,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 25,
                                ),
                                Row(
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          "Subtotal: ",
                                          style: TextStyle(
                                              color: Colors.grey[500],
                                              fontWeight: FontWeight.w500,
                                              fontSize: 15),
                                        ),
                                        Text(
                                          "Taxes: ",
                                          style: TextStyle(
                                              color: Colors.grey[500],
                                              fontWeight: FontWeight.w500,
                                              fontSize: 15),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          "Total: ",
                                          style: TextStyle(
                                              color: Colors.grey[500],
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "500000",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color: Colors.grey[500],
                                              fontSize: 15),
                                        ),
                                        Text(
                                          "450000",
                                          style: TextStyle(
                                              color: Colors.grey[500],
                                              fontWeight: FontWeight.w500,
                                              fontSize: 15),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          "50000",
                                          style: TextStyle(
                                              color: Colors.grey[500],
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18),
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Divider()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Card(
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Order Items",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: ThemeScheme.lightTheme.primaryColor),
                  ),
                ),
              ),
            ),
            CardofCart(),
            CardofCart(),
            CardofCart(),
            CardofCart(),
          ],
        ),
      ),
    );
  }
}

class CardofCart extends StatelessWidget {
  const CardofCart({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: double.infinity,
      // color: Colors.yellow,
      child: Card(
        elevation: 4,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                color: Colors.pink,
                child: Image.network(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVpTipbz8cRwVkCRvZEDpci2aFKQwE_NnHgkAcsGH7g58teuAs'),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Deep Cleaning',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('3 Bedroom Apartements'),
                    SizedBox(
                      height: 50,
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        'Price:Rs1400',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: ThemeScheme.lightTheme.primaryColor),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/material.dart';

import 'my_orders_ui.dart';

class MyOrdersUIDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        title: Text(
          "My Orders",
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: MyOrdersUI(),
    );
  }
}

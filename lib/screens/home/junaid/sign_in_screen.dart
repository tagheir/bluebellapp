import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NewSignInScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: ThemeScheme.lightTheme.primaryColor));
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeScheme.lightTheme.primaryColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        leading: Icon(Icons.arrow_back_ios),
        elevation: 0,
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        // centerTitle: true,
        title: Text(
          "Login",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40.0),
            topRight: Radius.circular(40.0),
          ),
        ),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: Text(
                " Please Login by",
                style: TextStyle(
                    color: ThemeScheme.lightTheme.primaryColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: Row(
                        children: <Widget>[
                          Container(
                            height: 50,
                            width: 50,
                            child: Icon(
                              Icons.phone,
                              color: Colors.white,
                            ),
                            decoration: BoxDecoration(
                              color: ThemeScheme.lightTheme.primaryColor,
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(20),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.horizontal(
                                  right: Radius.circular(20.0),
                                ),
                              ),
                              child: TextField(
                                cursorColor:
                                    ThemeScheme.lightTheme.primaryColor,
                                inputFormatters: [
                                  //  maskTextInputFormatter,
                                  LengthLimitingTextInputFormatter(20),
                                ],
                                keyboardType: TextInputType.number,
                                style: TextStyle(fontSize: 18),
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: ' Enter your phone number'),
                              ),
                            ),
                          )
                        ],
                      )),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: Row(
                        children: <Widget>[
                          Container(
                            height: 50,
                            width: 50,
                            child: Icon(
                              Icons.lock,
                              color: Colors.white,
                            ),
                            decoration: BoxDecoration(
                              color: ThemeScheme.lightTheme.primaryColor,
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(20),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.horizontal(
                                  right: Radius.circular(20.0),
                                ),
                              ),
                              child: TextField(
                                cursorColor:
                                    ThemeScheme.lightTheme.primaryColor,
                                inputFormatters: [
                                  //  maskTextInputFormatter,
                                  LengthLimitingTextInputFormatter(20),
                                ],
                                keyboardType: TextInputType.number,
                                style: TextStyle(fontSize: 18),
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: ' Enter your password'),
                              ),
                            ),
                          )
                        ],
                      )),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        "Forget Password ?",
                        style: TextStyle(color: Color(0xffEA7623)),
                      )),
                  SizedBox(
                    height: 50,
                  ),
                  Container(
                    height: 50,
                    width: 250,
                    decoration: BoxDecoration(
                      color: Color(0xffEA7623),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Center(
                      child: Text(
                        "Log In",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Divider(
                          thickness: 2,
                          color: Colors.orange,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 14.0),
                        child: Text(
                          "or login by",
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ),
                      Expanded(
                        child: Divider(
                          thickness: 2,
                          color: Colors.orange,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          height: 30,
                          width: 30,
                          child: Image.network(
                              "https://cdn.icon-icons.com/icons2/1011/PNG/512/Gmail_icon-icons.com_75706.png")),
                      SizedBox(
                        width: 30,
                      ),
                      Container(
                          height: 28,
                          width: 28,
                          child: Image.network(
                              "https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Facebook-512.png")),

//
                    ],
                  ),
                ],
              ),
            ),
            Spacer(),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              color: ThemeScheme.lightTheme.primaryColor,
              child: Center(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Don't have an account?",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    " Sign Up",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ],
              )),
            )
          ],
        ),
      ),
    );
  }
}

class CustomTextField extends StatefulWidget {
  final IconData prefixIcon;

  const CustomTextField({
    this.fieldKey,
    this.hintText,
    this.labelText,
    this.helperText,
    this.onSaved,
    this.validator,
    this.onFieldSubmitted,
    this.prefixIcon,
  });

  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;

  @override
  _TextFieldState createState() => _TextFieldState();
}

class _TextFieldState extends State<CustomTextField> {
  bool _obscureText = false;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: widget.fieldKey,
      obscureText: _obscureText,
      onSaved: widget.onSaved,
      validator: widget.validator,
      onFieldSubmitted: widget.onFieldSubmitted,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        prefixIcon: Icon(
          widget.prefixIcon,
          color: Color(0xffEA7623),
        ),
        hintText: widget.hintText,
        labelText: widget.labelText,
        helperText: widget.helperText,
      ),
    );
  }
}

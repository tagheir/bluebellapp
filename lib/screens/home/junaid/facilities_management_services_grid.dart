import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FacilitiesManagementServicesGrid extends StatelessWidget {
  Container CardForGrid({String service_name, String imageURL}) {
    return Container(
      //height: 200,
      child: Card(
        //for cutting the corners of cards
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          //side: BorderSide(color: Colors.orange, width: 2),
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 15.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.network(
              imageURL,
              height: 110.0,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Divider(
              color: Colors.orange,
              thickness: 2.0,
              height: 1.0,
            ),
//            SizedBox(
//              height: 15.0,
//            ),
            Container(
                //width: double.infinity,
                child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
                service_name,
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
              ),
            ))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: GridView.count(
        mainAxisSpacing: 20.0,
        crossAxisSpacing: 20.0,
        padding: EdgeInsets.all(10.0),
        crossAxisCount: 2,
        children: <Widget>[
          CardForGrid(
              service_name: 'Electrical Service',
              imageURL: 'https://i.ytimg.com/vi/Hd3iBpUpgeg/maxresdefault.jpg'),
          CardForGrid(
              service_name: 'Plumbing',
              imageURL:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSThrrIk0krJXM5AybXB0pZJmQQZpzyPjWrRE1XyXq_lQ-fZj5N'),
          CardForGrid(
              service_name: 'Pest Control',
              imageURL:
                  'https://image.shutterstock.com/image-vector/pest-control-harmful-insects-rodents-260nw-465458375.jpg'),
          CardForGrid(
              service_name: 'Ac Service',
              imageURL:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVpTipbz8cRwVkCRvZEDpci2aFKQwE_NnHgkAcsGH7g58teuAs'),
          CardForGrid(
              service_name: 'Handy Man ',
              imageURL:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT4GFi7kMwy6uXdxlFOPv1qvPKXXo0GiuAh_vFriHeuqI2Bb4tV'),
          CardForGrid(
            service_name: 'Cleaning Services',
            imageURL:
                'https://png.pngtree.com/element_our/sm/20180415/sm_5ad3472717767.jpg',
          ),
          CardForGrid(
              service_name: 'Ac Service',
              imageURL:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVpTipbz8cRwVkCRvZEDpci2aFKQwE_NnHgkAcsGH7g58teuAs'),
          CardForGrid(
              service_name: 'Handy Man ',
              imageURL:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT4GFi7kMwy6uXdxlFOPv1qvPKXXo0GiuAh_vFriHeuqI2Bb4tV'),
          CardForGrid(
            service_name: 'Cleaning Services',
            imageURL:
                'https://png.pngtree.com/element_our/sm/20180415/sm_5ad3472717767.jpg',
          )
        ],
      ),
    )

//
        );
  }
}

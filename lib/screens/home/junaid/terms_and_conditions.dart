import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class TermsAndConditions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "Terms and Services",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  height: 30.0,
                  width: 1.0,
                  color: Colors.red,
                  // margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                ),
                Expanded(
                  child: Html(padding: EdgeInsets.all(12.0), data: """
               <div>
              <p>
             </p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">What’s In?</p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">• Dust/damp-wipe all doors, switches.<br>• Bathrooms and toilets: Scrub floors, tiled walls, sanitary wares and fittings, wash down mirrors.<br>• Kitchen: scrub floors, tiled walls, sink and worktops surfaces, wipe down cabinets.<br>• Clean balcony and damp-wipe handrails if applicable.<br>• Clean A/C vents and skirting.<br>• Wash down internal windows and frames (up to a maximum height of 1.6 meters).<br>• Scrub the hard floors in all the rooms.</p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">Note:</p><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">Standard Prices are valid for the following areas only</p><ul style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 14px;"><li>Arabian Ranches</li><li>Jumeirah Circle</li><li>Jumeirah Park</li><li>Palm Jumeirah</li><li>Meadows</li><li>Springs</li><li>Hattan</li></ul><p style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 13px;">Prices for other areas might vary and sight survey is required for below areas/on customized villas</p><ul style="margin-bottom: 14px; color: rgb(66, 65, 65); font-family: Heebo, sans-serif; font-size: 14px;"><li>Umm Suqueim</li><li>Jumeirah</li><li>Mirdif</li><li>Al Barsha</li><li>Emirates Hills</li></ul>
               <p></p>
										  </div>
                                          
              """),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

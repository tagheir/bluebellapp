import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/home/junaid/save_card_info.dart';
import 'package:flutter/material.dart';

class PaymentSelectionMethod extends StatefulWidget {
  @override
  _PaymentSelectionMethodState createState() => _PaymentSelectionMethodState();
}

class _PaymentSelectionMethodState extends State<PaymentSelectionMethod> {
  int SelectedRadio;
  int selectedRadioTile;
  int radio;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SelectedRadio = 0;
    selectedRadioTile = 0;
  }

  setSelectedRadio(int val) {
    setState(() {
      SelectedRadio = val;
    });
  }

  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        title: Text(
          'Payment method',
          style: TextStyle(color: Colors.white),
        ),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          InkWell(
            onTap: () {
//              Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (BuildContext context) => SaveCardInfo()));
            },
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: RadioListTile(
                      value: 1,
                      groupValue: selectedRadioTile,
                      title: Text('By Cash'),
                      onChanged: (val) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    SaveCardInfo()));
                        print('Radio tile pressed  $val');
                        radio:
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                  Expanded(
                      flex: 3,
                      child: Image.network(
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQpa4bprhI2pLmtQK6mtyRiSYsQerVS3q-k1CvDgFfEGenvn7xV'))
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              //TODO
            },
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: RadioListTile(
                      value: 2,
                      groupValue: selectedRadioTile,
                      title: Text('Credit or debit card'),
                      onChanged: (val) {
                        print('Radia tile pressed  $val');
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQWjS0hD66RXw1AhcZL2SK2prpk9Cl0hMeMbRtDmlQlYZ0qfXb6'),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

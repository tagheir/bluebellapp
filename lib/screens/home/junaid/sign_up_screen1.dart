import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class NewSignUpScreen extends StatelessWidget {
  var maskTextInputFormatter = MaskTextInputFormatter(
      mask: " #### #######", filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: ThemeScheme.lightTheme.primaryColor));
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeScheme.lightTheme.primaryColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        leading: Icon(Icons.arrow_back_ios),
        elevation: 0,
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        // centerTitle: true,
        title: Text(
          "SignUp",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40.0),
              topRight: Radius.circular(40.0),
            ),
          ),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18.0),
                child: Text(
                  " Enter your credentials",
                  style: TextStyle(
                      color: ThemeScheme.lightTheme.primaryColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.person,
                                color: Colors.white,
                              ),
                              decoration: BoxDecoration(
                                color: ThemeScheme.lightTheme.primaryColor,
                                borderRadius: BorderRadius.horizontal(
                                  left: Radius.circular(20),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.horizontal(
                                    right: Radius.circular(20.0),
                                  ),
                                ),
                                child: TextField(
                                  cursorColor:
                                      ThemeScheme.lightTheme.primaryColor,
                                  inputFormatters: [
                                    //  maskTextInputFormatter,
                                    LengthLimitingTextInputFormatter(20),
                                  ],
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(fontSize: 18),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: ' Enter your Name'),
                                ),
                              ),
                            )
                          ],
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.markunread,
                                color: Colors.white,
                              ),
                              decoration: BoxDecoration(
                                color: ThemeScheme.lightTheme.primaryColor,
                                borderRadius: BorderRadius.horizontal(
                                  left: Radius.circular(20),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.horizontal(
                                    right: Radius.circular(20.0),
                                  ),
                                ),
                                child: TextField(
                                  cursorColor:
                                      ThemeScheme.lightTheme.primaryColor,
                                  inputFormatters: [
                                    //  maskTextInputFormatter,
                                    LengthLimitingTextInputFormatter(20),
                                  ],
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(fontSize: 18),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: ' Enter your email'),
                                ),
                              ),
                            )
                          ],
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.phone,
                                color: Colors.white,
                              ),
                              decoration: BoxDecoration(
                                color: ThemeScheme.lightTheme.primaryColor,
                                borderRadius: BorderRadius.horizontal(
                                  left: Radius.circular(20),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.horizontal(
                                    right: Radius.circular(20.0),
                                  ),
                                ),
                                child: TextField(
                                  cursorColor:
                                      ThemeScheme.lightTheme.primaryColor,
                                  inputFormatters: [
                                    //  maskTextInputFormatter,
                                    LengthLimitingTextInputFormatter(20),
                                  ],
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(fontSize: 18),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: ' Enter your phone number'),
                                ),
                              ),
                            )
                          ],
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.lock,
                                color: Colors.white,
                              ),
                              decoration: BoxDecoration(
                                color: ThemeScheme.lightTheme.primaryColor,
                                borderRadius: BorderRadius.horizontal(
                                  left: Radius.circular(20),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.horizontal(
                                    right: Radius.circular(20.0),
                                  ),
                                ),
                                child: TextField(
                                  cursorColor:
                                      ThemeScheme.lightTheme.primaryColor,
                                  inputFormatters: [
                                    //  maskTextInputFormatter,
                                    LengthLimitingTextInputFormatter(20),
                                  ],
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(fontSize: 18),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: ' Enter your password'),
                                ),
                              ),
                            )
                          ],
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.lock,
                                color: Colors.white,
                              ),
                              decoration: BoxDecoration(
                                color: ThemeScheme.lightTheme.primaryColor,
                                borderRadius: BorderRadius.horizontal(
                                  left: Radius.circular(20),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.horizontal(
                                    right: Radius.circular(20.0),
                                  ),
                                ),
                                child: TextField(
                                  cursorColor:
                                      ThemeScheme.lightTheme.primaryColor,
                                  inputFormatters: [
                                    //  maskTextInputFormatter,
                                    LengthLimitingTextInputFormatter(20),
                                  ],
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(fontSize: 18),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: ' Confirm your password'),
                                ),
                              ),
                            )
                          ],
                        )),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 50,
                      width: 250,
                      decoration: BoxDecoration(
                        color: Color(0xffEA7623),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          "Sign Up",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                color: ThemeScheme.lightTheme.primaryColor,
                child: Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Already have an account?",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      " Sign In",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                  ],
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}

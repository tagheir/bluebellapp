import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/material.dart';

class AboutUS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "About Us",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 90),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Company Vision",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  Divider(
                    thickness: 3,
                    color: ThemeScheme.lightTheme.primaryColor,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                  "To become the ultimate provider of sustainable and technology-driven home maintenance services throughout the UAE."),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 110),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Company Mission",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  Divider(
                    thickness: 3,
                    color: ThemeScheme.lightTheme.primaryColor,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                  "To deliver excellence in home maintenance services with a focus on quality, value and sustainable best practice while being led by innovative technology, which exceeds the expectations of our customers."),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 100,
              // color: Colors.deepOrange,
              width: MediaQuery.of(context).size.width,
              child: Image.network(
                  "https://s3.ap-south-1.amazonaws.com/staging.in.pro.zuper/attachments/5d5ae3ca1410808c598e2601/f009a770-0c46-11ea-ad3d-fde222d4366b.jpg"),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Message from our CEO",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  Divider(
                    thickness: 3,
                    color: ThemeScheme.lightTheme.primaryColor,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                  "Since its establishment in 1980, Farnek has grown exponentially and developed into one of the UAE’s foremost total facilities management companies, a market leader in smart FM technology as well as sustainability.Our highly skilled workforce has grown to over 4,000 employees and we have now opened new offices in the Northern Emirates, supplementing our existing offices in Abu Dhabi and Dubai. We are also investing over AED150 million into a new state-of-the-art staff accommodation centre in Dubai Investments Park, which will house more than 8,000 employees.Currently, we deliver professional FM services on behalf of blue chip organisations such as Dubai Airport, DU, Etihad Airways, and Emaar’s Dubai Mall. We are also making significant inroads into the entertainment sector, with recent contract wins for IMG Worlds of Adventure and Dubai Parks & Resorts.One of our most significant achievements remains our successful bid in 2010, to provide MEP FM services to the world’s tallest building, The Burj Khalifa, which we still retain today.So, it is with an air of confidence and positive anticipation, that we look forward to Expo 2020 and beyond as FM enters a new era in the UAE.Farnek is also a leader in sustainability and is a member of the Emirates Green Building Council, has associations with international organisations such as Green Globe and Myclimate and has already been presented with the prestigious Emirates Energy Award for producing the Middle East’s first hotel energy consumption benchmark survey as well as setting the precedent to winning the Green FM Company of the Year in its first year of inception in the Middle East."),
            ),
            SizedBox(
              height: 70,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 58.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        // height: 85,
                        decoration: const ShapeDecoration(
                          color: Color(0xFFEA7623),
                          shape: CircleBorder(),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Icon(
                            Icons.directions_car,
                            color: Colors.white,

                            //color: Colors.white,
                          ),
                        ),
                      ),
                      Text(
                        "100+",
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        "Vehicles",
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        // height: 85,
                        decoration: const ShapeDecoration(
                          color: Color(0xFFEA7623),
                          shape: CircleBorder(),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Icon(
                            Icons.person,
                            color: Colors.white,

                            //color: Colors.white,
                          ),
                        ),
                      ),
                      Text(
                        "500+",
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        "Employees",
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        // height: 85,
                        decoration: const ShapeDecoration(
                          color: Color(0xFFEA7623),
                          shape: CircleBorder(),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Icon(
                            Icons.person_add,
                            color: Colors.white,

                            //color: Colors.white,
                          ),
                        ),
                      ),
                      Text(
                        "6000+",
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        "Customers",
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        // height: 85,
                        decoration: const ShapeDecoration(
                          color: Color(0xFFEA7623),
                          shape: CircleBorder(),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Icon(
                            Icons.access_time,
                            color: Colors.white,

                            //color: Colors.white,
                          ),
                        ),
                      ),
                      Text(
                        "24",
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        "Hours Service",
                        style: TextStyle(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

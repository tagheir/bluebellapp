import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/widgets/headings/left_text_fz20_w7.dart';
import 'package:bluebellapp/screens/widgets/project_widgets/package_compact_view_widget.dart';
import 'package:bluebellapp/screens/widgets/project_widgets/product_compact_view.dart';
import 'package:bluebellapp/screens/widgets/project_widgets/service_compact_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'cart_list.dart';

import 'packages_list.dart';
import 'dashboard.dart';

class HomeWidget extends StatelessWidget {
  final Function call;
  final Function call1;
  final Function call2;
  final Function call3;
  const HomeWidget({this.call, Key key, this.call1, this.call2, this.call3})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    // Dashboard obj = Dashboard();

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: LeftTextFz20Fw7('Packages'),
//              ),

//              Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  LeftTextFz20Fw7('Packages'),
//                  FlatButton(
//                    onPressed: () {},
//                    child: FlatButton(
//                      onPressed: () {
//                        call();
//                      },
//                      child: Text(
//                        'View All',
//                        style: TextStyle(
//                            color: ThemeScheme.lightTheme.primaryColor),
//                      ),
//                    ),
//                  )
//                ],
//              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  LeftTextFz20Fw7('Products'),
                  FlatButton(
                    onPressed: () {
                      call();
                    },
                    child: Text(
                      'View All',
                      style:
                          TextStyle(color: ThemeScheme.lightTheme.primaryColor),
                    ),
                  )
                ],
              ),
              // Align(alignment: Alignment.bottomRight, child: Text('View All')),
              Container(
                height: 180.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    PackageCompactViewWidget(),
                    PackageCompactViewWidget(),
                    PackageCompactViewWidget()
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
//              Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  LeftTextFz20Fw7('Facilities Management Services'),
//                  FlatButton(
//                    onPressed: () {
//                      call1();
//                    },
//                    child: FlatButton(
//                      child: Text(
//                        'View All',
//                        style: TextStyle(color: Color(0xFFEA7623)),
//                      ),
//                    ),
//                  )
//                ],
//              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  LeftTextFz20Fw7('Facilities Management Services'),
                  FlatButton(
                    onPressed: () {
                      call1();
                    },
                    child: Text(
                      'View All',
                      style:
                          TextStyle(color: ThemeScheme.lightTheme.primaryColor),
                    ),
                  )
                ],
              ),

              Container(
                // color: Colors.red,
                height: 150.0,
                // color: Colors.yellow,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView()
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  LeftTextFz20Fw7('Landscape Services'),
                  FlatButton(
                    onPressed: () {
                      call2();
                    },
                    child: Text(
                      'View All',
                      style:
                          TextStyle(color: ThemeScheme.lightTheme.primaryColor),
                    ),
                  )
                ],
              ),
              Container(
                height: 150.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView(),
                    ServiceCompactView()
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  LeftTextFz20Fw7('Products'),
                  FlatButton(
                    onPressed: () {
                      call3();
                    },
                    child: Text(
                      'View All',
                      style:
                          TextStyle(color: ThemeScheme.lightTheme.primaryColor),
                    ),
                  )
                ],
              ),
              Container(
                height: 150.0,
                //   color: Colors.pinkAccent,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    ProductCompactView(),
                    ProductCompactView(),
                    ProductCompactView(),
                    ProductCompactView(),
                    ProductCompactView(),
                    ProductCompactView(),
                    ProductCompactView()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/home/junaid/profile.dart';
import 'payment_selection_method.dart';
import 'enter_delivery_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int count = 1;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeScheme.lightTheme.primaryColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            'Checkout',
            style: TextStyle(color: Colors.white),
          ),
        ),
        //Main Column in which all Container are aligned
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  Card(
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  '- ',
                                  style: TextStyle(
                                      color:
                                          ThemeScheme.lightTheme.primaryColor,
                                      fontSize: 40),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Text("$count", style: TextStyle(fontSize: 20)),
                                SizedBox(
                                  width: 15,
                                ),
                                Text('+',
                                    style: TextStyle(
                                        color:
                                            ThemeScheme.lightTheme.primaryColor,
                                        fontSize: 30)),
                                SizedBox(
                                  width: 20,
                                ),
                                Text(
                                  'Deal 1',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                ),
                                Spacer(),
                                Text(
                                  'Rs 320',
                                  style: TextStyle(fontSize: 16),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  '- ',
                                  style: TextStyle(
                                      color:
                                          ThemeScheme.lightTheme.primaryColor,
                                      fontSize: 40),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Text("$count", style: TextStyle(fontSize: 20)),
                                SizedBox(
                                  width: 15,
                                ),
                                Text('+',
                                    style: TextStyle(
                                        color:
                                            ThemeScheme.lightTheme.primaryColor,
                                        fontSize: 30)),
                                SizedBox(
                                  width: 20,
                                ),
                                Text(
                                  'Deal 2',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                ),
                                Spacer(),
                                Text(
                                  'Rs 320',
                                  style: TextStyle(fontSize: 16),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  '- ',
                                  style: TextStyle(
                                      color:
                                          ThemeScheme.lightTheme.primaryColor,
                                      fontSize: 40),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Text("$count", style: TextStyle(fontSize: 20)),
                                SizedBox(
                                  width: 15,
                                ),
                                Text('+',
                                    style: TextStyle(
                                        color:
                                            ThemeScheme.lightTheme.primaryColor,
                                        fontSize: 30)),
                                SizedBox(
                                  width: 20,
                                ),
                                Text(
                                  'Deal 1',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                ),
                                Spacer(),
                                Text(
                                  'Rs 320',
                                  style: TextStyle(fontSize: 16),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  '- ',
                                  style: TextStyle(
                                      color:
                                          ThemeScheme.lightTheme.primaryColor,
                                      fontSize: 40),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Text("$count", style: TextStyle(fontSize: 20)),
                                SizedBox(
                                  width: 15,
                                ),
                                Text('+',
                                    style: TextStyle(
                                        color:
                                            ThemeScheme.lightTheme.primaryColor,
                                        fontSize: 30)),
                                SizedBox(
                                  width: 20,
                                ),
                                Text(
                                  'Deal 1',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                ),
                                Spacer(),
                                Text(
                                  'Rs 320',
                                  style: TextStyle(fontSize: 16),
                                )
                              ],
                            ),
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 170,
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Subtotal',
                                  style: TextStyle(fontSize: 15),
                                ),
                                Text('Rs 320', style: TextStyle(fontSize: 15)),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Delivery fee',
                                    style: TextStyle(fontSize: 15)),
                                Text('Rs100', style: TextStyle(fontSize: 15))
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Do you have a free voucher?',
                              style: TextStyle(
                                  color: ThemeScheme.lightTheme.primaryColor),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Total',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                                Text(
                                  'Rs 600.00',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Contact Info',
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.grey),
                                ),
                                Text('Please enter contact info',
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.grey)),
                              ],
                            ),
                            Divider(),
                            SizedBox(
                              height: 10,
                            ),
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              EnterDeliveryAddress()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Delivery details',
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.black)),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text('182DD,DHA Phase 4,Octacer,Lahore',
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.grey)),
                                        SizedBox(
                                          height: 15,
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Icon(Icons.location_on)
                                      ],
                                    )
                                  ],
                                )),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Delivery Time',
                                  style: TextStyle(fontSize: 15),
                                ),
                                Text(
                                  'NOW(55 min)',
                                  style: TextStyle(fontSize: 15),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 120,
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Paymment Methods',
                              style: TextStyle(fontSize: 15),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            PaymentSelectionMethod()));
                              },
                              child: Text(
                                'CASH',
                                style: TextStyle(
                                    fontSize: 15,
                                    color: ThemeScheme.lightTheme.primaryColor),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 90.0),
                    child: Container(
                      height: 130,
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'By Completing the order the order ,I hereby accepts to the terms and condition',
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            new Positioned(
              top: MediaQuery.of(context).size.height - 70,
              child: Container(
                color: ThemeScheme.lightTheme.primaryColor,
                height: 70,
                width: MediaQuery.of(context).size.width,
                //color: ThemeScheme.lightTheme.primaryColor,

                child: FlatButton(
                  onPressed: () {
                    {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => Profile()));
                    }
                  },
                  child: Container(
                    color: ThemeScheme.lightTheme.primaryColor,
                    child: Center(
                        child: Text(
                      'Place your order',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    )),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}

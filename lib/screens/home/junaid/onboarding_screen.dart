// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// class OnboardingScreen extends StatelessWidget {
//   List<PageViewModel> getPages() {
//     return [
//       PageViewModel(
//         image: Image.network(
//             "https://icons.iconarchive.com/icons/danleech/simple/256/facebook-icon.png"),
//         title: "Facebook",
//         body: "Facebook is a social app",
//         footer: Text("Octacer"),
//       ),
//       PageViewModel(
//         image: Image.network(
//             "https://cdn1.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-snapchat-2019-square1-512.png"),
//         title: "Snapchat",
//         body:
//             "Snapchat is widely famous for its Stories feature and its camera filters",
//         footer: Text("Octacer"),
//       ),
//       PageViewModel(
//         image: Image.network(
//             "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT_RJ0Lmew4ZMVpaVjugF-_qY-F_FC6UBE2vEIoleHy92GRQGQI&usqp=CAU"),
//         title: "This is Quora,a social platform",
//         body:
//             "On this platform people post their question on different topic and others try to answer their queries",
//         footer: Text("Octacer"),
//       ),
//       PageViewModel(
//         image: Image.network(
//             "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQbCVyKKv32iPz24KEipX1EOw7AmOtDsdx29AMsFqJQUfZAqymd&usqp=CAU"),
//         title: "Instagram",
//         body: "Instagram  is a social app mainly comprised of pics and images",
//         footer: Text("Octacer"),
//       )
//     ];
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         actions: <Widget>[
//           Padding(
//             padding: const EdgeInsets.only(right: 8.0),
//             child: Center(
//                 child: Text(
//               "Done",
//               style: TextStyle(color: Colors.white, fontSize: 18),
//             )),
//           )
//         ],
//         title: Text(
//           "Onboarding screen",
//           style: TextStyle(color: Colors.white),
//         ),
//       ),
//       body: IntroductionScreen(
//         globalBackgroundColor: Colors.white,
//         pages: getPages(),
//         done: Text(
//           "Done",
//           style: TextStyle(color: Colors.black),
//         ),
//         onDone: () {},
//       ),
//     );
//   }
// }

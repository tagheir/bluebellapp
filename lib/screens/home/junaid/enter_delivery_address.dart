import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/home/junaid/choose_addresses_list.dart';
import 'package:bluebellapp/screens/home/junaid/my_orders_ui.dart';
import 'package:bluebellapp/screens/home/junaid/profile.dart';
import 'package:flutter/material.dart';
import 'home_address.dart';
import 'other_address.dart';

class EnterDeliveryAddress extends StatefulWidget {
  @override
  _EnterDeliveryAddressState createState() => _EnterDeliveryAddressState();
}

class _EnterDeliveryAddressState extends State<EnterDeliveryAddress> {
  List<Widget> containers = [
    ChooseAddressesList(),
    Profile(),
    MyOrdersUI(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeScheme.lightTheme.primaryColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            'Delivery',
            style: TextStyle(color: Colors.white),
          ),
          bottom: TabBar(
            labelColor: Colors.white,
            tabs: <Widget>[
              Tab(
                text: 'Addresses',
                icon: Icon(
                  Icons.work,
                ),
              ),
              Tab(
                text: 'Profile ',
                icon: Icon(Icons.person),
              ),
              Tab(
                text: 'My Orders',
                icon: Icon(Icons.location_on),
              ),
            ],
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: containers,
        ),
      ),
    );
  }
}

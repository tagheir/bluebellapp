import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:bluebellapp/screens/widgets/project_widgets/grid_container.dart';
import 'homescreen.dart';

class PackageList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Column(
              children: <Widget>[
                PackagesListCompactView('Basic'),
                PackagesListCompactView('Executive'),
                PackagesListCompactView('Custom'),
                PackagesListCompactView('Standard')
              ],
            ),
          ),
        ),

//
//
      ),
    );
  }

  Container PackagesListCompactView(String package_name) {
    return Container(
      height: 220,
      //color: Colors.red,
      child: Card(
        elevation: 8,
        clipBehavior: Clip.antiAlias,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 16,
            ),
            SvgPicture.asset(
              "assets/images/package.svg",
              color: Colors.black,
              width: 40.0,
              height: 40.0,
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              package_name,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40.0),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
              child: Divider(
                color: Color(0XFFEA7623),
                height: 5,
                thickness: 3,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
              child: Text(
                'Starting at',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            RichText(
              text: TextSpan(
                  text: 'AED 121',
                  style: TextStyle(color: Color(0XFFEA7623)),
                  children: <TextSpan>[
                    TextSpan(
                        text: '/month', style: TextStyle(color: Colors.black))
                  ]),
            )

            //Image.network(
            //  'https://cdn.pixabay.com/photo/2013/07/21/13/00/rose-165819__340.jpg'),
          ],
        ),
      ),
    );
  }
}

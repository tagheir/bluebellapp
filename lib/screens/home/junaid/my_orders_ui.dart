import 'package:bluebellapp/models/order_dto.dart';
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/widgets/myorders_card.dart';
import 'package:flutter/material.dart';

class MyOrdersUI extends StatelessWidget {
  List<OrderDto> orderlist = [
    OrderDto(
        id: 005,
        paidDateUtc: "20-05-20",
        orderStatus: 0,
        paymentMethodSystemName: "Credit Card",
        currencyRate: 555),
    OrderDto(
        id: 0925,
        paidDateUtc: "20-05-20",
        orderStatus: 1,
        paymentMethodSystemName: "Credit Card",
        currencyRate: 585)
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        iconTheme: IconThemeData(color: Colors.white),
//        title: Text(
//          "My Orders",
//          style: TextStyle(color: Colors.white),
//        ),
//      ),
      body: ListView(
        children: <Widget>[
          MyOrderCard(obj: orderlist[1]),
          MyOrderCard(obj: orderlist[0]),
          MyOrderCard(obj: orderlist[1]),
          MyOrderCard(obj: orderlist[0]),
          MyOrderCard(obj: orderlist[1]),
          MyOrderCard(obj: orderlist[0]),
          MyOrderCard(obj: orderlist[1]),
          MyOrderCard(obj: orderlist[0]),
        ],
      ),
    );
  }
}

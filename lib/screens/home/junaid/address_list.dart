import 'package:bluebellapp/models/customer_address.dart';
import 'package:bluebellapp/screens/home/junaid/create_or_edit_address_screen.dart';
import 'package:flutter/material.dart';

class AddressListTile extends StatefulWidget {
  final Address add;
  final int value;
  int groupValue;
  AddressListTile({this.add, this.value, this.groupValue});

  @override
  _AddressListTileState createState() => _AddressListTileState();
}

class _AddressListTileState extends State<AddressListTile> {
  @override
  Widget build(BuildContext context) {
    //` int customselectedRadioTile = 0;

    return RadioListTile(
      value: widget.value,
      groupValue: widget.groupValue,
      onChanged: (int value) {
        setState(() {
          if (widget.groupValue == -1) {
            widget.groupValue = value;
          } else {
            widget.groupValue = -1;
          }
        });
// MyRadioFunction(value);
// AlertDialogFunction();
      },
      title: Align(
        alignment: Alignment.bottomLeft,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 18,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(widget.add.address1),
                InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CreateorEditAddress(),
                        ),
                      );
                    },
                    child: Icon(Icons.edit))
              ],
            )
          ],
        ),
      ),
      subtitle: Text(widget.add.address2),
    );
  }
}

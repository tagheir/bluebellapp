import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class CreateorEditAddress extends StatelessWidget {
  var maskFormatter = new MaskTextInputFormatter(
      mask: '+92 ##########', filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeScheme.lightTheme.primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "Edit Address",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: TextFormField(
                    autofocus: true,
                    keyboardType: TextInputType.text,
                    maxLength: 10,
                    decoration: const InputDecoration(
                      //icon: Icon(Icons.person),
                      //hintText: 'What do people call you?',
                      labelText: 'Firstname *',
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: TextFormField(
                    maxLength: 10,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      // icon: Icon(Icons.person),
                      //hintText: 'What do people call you?',
                      labelText: 'Lastname *',
                    ),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFormField(
//              inputFormatters: [
//                maskFormatter,
//                LengthLimitingTextInputFormatter(20),
//              ],
              initialValue: "+92 ",
              maxLength: 14,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                labelText: 'Phone number *',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFormField(
              keyboardType: TextInputType.text,
              maxLength: 15,
              decoration: const InputDecoration(
                labelText: 'Country *',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFormField(
              keyboardType: TextInputType.text,
              maxLength: 15,
              decoration: const InputDecoration(
                labelText: 'City *',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFormField(
              keyboardType: TextInputType.text,
              maxLength: 15,
              decoration: const InputDecoration(
                labelText: 'State *',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFormField(
              maxLength: 15,
              decoration: const InputDecoration(
                labelText: 'Address *',
              ),
            ),
          ),
          Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 38.0, horizontal: 18),
              child: Card(
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
                child: ButtonTheme(
                  height: 50,
                  minWidth: 400,
                  child: RaisedButton(
                    onPressed: () {},
                    textColor: Colors.white,
                    child: Text(
                      'Save Address',
                      style: TextStyle(fontSize: 20),
                    ),
                    color: ThemeScheme.lightTheme.primaryColor,
                  ),
                ),
              )),
        ],
      ),
    );
  }
}

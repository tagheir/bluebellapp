import 'package:bluebellapp/models/inquiry_form_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/simple_form_field.dart';
import 'package:flutter/material.dart';

class ProductInquiryForm extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  InquiryFormDto inquiryFormDto = InquiryFormDto();
  List<Widget> formWidgets = List<Widget>();
  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Container(
          height: 400,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, top: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Icon(Icons.close, color: LightColor.orange)),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(GeneralStrings.SEND_INQUIRY,
                          style:
                              TextConstants.H6.apply(color: LightColor.orange))
                    ],
                  ),
                ),
                getInquiryForm(),
                Padding(
                  padding: const EdgeInsets.only(right: 20, top: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(GeneralStrings.SEND,
                          style:
                              TextConstants.H5.apply(color: LightColor.orange))
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  getInquiryForm() {
    formWidgets.add(getFormmRow(SimpleFormField(
        initialValue: '',
        inputType: TextInputType.text,
        label: 'Name',
        //    maxLength: 15,
        onChanged: (value) {
          inquiryFormDto.name = value;
        })));
    formWidgets.add(getFormmRow(SimpleFormField(
        initialValue: '',
        inputType: TextInputType.text,
        label: 'Email',
        //    maxLength: 30,
        onChanged: (value) {
          inquiryFormDto.email = value;
        })));
    formWidgets.add(getFormmRow(SimpleFormField(
        initialValue: '+92',
        inputType: TextInputType.text,
        label: 'Phone Number',
        //    maxLength: 14,
        onChanged: (value) {
          inquiryFormDto.phoneNumber = value;
        })));
    formWidgets.add(getFormmRow(SimpleFormField(
        initialValue: '',
        inputType: TextInputType.text,
        label: 'Message',
        maxLines: 3,
        //  maxLength: 250,
        onChanged: (value) {
          inquiryFormDto.phoneNumber = value;
        })));
    return Column(children: formWidgets);
  }

  Padding getFormmRow(SimpleFormField formField) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[SizedBox(width: 250, child: formField)],
      ),
    );
  }
}

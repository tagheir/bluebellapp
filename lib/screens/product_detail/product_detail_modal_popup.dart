import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_bloc.dart';
import 'package:bluebellapp/models/addToCart_dto.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/repos/cart_repo.dart';
import 'package:bluebellapp/resources/constants/helper_constants/attribute_control_type.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/network_cache_image.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custome_alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bluebellapp/screens/product_detail/product_bottom_tab_bar.dart';
import 'package:bluebellapp/resources/constants/helper_constants/template_type.dart';
import 'package:intl/intl.dart';

class ProductDetailModalPopup extends StatefulWidget {
  final bool isCartProduct;
  final int cartItemId;
  GlobalKey<ScaffoldState> scaffoldKey;
  ProductDetailModalPopup(
      {this.isCartProduct, this.cartItemId, this.scaffoldKey}) {
    //print(isCartProduct);
  }
  @override
  _ProductDetailModalPopupState createState() =>
      _ProductDetailModalPopupState();
}

class _ProductDetailModalPopupState extends State<ProductDetailModalPopup> {
  String currentItemSelected;
  var selectedDate = "";
  DateTime dateTime;
  BuildContext _context;
  ProductDetailDto productDetail;
  ProductDto product;
  CartRepository cartRepo;
  bool isDataLoaded = false;
  bool haveDropDownAttributes = false;
  bool haveDateAttribute = false;
  ProductAttribute productAttr;
  ProductAttribute dateAttr;
  List<AttributeValue> attrVal;
  bool canAddToCart = false;
  bool productDataSet = false;
  AddToCartDto myCart = AddToCartDto();
  AttrMap attrMap = AttrMap();
  AttrMap dayMap = AttrMap();
  AttrMap monthMap = AttrMap();
  AttrMap yearMap = AttrMap();
  GlobalKey<ScaffoldState> scaffoldKey;
  CustomProgressDialog pr;
  DateTime currentDate;
  AnimationController controller;
  AppBloc appBloc;
  ProductBloc productBloc;
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  @override
  void initState() {
    super.initState();
    scaffoldKey = widget.scaffoldKey ?? GlobalKey<ScaffoldState>();
  }

  var selectedAttributeValues = Map<String, String>();

  @override
  void dispose() {
    super.dispose();
  }

  addProductToCart() async {
    if (haveDropDownAttributes) {
      myCart.form.add(attrMap);
    }
    if (haveDateAttribute) {
      if (dayMap.value != null &&
          monthMap.value != null &&
          yearMap.value != null) {
        myCart.form.add(dayMap);
        myCart.form.add(monthMap);
        myCart.form.add(yearMap);
        canAddToCart = true;
      } else {
        ////print("Service date not selected");
        CustomAlertDialog.showNew(
            cntxt: _context, text: 'Warning !' + '\nService Date Not Selected');
      }
    } else {
      canAddToCart = true;
    }
    //print(canAddToCart);
    //print(product.name);
    //print(product.templateType);
    //print(product.templateType.isMaintenancePackage());
    if (widget.isCartProduct != true &&
        canAddToCart == true &&
        product.templateType.isMaintenancePackage() == false) {
      await pr.showDialog();
      cartRepo
          .addToCart(
              cart: myCart,
              callBack: (data) async {
                await pr.hideDialog();
                showInSnackBar(data);
              })
          .then((data) async {
        if (data != null) {
          // pr.showDialog();
          //print("data no null");
          await pr.hideDialog();
          appBloc.moveBack();
          appBloc.add(CartItemsViewEvent());
        }
      });
    } else if (canAddToCart == true &&
        product != null &&
        product.templateType.isMaintenancePackage() == true) {
      var productAttribute =  myCart.directOrderProductAttribute;
      productAttribute.price = productDetail.price.toDouble();
      productAttribute.dropDownAttributeValue  = attrMap?.value;
      productAttribute.pictureThumb = product?.pictureThumb;
      productAttribute.name = product.name;
      context.addEvent(
          DirectCheckoutViewEvent(addToCartDto: myCart));
    } else if (widget.isCartProduct == true &&
        canAddToCart == true &&
        product == null) {
      await pr.showDialog();
      cartRepo
          .updateCart(
              cart: myCart,
              callBack: (data) async {
                await pr.hideDialog();
                showInSnackBar(data);
              })
          .then((data) async {
        if (data != null) { 
          // pr.showDialog();
          //print("data not null");
          await pr.hideDialog();
          appBloc.moveBack();
          appBloc.add(CartItemsViewEvent());
        }
      });
    }
  }

  getDropDownData(ProductAttribute dropDown) async {
    productAttr = dropDown;
    if (widget.isCartProduct == true) {
      var dropAttr =
          await cartRepo.getDropDownAttributeValue(productId: productDetail.id);
      currentItemSelected = dropAttr?.name;
    } else {
      currentItemSelected =
          productAttr?.attributeValues?.attributeValue?.first?.name;
    }
    attrVal = productAttr?.attributeValues?.attributeValue;
    attrMap.key = 'product_attribute_' + productAttr.id.toString();
    setPrice();
    // attrMap.value = attrVal?.first?.id.toString();
    // var price = productDetail.price;
    // productDetail.price = price +
    //     int.parse(attrVal
    //         .firstWhere((v) => v.name == this.currentItemSelected,
    //             orElse: () => null)
    //         .priceAdjustment
    //         .toString());
    setState(() {
      haveDropDownAttributes = true;
    });
    //print("============" + currentItemSelected + "================");
  }

  getDateData(ProductAttribute date) async {
    if (widget.isCartProduct == true) {
      selectedDate =
          await cartRepo.getDateAttributeValue(productId: productDetail.id);
    } else {
      selectedDate = 'Schedule a task on time';
    }
    dateAttr = date;
    dayMap.key = 'product_attribute_' + dateAttr.id.toString() + '_day';
    monthMap.key = 'product_attribute_' + dateAttr.id.toString() + '_month';
    yearMap.key = 'product_attribute_' + dateAttr.id.toString() + '_year';
    setState(() {
      haveDateAttribute = true;
    });
  }

  Widget dropDownWidget(ProductAttribute item) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            height: 50,
            child: DropdownButton<String>(
              isExpanded: true,
              items: item.attributeValues.attributeValue.map((attribute) {
                return DropdownMenuItem<String>(
                    value: attribute.name, child: Text(attribute.name));
              }).toList(),
              onChanged: (String newValueSelected) {
                setState(() {
                  currentItemSelected = newValueSelected;
                });
                setPrice();
                //Your code to execute when menu item is selected by the user
              },
              value: currentItemSelected,
            ),
          ),
        )
      ],
    ).padding(EdgeInsets.only(bottom: 16));
  }

  Widget datePickerWidget(ProductAttribute item) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 5, right: 5, bottom: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              onPressed: () async {
                DateTime userselecteddate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime.now().subtract(Duration(days: 1)),
                    lastDate: DateTime(2100));
                setState(() {
                  String formatteddate =
                      DateFormat.yMMMd().format(userselecteddate);
                  selectedDate = formatteddate.toString();
                  dayMap.value = userselecteddate.day.toString();
                  monthMap.value = userselecteddate.month.toString();
                  yearMap.value = userselecteddate.year.toString();
                });
              },
              child: Container(
                height: 50,
                // width: MediaQuery.of(context).size.width,
                //color: Colors.red,
                child: Card(
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.grey, width: 1)),
                  child: Center(
                    child: Text(
                      selectedDate,
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  setPrice() {
    attrMap.value = attrVal
        .firstWhere((v) => v.name == this.currentItemSelected,
            orElse: () => null)
        .id
        .toString();
    var price = product.price;
    productDetail.price = price +
        int.parse(attrVal
            .firstWhere((v) => v.name == this.currentItemSelected,
                orElse: () => null)
            .priceAdjustment
            .toString());
  }

  setProductData() {
    if (productDetail?.productAttributes?.productAttribute != null) {
      productDetail?.productAttributes?.productAttribute?.forEach((a) async {
        switch (a.attributeControlTypeId) {
          case AttributeControlType.DropdownList:
            return await getDropDownData(a);

          case AttributeControlType.DatePicker:
            return await getDateData(a);
        }
      });
    }
    productDataSet = true;
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    appBloc = BlocProvider.of<AppBloc>(context);
    productBloc = BlocProvider.of<ProductBloc>(context);
    productDetail = productBloc.productDetail;
    product = productBloc.product;
    //product2 = productBloc.product;
    pr = CustomProgressDialog(context: context);
    cartRepo = appBloc.getRepo().getCartRepository();
    if (widget.cartItemId != null) {
      myCart.cartItemId = widget.cartItemId;
    }
    myCart.productId = productDetail.id;

    if (productDataSet == false) {
      setProductData();
    }
    return showProductAttributesModalPopup();
  }

  showProductAttributesModalPopup() {
    return Padding(
      padding: const EdgeInsets.only(top: 32.0),
      child: Scaffold(
        appBar: _getAppBar(),
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * .9,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    getBasicDetails(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Divider(),
                    ),
                    getProductAttributesWidgetList(
                            productDetail.productAttributes?.productAttribute)
                        .padding(AppTheme.padding),
                    SizedBox(
                      height: MediaQuery.of(context).size.height - 480,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height - 162,
              child: getBottomTabBarWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Container getBottomTabBarWidget() {
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      // height: 150,
      // width: MediaQuery.of(context).size.width,
      child: ProductBottomTabBar(
        isCartProduct: widget.isCartProduct,
        templateType: product?.templateType,
        onAddToCartButtonPressed: () {
          addProductToCart();
        },
        price: productDetail.price.toString(),
      ),
    );
  }

  Padding getBasicDetails() {
    return Padding(
      padding: const EdgeInsets.only(top: 20)
          .add(EdgeInsets.symmetric(horizontal: 16)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Container(
                color: Colors.orange,
                child: NetworkCacheImage(
                  altImageUrl: "https://placehold.it/330",
                  imageUrl: productDetail.pictureWithCdn,
                  fit: BoxFit.fill,
                ),
                // Image.network(
                //   ApiRoutes.CdnPath + (product.pictures?.first ?? ""),
                //   fit: BoxFit.fill,
                // ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    productDetail.name,
                    style:
                        TextConstants.H6_5.apply(color: LightColor.lightBlack),
                  ).wrap(),
                  Text(
                    'Rs ' + productDetail.price.toString(),
                    style: TextConstants.H5.apply(color: LightColor.orange),
                  ).wrap(),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getProductAttributesWidgetList(List<ProductAttribute> attributes) {
    if (attributes == null || attributes.length < 1) return Text('');
    var widgets = List<Widget>();
    for (var item in attributes) {
      var curWidget;
      if (item.attributeControlTypeId == AttributeControlType.DropdownList) {
        curWidget = (dropDownWidget(item));
      } else if (item.attributeControlTypeId ==
          AttributeControlType.DatePicker) {
        curWidget = (datePickerWidget(item));
      }
      if (curWidget != null) {
        widgets.add(Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              item.textPrompt,
              style: TextConstants.H6_5.apply(color: LightColor.darkGrey),
            ),
            curWidget,
          ],
        ));
      }
    }
    return Column(children: widgets);
  }

  AppBar _getAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      // leading: IconButton(
      //   icon: Icon(
      //     Icons.arrow_back,
      //     color: Colors.orange,
      //   ),
      //   onPressed: () => Navigator.of(context).pop(),
      // ),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 16.0),
          child: IconButton(
            icon: Icon(
              Icons.close,
              color: Colors.orange,
              size: 32,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
      ],
    );
  }
}

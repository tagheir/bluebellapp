import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/screens/widgets/bottom_sheet_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductInquiryTab extends StatelessWidget {
  final Function() onCallClick;
  final Function() onFormClick;
  ProductInquiryTab({this.onCallClick, this.onFormClick});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: const Radius.circular(20.0),
          )),
      child: Padding(
        padding:
            const EdgeInsets.only(top: 10, left: 15, right: 15, bottom: 50),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            BottomSheetIconButton(
              onPressed: () {
                //onCallClick();
                _launchURL('tel:+923214445292');
              },
              flex: 2,
              icon: Icons.call,
              backgroundColor: LightColor.orange,
            ),
            _divider(),
            BottomSheetIconButton(
              flex: 2,
              onPressed: () {
                _launchURL('https://wa.me/+923214445292');
              },
              faIcon: FontAwesomeIcons.whatsapp,
              color: Colors.white,
            ),
            _divider(),
            BottomSheetIconButton(
              flex: 2,
              onPressed: onFormClick,
              icon: Icons.question_answer,
            ),
          ],
        ),
      ),
    );
  }

  Container _divider() {
    return Container(
      height: 50.0,
      width: 1.0,
      color: Colors.grey,
      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print('Could not launch $url');
    }
  }
}

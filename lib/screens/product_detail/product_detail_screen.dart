import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_event.dart';
import 'package:bluebellapp/bloc/product_bloc/product_state.dart';
import 'package:bluebellapp/models/addToCart_dto.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/repos/cart_repo.dart';
import 'package:bluebellapp/repos/services_repo.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/screens/loading_screen.dart';
import 'package:bluebellapp/screens/product_detail/product_detail_main_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductDetailPage extends StatefulWidget {
  final int productId;
  final int cartItemId;
  final bool isCartProduct;
  ProductDetailPage({
    this.productId,
    this.isCartProduct = false,
    this.cartItemId,
  });
  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  //BuildContext cntxt;
  ProductDetailDto productDetail;
  ProductDto product;
  CartRepository cartRepo;
  ServiceRepository serviceRepo;
  bool isDataLoaded = false;
  AddToCartDto myCart = AddToCartDto();
  //AppBloc appBloc;
  //ProductBloc productBloc;

  @override
  Widget build(BuildContext context) {
    {
      // appBloc = context.getAppBloc();
      // productBloc = context.getProductBloc();
      // cartRepo = appBloc.getRepo().getCartRepository();
      // serviceRepo = appBloc.getRepo().getServiceRepository();
      // cntxt = context;

      return BlocBuilder<ProductBloc, ProductState>(
        builder: (context, state) {
          print(state);
          if (state is ProductStateUninitialized) {
            context.getProductBloc().add(ProductParamEvent(
                  appBloc: context.getAppBloc(),
                  cartItemId: widget.cartItemId,
                  isCartProduct: widget.isCartProduct,
                  productId: widget.productId,
                ));
            return LoadingScreen();
          }
          if (state is ProductStateLoading) {
            return LoadingScreen();
          }
          if (state is ProductStateInitialized) {
            product = state.product;
            productDetail = state.productDetail;
            return ProductDetailMainView(
              //product: state.product,
              productDetail: state.productDetail,
              isCartProduct: widget.isCartProduct,
              cartItemId: widget.cartItemId,
            );
          }
          return Text('FAILED !!!').center();
        },
      );
    }
  }
}

import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/template_type.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/screens/widgets/bottom_sheet_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ProductBottomTabBar extends StatelessWidget {
  bool isCartProduct;
  String price;
  Function onAddToCartButtonPressed;
  TemplateType templateType;
  ProductBottomTabBar(
      {this.isCartProduct, this.price, this.onAddToCartButtonPressed,this.templateType});

  String getButtonText() {
    print(templateType);
    if (isCartProduct == true && templateType?.isMaintenancePackage() == false) {
      return 'Update Cart';
    } else if (isCartProduct == false && templateType?.isMaintenancePackage() == false) {
      return 'Add To Cart';
     } 
    // &&templateType?.isMaintenancePackage() == false
    else if (isCartProduct == false &&
        templateType?.isMaintenancePackage() == true) {
      return 'Buy Package';
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(20.0),
            topRight: const Radius.circular(20.0),
          )),
      child: Padding(
        padding: const EdgeInsets.only(top: 10, left: 0, right: 20, bottom: 47),
        child: Row(
          children: <Widget>[
            BottomSheetIconButton(
              onPressed: () {},
              icon: Icons.favorite,
            ),
            LayoutConstants.sizedBox15H,
            Expanded(
              flex: 3,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 50,
                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Text(
                              getButtonText(),
                              style:
                                  TextConstants.P6_5.apply(color: Colors.white),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Text(
                              'Rs. ' + this.price.toString(),
                              style:
                                  TextConstants.H6_5.apply(color: Colors.white),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                      onPressed: () {
                        onAddToCartButtonPressed();
                      },
                      color: LightColor.orange,
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(15.0),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

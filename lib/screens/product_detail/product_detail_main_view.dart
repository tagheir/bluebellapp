import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_bloc.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/product_detail/product_detail_modal_popup.dart';
import 'package:bluebellapp/screens/product_detail/product_bottom_tab_bar.dart';
import 'package:bluebellapp/screens/product_detail/product_inquiry_tab.dart';
import 'package:bluebellapp/screens/product_detail/product_inquiry_form.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/network_cache_image.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/header_clipper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class ProductDetailMainView extends StatefulWidget {
  final ProductDetailDto productDetail;
  //final ProductDto product;
  final bool isCartProduct;
  final int cartItemId;

  const ProductDetailMainView(
      {Key key,
      this.productDetail,
      //  this.product,
      this.isCartProduct,
      this.cartItemId})
      : super(key: key);
  @override
  _ProductDetailMainViewState createState() => _ProductDetailMainViewState();
}

class _ProductDetailMainViewState extends State<ProductDetailMainView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey _scaffoldKeyChild = GlobalKey();
  ProductDto product;
  ProductBloc productBloc;

  @override
  Widget build(BuildContext context) {
    productBloc  = context.getProductBloc();
    product = productBloc.product;
    return Scaffold(
      key: _scaffoldKey,
      appBar: _getAppBar(),
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white.withOpacity(0.9),
      body: Stack(
        key: _scaffoldKeyChild,
        children: <Widget>[
          SafeArea(
            top: false,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //_appBar(),
                  _getProductImageWidget(),
                  _getProductValuesWidget().padding(AppTheme.padding),
                  Divider().padding(LayoutConstants.edgeInsetsTop10),
                  _getProductDescriptionWidget().padding(AppTheme.hPadding),
                ],
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height - 75,
            child: getBottomTabBarWidget(),
          ),
        ],
      ),
    );
  }

  AppBar _getAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 4,
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => _scaffoldKey.currentContext.moveBack(),
      ),
      actions: <Widget>[
        InkWell(
          onTap: () =>
              _scaffoldKey.currentContext.addEvent(CartItemsViewEvent()),
          child: Container(
            margin: EdgeInsets.only(right: 10, top: 6),
            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: LightColor.orange),
            child: Icon(Icons.shopping_cart, color: Colors.white),
          ),
        )
      ],
    );
  }

  Widget _appBar() {
    return ClipPath(
      clipper: HeaderClipper(),
      child: Container(
        height: 280,
        width: double.infinity,
        decoration: BoxDecoration(
          color: LightColor.orange,
          image: DecorationImage(
              alignment: Alignment(-.2, 0),
              image: NetworkImage(
                ApiRoutes.CdnPath +
                    (widget.productDetail.pictures?.first ?? ""),
              ),
              fit: BoxFit.cover),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Expanded(
            //   child: Stack(
            //     children: <Widget>[
            //       FadeInImage.memoryNetwork(
            //         alignment: Alignment.topCenter, // add this
            //         placeholder: kTransparentImage,
            //         image: ApiRoutes.CdnPath + widget.product.pictures.first,
            //         fit: BoxFit.fill,
            //       ),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  Widget _getProductImageWidget() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      width: double.infinity,
      height: 330,
      decoration: BoxDecoration(
          color: Colors.orange,
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(30.0))),
      child: ClipRRect(
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(30.0)),
        child: NetworkCacheImage(
          altImageUrl: "https://placehold.it/330",
          fit: BoxFit.fill,
          imageUrl: widget.productDetail.pictureWithCdn,
        ),
        // FadeInImage.memoryNetwork(
        //   alignment: Alignment.topCenter, // add this
        //   placeholder: kTransparentImage,
        //   image: ApiRoutes.CdnPath + (widget.productDetail.pictures?.first ?? ""),

        //   fit: BoxFit.fill,
        // ),
      ),
    );
  }

  Widget _getProductValuesWidget() {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text(
            widget.productDetail.name,
            style: TextConstants.H4.apply(color: Colors.grey.shade700),
          ),
        ),
        Expanded(
          flex: widget.productDetail?.price == null ||
                  widget.productDetail.price <= 0
              ? 0
              : 2,
          child: Text(
            widget.productDetail?.price == null ||
                    widget.productDetail.price <= 0
                ? ""
                : 'Rs. ' + widget.productDetail.price.toString(),
            textAlign: TextAlign.right,
            style: TextConstants.H4.apply(color: LightColor.orange),
          ),
        ),
      ],
    );
  }

  Widget _getProductDescriptionWidget() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 8, top: 8, bottom: 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              GeneralStrings.DESCRIPTION,
              style: TextConstants.H5.apply(color: Colors.grey.shade700),
            ),
            Html(
              padding: EdgeInsets.all(12.0),
              data: widget.productDetail.fullDescription,
            ),
            LayoutConstants.sizedBox50H
          ],
        ),
      ),
    );
  }

  Container getBottomTabBarWidget() {
    //print(widget.product.price.toString());
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      // height: 150,
      // width: MediaQuery.of(context).size.width,
      child: widget.productDetail.price == 0
          ? ProductInquiryTab(
              onFormClick: () {
                showDialog(
                    context: _scaffoldKey.currentContext,
                    builder: (BuildContext context) {
                      return ProductInquiryForm();
                    });
              },
            )
          : ProductBottomTabBar(
              isCartProduct: widget.isCartProduct,
              templateType: product?.templateType,
              onAddToCartButtonPressed: () {
                showBottomSheet(
                  context: _scaffoldKeyChild.currentContext,
                  builder: (context) => ProductDetailModalPopup(
                    isCartProduct: widget.isCartProduct,
                    cartItemId: widget.cartItemId,
                    scaffoldKey: _scaffoldKey,
                  ),
                );
              },
              price: widget.productDetail.price.toString(),
            ),
    );
  }
}

import 'dart:io';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/shared/_layout_screen.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/container_cache_image.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/network_cache_image.dart';
import 'package:bluebellapp/screens/widgets/fixed_bottom_button.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custome_alert_dialog.dart';
import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/customer_dto.dart';
import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:bluebellapp/services/base64_encode_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  File selectedFile;
  CustomerDto myInfo;
  bool isDataLoaded = false;
  CustomProgressDialog pr;
  BuildContext cntxt;
  AppBloc appBloc;
  Widget getImageWidget() {
    if (selectedFile != null) {
      return Image.file(
        selectedFile,
        height: 100,
        width: 100,
        fit: BoxFit.cover,
      );
    } else {
      return myInfo?.imageUrl == null
          ? ContainerCacheImage(
              altImageUrl: "https://placehold.it/100",
              imageUrl: "https://placehold.it/100",
              borderRadius: BorderRadius.circular(5.0),
            )
          : ContainerCacheImage(
              altImageUrl: "https://placehold.it/100",
              imageUrl: myInfo.imageUrl,
              borderRadius: BorderRadius.circular(5.0),
            );
    }
  }

  var maskFormatter = MaskTextInputFormatter(
    mask: ' ##########',
    filter: {
      "#": RegExp(r'[0-9]'),
    },
  );

  getImage(ImageSource source) async {
    File image = await ImagePicker.pickImage(source: source);
    //This function is for getting the image from
    // camera or gallery
    if (image != null) {
      File cropped = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatio: CropAspectRatio(ratioX: 0.5, ratioY: 0.5),
        //compressQuality: 100,
        compressQuality: 50,
        maxHeight: 100,
        maxWidth: 100,
        cropStyle: CropStyle.rectangle,
        compressFormat: ImageCompressFormat.jpg,
        androidUiSettings: AndroidUiSettings(
            toolbarWidgetColor: Colors.white,
            toolbarColor: ThemeScheme.lightTheme.primaryColor,
            toolbarTitle: "Bluebell",
            statusBarColor: ThemeScheme.lightTheme.primaryColor,
            backgroundColor: Colors.white),
      );
      this.setState(() {
        selectedFile = cropped;
      });
    }
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  Future<void> updateCustomerInfo() async {
    await pr.showDialog();
    if (selectedFile != null && myInfo != null) {
      myInfo.imageBase64 = EncodeToBase64.encodeImage(image: selectedFile);
    }
    appBloc
        .getRepo()
        .getUserRepository()
        .updateCustomerInfo(
          info: myInfo,
          callBack: (data) {
            setState(() {
              // isDataLoaded = true;
              showInSnackBar(data);
            });
          },
        )
        .then((data) async {
      if (data.id != null) {
        await pr.hideDialog();
        CustomAlertDialog.showNew(cntxt: cntxt, text: 'Profile Updated !');
        appBloc.moveBack();
      }
    });
  }

  getCustomerInfo() {
    appBloc.getRepo().getUserRepository().getProfileInfo(callBack: (data) {
      setState(() {
        isDataLoaded = true;
        showInSnackBar(data);
      });
    }).then((data) {
      if (data != null) {
        setState(() {
          isDataLoaded = true;
          myInfo = data;
        });
      }
    });
  }

  Widget _title() {
    return Container(
      margin: AppTheme.padding,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Profile', style: TextConstants.H6_5),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    appBloc = BlocProvider.of<AppBloc>(context);
    if (pr == null) {
      pr = CustomProgressDialog(context: context);
    }
    if (cntxt == null) {
      cntxt = context;
    }
    if (!isDataLoaded) {
      getCustomerInfo();
    }
    return LayoutScreen(
      scaffoldKey: scaffoldKey,
      defaultAppBar: false,
      showDefaultBackButton: true,
      showBottomNav: true,
      bottomBar: FixedBottomButton(onTap: updateCustomerInfo, text: "Save"),
      body: isDataLoaded == false
          ? LoadingIndicator()
          : myInfo == null ? Text('') : getProfileWidget(context),
    );
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: isDataLoaded == false
          ? LoadingIndicator()
          : myInfo == null ? Text('') : getProfileWidget(context),
    );
  }

  Widget getProfileWidget(BuildContext context) {
    var list = new List<Widget>();
    list.add(_title());
    if (isDataLoaded != true) {
      list.add(Center(child: LoadingIndicator()));
    } else {
      list.add(
        Stack(
          children: <Widget>[
            Container(
              //color: Colors.red,
              width: 100,
              height: 100,
              child: getImageWidget(),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                // color: Colors.red
              ),
            ),
            Positioned(
              left: 80,
              top: 77,
              child: InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return Dialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: _getEditPictureDialogWidget(context),
                      );
                    },
                  );
                },
                child: Container(
                  // height: 55,
                  decoration: const ShapeDecoration(
                    color: Color(0xFFEA7623),
                    shape: CircleBorder(),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Icon(
                      Icons.edit,
                      color: Colors.white,

                      //color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
          overflow: Overflow.visible,
        ),
      );
      list.add(SizedBox(
        height: 35,
      ));
      list.add(
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextFormField(
              // autofocus: true,
              keyboardType: TextInputType.text,
              onChanged: (value) {
                myInfo.firstName = value;
              },
              maxLength: 10,
              initialValue: myInfo?.firstName,
              inputFormatters: [LengthLimitingTextInputFormatter(15)],
              decoration: InputDecoration(labelText: 'First Name'),
            ),
            TextFormField(
              //   autofocus: true,
              keyboardType: TextInputType.text,
              maxLength: 10,
              initialValue: myInfo?.lastName,
              onChanged: (value) {
                myInfo.lastName = value;
              },
              inputFormatters: [LengthLimitingTextInputFormatter(15)],
              decoration: InputDecoration(labelText: 'Last Name'),
            ),
            SizedBox(
              height: 5,
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              initialValue: myInfo?.email,
              onChanged: (value) {
                myInfo.email = value;
              },
              // maxLength: 10,
              inputFormatters: [LengthLimitingTextInputFormatter(16)],
              decoration: InputDecoration(labelText: 'Email'),
            ),
            SizedBox(
              height: 5,
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              // maxLength: 2,
              //  autofocus: true,
              initialValue:
                  myInfo?.phoneNumber == null ? "+92 " : myInfo.phoneNumber,
              maxLength: 13,
              onChanged: (value) {
                myInfo.phoneNumber = value;
              },
              decoration: InputDecoration(
                labelText: 'Phone number',
              ),
            ),
            // TextFormField(
            //   keyboardType:
            //       TextInputType.numberWithOptions(),
            //   obscureText: true,
            //   autofocus: true,
            //   inputFormatters: [
            //     LengthLimitingTextInputFormatter(15),
            //   ],
            //   decoration:
            //       InputDecoration(labelText: 'Password'),
            // ),
          ],
        ),
      );
      list.add(SizedBox(
        // height: MediaQuery.of(context).size.height - 600,
        height: 30,
      ));
      // list.add(Card(
      //   clipBehavior: Clip.antiAlias,
      //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      //   child: ButtonTheme(
      //     height: 50,
      //     minWidth: 400,
      //     child: RaisedButton(
      //       onPressed: () {
      //         updateCustomerInfo();
      //       },
      //       textColor: Colors.white,
      //       child: Text(
      //         'Save',
      //         style: TextStyle(fontSize: 20),
      //       ),
      //       color: ThemeScheme.lightTheme.primaryColor,
      //     ),
      //   ),
      // ));
    }
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 24, top: 24),
      child: ListView(
        children: [
          Container(
              child: Column(
            children: list,
          )),
        ],
      ),
    );
  }

  Container _getEditPictureDialogWidget(BuildContext context) {
    return Container(
      height: 190,
      width: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width,
            // color: Colors.deepOrange,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, top: 16),
              child: Text(
                "Choose Picture From",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: ThemeScheme.lightTheme.primaryColor),
              ),
            ),
          ),
          Divider(
            thickness: 3,
            color: ThemeScheme.lightTheme.primaryColor,
          ),
          Container(
            height: 40,
            width: MediaQuery.of(context).size.width,
            child: InkWell(
                onTap: () {
                  getImage(ImageSource.gallery);
                },
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 5,
                    ),
                    Image.network(
                        "https://purepng.com/public/uploads/large/purepng.com-photos-icon-android-kitkatsymbolsiconsapp-iconsandroid-kitkatandroid-44-721522597661gzej2.png"),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "From Gallery",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    )
                  ],
                )),
          ),
          Divider(
            color: ThemeScheme.lightTheme.primaryColor,
          ),
          Container(
            height: 40,
            width: MediaQuery.of(context).size.width,
            child: InkWell(
              onTap: () {
                getImage(ImageSource.camera);
              },
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 5,
                  ),
                  Image.network(
                      "https://icons.iconarchive.com/icons/dtafalonso/android-lollipop/256/Camera-icon.png"),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Camera",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

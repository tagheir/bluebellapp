import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/category_dto.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/resources/constants/helper_constants/layout_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/template_type.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/network_cache_image.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/empty_data_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FacilitiesManagementServicesGrid extends StatelessWidget {
  final List<CategoryDto> categories;
  FacilitiesManagementServicesGrid({this.categories});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: categories?.length == 0
          ? DataEmpty(
              message: 'No facility management service to show',
            )
          : GridView.count(
              mainAxisSpacing: 20.0,
              crossAxisSpacing: 20.0,
              padding: EdgeInsets.all(10.0),
              crossAxisCount: 2,
              children: List.generate(categories?.length, (index) {
                return ServiceGridViewCard(categories[index]);
              }),
            ),
    )

//

        );
  }
}

class ServiceGridViewCard extends StatelessWidget {
  final CategoryDto category;
  ServiceGridViewCard(this.category);

  @override
  Widget build(BuildContext context) {
    var color = LightColor.grey;
    if (category.templateType == TemplateType.FacilitiesManagement) {
      color = LightColor.orange;
    } else if (category.templateType == TemplateType.Landscape) {
      color = LightColor.green;
    } else if (category.templateType == TemplateType.MaintenancePackage) {
      color = LightColor.lightGrey;
    } else if (category.templateType == TemplateType.Default) {
      color = LightColor.darkGrey;
    }
    return GestureDetector(
      onTap: () {
        category.subCategories.length > 0 ? context.addEvent(
          SubCategoryViewEvent(
            products: category.products,
            category: category,
          ),
        ): context.addEvent(ProductDetailViewEvent(
              productId: category.id, isCardProduct: false));
      },
      child: Container(
        margin: AppTheme.h_5Padding,
        decoration: BoxDecoration(
          color: LightColor.background,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: LightColor.darkGrey,
              blurRadius: 0.5,
              spreadRadius: 0.5,
              offset: Offset(0.5, 1.0),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 100.0,
              width: double.infinity,
              child: NetworkCacheImage(
                altImageUrl: "https://placehold.it/130",
                fit: BoxFit.fill,
                height: 70,
                imageUrl: category.pictureThumbPath,
              ).center(),
            ),
            Divider(
              color: color,
              thickness: 2,
              height: 0.8,
            ).padding(AppTheme.h2Padding),
            Container(
              child: Text(
                category.name,
                textAlign: TextAlign.center,
                style: TextConstants.P7,
              ).padding(LayoutConstants.edgeInsets4),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';
import 'package:bluebellapp/screens/widgets/cache_image_widgets/network_cache_image.dart';
import 'package:flutter/material.dart';

class ProductCompactView extends StatelessWidget {
  final ProductDto product;

  final double height;
  const ProductCompactView({Key key, this.product, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var size = (MediaQuery.of(context).size.width);
    // print(size);
    return GestureDetector(
        onTap: () {
          context.addEvent(ProductDetailViewEvent(
              productId: product.id, isCardProduct: false));
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Card(
            elevation: 8,
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: Column(
              children: <Widget>[
                Container(
                    width: double.infinity,
                    height: this.height * 0.60,
                    child: NetworkCacheImage(
                      altImageUrl: "https://placehold.it/" +
                          (this.height * 0.60).toString(),
                      fit: BoxFit.fill,
                      imageUrl: product.pictureThumb,
                    )
                    // Image.network(
                    //   ApiRoutes.CdnPath + product.pictures.first,
                    //   fit: BoxFit.fill,
                    // ),
                    ),
                Container(
                  height: 70,
                  width: double.infinity,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 2.0, vertical: 4),
                      child: Text(product.name,
                          textAlign: TextAlign.center, style: TextConstants.H6),
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}

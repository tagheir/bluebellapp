import 'package:bluebellapp/models/customerAddress_dto.dart';
import 'package:flutter/material.dart';



class AddressListTile extends StatefulWidget {
  final CustomerAddressDto add;
  final int value;
  int groupValue;
  Function callBack;
  AddressListTile({this.add, this.value, this.groupValue,this.callBack});

  @override
  _AddressListTileState createState() => _AddressListTileState();
}

class _AddressListTileState extends State<AddressListTile> {
  @override
  Widget build(BuildContext context) {
    //` int customselectedRadioTile = 0;

    return RadioListTile(
      value: widget.value,
      groupValue: widget.groupValue,
      onChanged: null,
      title: Align(
        alignment: Alignment.bottomLeft,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 18,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(widget.add.city,style: TextStyle(color: Colors.black,fontWeight: FontWeight.w400),),
                InkWell(
                    onTap: () {
                      widget.callBack();
                    },
                    child: Icon(Icons.edit))
              ],
            )
          ],
        ),
      ),
      
      subtitle: Text(widget.add.address1,style: TextStyle(color: Colors.grey.shade600,fontWeight: FontWeight.w400),),
    );
  }
}

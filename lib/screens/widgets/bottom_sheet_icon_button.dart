import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BottomSheetIconButton extends StatelessWidget {
  final Function() onPressed;
  final IconData icon;
  final IconData faIcon;
  final Color color;
  final Color backgroundColor;

  final int flex;
  const BottomSheetIconButton({
    Key key,
    @required this.onPressed,
    this.icon,
    this.color = Colors.white,
    this.backgroundColor = LightColor.orange,
    this.flex = 1,
    this.faIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: Column(
        children: <Widget>[
          Container(
            width: 60,
            height: 50,
            child: RaisedButton(
              onPressed: onPressed,
              color: backgroundColor,
              padding: EdgeInsets.all(0),
              child: icon != null
                  ? Icon(icon, size: 24, color: color)
                  : FaIcon(faIcon, size: 24, color: color),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

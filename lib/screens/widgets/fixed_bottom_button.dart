import 'package:bluebellapp/resources/constants/helper_constants/text_constants.dart';
import 'package:bluebellapp/resources/themes/light_color.dart';
import 'package:flutter/material.dart';

class FixedBottomButton extends StatelessWidget {
  const FixedBottomButton({
    Key key,
    @required this.onTap,
    this.text,
    this.child,
  }) : super(key: key);

  final Function() onTap;
  final String text;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    final appSize = MediaQuery.of(context).size;
    final height = 60.0;
    //var height = 70.0;
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: LightColor.orange,
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        height: height,
        width: appSize.width,
        child: Center(
          child: child ??
              Text(
                text,
                style: TextConstants.H6_5.apply(
                  color: Colors.white,
                ),
              ),
        ),
      ),
    );
  }
}

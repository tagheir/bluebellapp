import 'package:flutter/material.dart';

class Common {
  

  static Widget buildTile(Widget child, {Function() onTap}) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Material(
          elevation: 16.0,
          borderRadius: BorderRadius.circular(12.0),
          //shadowColor: Color(0x802196F3),
          child: InkWell(
              // Do onTap() if it isn't null, otherwise do print()
              onTap: onTap != null
                  ? () => onTap()
                  : () {
                      print('Not set yet');
                    },
              child: child)),
    );
  }

  static InputBorder getCircularInputBorder() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(48.0)),
      borderSide: BorderSide(width: 0, style: BorderStyle.none),
    );
  }

  static InputDecoration getInputDecoration(
      {bool circularBorder = false,
      Color backgroundColor,
      Color color,
      IconData icon,
      String hintText,
      String labelText,
      String helperText}) {
    return InputDecoration(
        border: circularBorder
            ? Common.getCircularInputBorder()
            : const UnderlineInputBorder(),
        filled: true,
        fillColor: backgroundColor,
        prefixIcon: Icon(icon),
        //prefixStyle: TextStyle(backgroundColor: color),
        hintText: hintText,
        labelText: labelText,
        helperText: helperText);
  }
}

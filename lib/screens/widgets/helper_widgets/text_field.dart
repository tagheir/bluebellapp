import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final IconData prefixIcon;

  const CustomTextField({
    this.fieldKey,
    this.hintText,
    this.labelText,
    this.helperText,
    this.onSaved,
    this.validator,
    this.onFieldSubmitted,
    this.prefixIcon,
    this.onChanged,
    this.autofocus,
    this.inputType,
    this.maxLength,
    this.initialValue
  });

  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final Function onChanged;
  final bool autofocus;
  final TextInputType inputType;
  final int maxLength;
  final String initialValue;
  @override
  _TextFieldState createState() => _TextFieldState();
}

class _TextFieldState extends State<CustomTextField> {
  bool _obscureText = false;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: widget.fieldKey,
      initialValue: widget.initialValue,
      autofocus: true,
      keyboardType: widget.inputType,
      maxLength: widget.maxLength,
      obscureText: _obscureText,
      onSaved: widget.onSaved,
      validator: widget.validator,
      onChanged: widget.onChanged,
      textAlign: TextAlign.left,
      onFieldSubmitted: widget.onFieldSubmitted,
      decoration: InputDecoration(
       border: const UnderlineInputBorder(),
        filled: true,
        prefixIcon: Icon(widget.prefixIcon),
        hintText: widget.hintText,
        labelText: widget.labelText,
        helperText: widget.helperText,
      ),
    );
  }
}

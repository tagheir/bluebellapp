import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SimpleFormField extends StatelessWidget {
  int maxLines = 1;
  TextInputType inputType;
  String label;
  int maxLength;
  String initialValue;
  Function onChanged;
  Function validator;
  SimpleFormField(
      {this.maxLength,
      this.inputType,
      this.label,
      this.maxLines,
      this.initialValue,
      this.validator,
      this.onChanged});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: false,
      maxLength: maxLength,
      maxLines: maxLines,
      onChanged: onChanged,
      validator: validator,
      initialValue: initialValue,
      keyboardType: inputType,
      decoration: InputDecoration(
        labelText: label,
        //   errorText:  'This field is required' ,
      ),
    );
  }
}

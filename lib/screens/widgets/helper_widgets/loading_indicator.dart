import 'package:bluebellapp/resources/theme_scheme.dart';
import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Center(
        child: CircularProgressIndicator(backgroundColor: ThemeScheme.swatchColor,
      ));
}

import 'package:flutter/material.dart';

class DataEmpty extends StatelessWidget {
  final String message;
   DataEmpty({this.message});
  @override
  Widget build(BuildContext context) => Center(
        child: Text(message),
      );
}

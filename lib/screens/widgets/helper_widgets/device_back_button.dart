import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BackButtonWrapper extends StatelessWidget {
  final Widget widget;
  final AppState returnState;
  BackButtonWrapper({this.widget,this.returnState});
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => BlocProvider.of<AppBloc>(context).moveBack(returnState: returnState),
      child: widget,
    );
  }
}

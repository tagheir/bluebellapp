import 'dart:ui';

import 'package:bluebellapp/screens/widgets/helper_widgets/loading_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';


class ContainerCacheImage extends StatefulWidget {
  String imageUrl;
  final double width;
  final double height;
  final BoxFit fit;
  final bool showDetail;
  final bool blurOnly;
  final bool showShadow;
  final BorderRadiusGeometry borderRadius;
  final BoxShape shape;
  final String altImageUrl;
  final double shadowOpacity;

  ContainerCacheImage({
    Key key,
    this.showShadow = false,
    this.showDetail = false,
    this.blurOnly = false,
    this.imageUrl,
    this.width,
    this.height,
    this.fit,
    this.borderRadius,
    this.shape,
    this.altImageUrl,
    this.shadowOpacity,
  }) : super(key: key);
  @override
  _ContainerCacheImageState createState() => _ContainerCacheImageState();
}

class _ContainerCacheImageState extends State<ContainerCacheImage> {
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      fit: widget.fit,
      imageBuilder: (context, imageProvider) {
        return widget.showDetail == true
            ? showDetailImage(imageProvider)
            : Container(
                width: widget.width,
                height: widget.height,
                decoration: buildBoxDecoration(imageProvider),
              );
      },
      imageUrl: widget.imageUrl == null ? widget.altImageUrl : widget.imageUrl,
      placeholder: (context, url) => Center(
        child: Container(
          width: widget.width,
          height: widget.height,
          child: LoadingIndicator(),
        ),
      ),
      errorWidget: (context, url, error) {
        widget.imageUrl = widget.altImageUrl;

        return CachedNetworkImage(
          fit: widget.fit,
          imageBuilder: (context, imageProvider) {
            return Container(
              width: widget.width,
              height: widget.height,
              decoration: buildBoxDecoration(imageProvider),
            );
          },
          imageUrl: widget.altImageUrl,
          placeholder: (context, url) => Center(
            child: Container(
              width: widget.width,
              height: widget.height,
              child: LoadingIndicator(),
            ),
          ),
          errorWidget: (context, url, error) {
            widget.imageUrl = widget.altImageUrl;

            return Container(
              width: widget.width,
              height: widget.height,
              child: Icon(Icons.error),
            );
          },
        );
      },
    );
  }

  Stack showDetailImage(ImageProvider imageProvider) {
    return Stack(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          width: widget.width,
          height: widget.height,
          decoration: buildBoxDecoration(imageProvider),
          child: ClipRect(
              child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              decoration:
                  new BoxDecoration(color: Colors.grey.withOpacity(0.3)),
            ),
          )),
        ),
        widget.blurOnly == true
            ? Text('')
            : Positioned(
                top: widget.height / 12.0,
                left: MediaQuery.of(context).size.width / 4.0,
                child: Container(
                  width: widget.width / 2.0,
                  height: widget.height / 1.2,
                  decoration: buildBoxDecoration(imageProvider),
                ),
              ),
      ],
    );
  }

  BoxDecoration buildBoxDecoration(ImageProvider imageProvider) {
    if (widget.borderRadius != null && widget.showShadow == false) {
      return BoxDecoration(
        borderRadius: widget.borderRadius,
        color: Colors.white.withOpacity(0.3),
        image: DecorationImage(
          image: imageProvider,
          fit: widget.fit,
          colorFilter: widget.blurOnly == true
              ? ColorFilter.mode(
                  Colors.black.withOpacity(0.5), BlendMode.darken)
              : null,
        ),
      );
    } else if (widget.shape != null) {
      return BoxDecoration(
        shape: widget.shape,
        image: DecorationImage(
          image: imageProvider,
          fit: widget.fit,
        ),
      );
    } else if (widget.blurOnly == false && widget.showShadow == true) {
      return BoxDecoration(
        borderRadius: widget.borderRadius,
        color: Colors.white.withOpacity(0.3),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade900.withOpacity(widget.shadowOpacity ?? 0.9),
            blurRadius: 5.0, // soften the shadow
            spreadRadius: 3.0, //extend the shadow
          )
        ],
        image: DecorationImage(
          image: imageProvider,
          fit: widget.fit,
        ),
      );
    } else {
      return BoxDecoration(
        image: DecorationImage(
          image: imageProvider,
          fit: widget.fit,
        ),
      );
    }
  }
}

import 'package:bluebellapp/bloc/master_bloc.dart';
import 'package:bluebellapp/helpers/print_bloc_delegate.dart';
import 'package:bluebellapp/resources/themes/theme.dart';
import 'package:bluebellapp/services/call_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'bloc/bloc/app_bloc.dart';

GetIt locator = GetIt.asNewInstance();
void set() {
  locator.registerSingleton(CallService);
}

void main() {
  BlocSupervisor.delegate = PrintBlocDelegate();
  set();
  runApp(
    BlocProvider<AppBloc>(
      child: MyApp(),
      create: (context) => AppBloc()..add(AppStarted()),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'bluebell',
      debugShowCheckedModeBanner: false,
      theme: AppTheme.lightTheme.copyWith(
        textTheme: GoogleFonts.muliTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
      home: MasterBloc.getMasterBlocBuilder(),
    );
  }
}

class GeneralStrings {
  // Fonts
  static const String FONT_POPPINS = "Poppins";
  static const String FONT_OPEN_SANS = "OpenSans";
  static const String FONT_SFUID_DISPLAY = "SFUIDisplay";

  static const String FONT_PRIMARY = FONT_OPEN_SANS;
  static const String FONT_SECONDARY = FONT_OPEN_SANS;

  static const String SKIP = "Skip";
  static const String NEXT = "Next";
  static const String GET_STARTED = "Get Started !!";
  static const String SLIDER_HEADING_1 = "Easy Exchange!";
  static const String SLIDER_HEADING_2 = "Easy to Use!";
  static const String SLIDER_HEADING_3 = "Connect with Others";
  static const String SLIDER_DESC =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultricies, erat vitae porta consequat.";

  static const String LOGIN = "Login";
  static const String LOGIN_2 = "Log In";
  static const String LOGIN_DESCRIPTION =
      "Please enter your credentials to log in";

  static const String SIGNUP = "Sign Up";
  static const String SIGNUP_DESCRIPTION =
      "Please enter your details to sign up";

  static const String FORGOT_PASSWORD = "Forgot Password";
  static const String FORGOT_PASSWORD_2 = "Forgot Password?";
  static const String FORGOT_PASSWORD_DESCRIPTION = "Forgot your Password?";
  static const String FORGOT_PASSWORD_LINK_TEXT = "Forgot your password?";

  static const String VERIFY_CODE = "Verify Code";
  static const String VERIFY_CODE_DESCRIPTION =
      "Enter the code sent to your number";

  static const String AlreadyHaveAccount = "Already have an account? Log In";
  static const String DoNotHaveAccount = "Don't have an account? Sign Up";

  // Titles
  static const String DESCRIPTION = "Description";
  static const String SEND_INQUIRY = "Send Us Inquiry";
  
  // Infos
  static const String NO_ADDRESSES = "No address to show";
  static const String NO_SERVICES = "No service to show";
  static const String NO_PRODUCTS = "No product to show";

  // Dialog Actions Strings
  static const String YES = "Yes";
  static const String NO = "No";
  static const String CANCEL = "Cancel";
  static const String DELETE = "Delete";
  static const String SUBMIT = "Submit";
  static const String VERIFY = "Verify";
  static const String SEND = "Send";
  static const String SET = "Set";

  // Authentication Strings
  static const String AUTH_TOKEN = "AUTH_TOKEN";
  static const String AUTH_REFRESH_TOKEN = "AUTH_REFRESH_TOKEN";

  // Authentication Errors
  static const String USER_DOES_NOT_EXISTS_GENERAL =
      "User with this email address or phone number does not exist";
  static const String USER_DOES_NOT_EXISTS_EMAIL =
      "User with this email does not exist";
  static const String USER_DOES_NOT_EXISTS_PHONE_NUMBER =
      "User with this phone number does not exist";
  static const String USER_PASSWORD_INCORRECT =
      "Username or password is incorrect !!";
  static const String REQUEST_TIMEOUT = "Request timeout. Please try again !!";
  static const String INVALID_CODE = "Invalid Code !!";
  static const String INVALID_TOKEN = "Invalid Token !!";
  static const String TOKEN_NOT_EXPIRED = "This token hasn't expired yet !!";
  static const String REFRESH_TOKEN_EXPIRED = "This refresh token has expired";
  static const String REFRESH_TOKEN_DOES_NOT_EXISTS =
      "This refresh token does not exist";
  static const String REFRESH_TOKEN_DOES_NOT_MATCH_JWT =
      "This refresh token does not match this JWT";

  static const String REFRESH_TOKEN_ALREADY_USED =
      "This refresh token has been used";
  static const String REFRESH_TOKEN_INVALIDATED =
      "This refresh token has been invalidated";

  static const String SYSTEM_FAILED = "System Failed to Process";

  // Network String
  static const String NO_INTERNET_CONNECTION = "No Internet Connection";
  static const String SERVER_NOT_RESPONDING =
      "Server is not responding at the moment !!";

  /* Enums */

  // Payment Method
  static const String PAYMENT_METHOD_JAZZ_CASH = "Jazz Cash";
  static const String PAYMENT_METHOD_EASY_PAISA = "Easy Paisa";
  static const String PAYMENT_METHOD_CREDIT_CARD = "Credit Card";
  static const String PAYMENT_METHOD_CASH = "Cash";

  // Order
  static const String ORDER_PENDING = "Pending";
  static const String ORDER_PROCESSING = "Processing";
  static const String ORDER_COMPLETE = "Complete";
  static const String ORDER_CANCELLED = "Cancelled";

  // Payment
  static const String PAYMENT_PENDING = "Pending";
  static const String PAYMENT_AUTHORIZED = "Authorized";
  static const String PAYMENT_PAID = "Paid";
  static const String PAYMENT_PARTIALLY_REFUNDED = "Partially Refunded";
  static const String PAYMENT_REFUNDED = "Refunded";
  static const String PAYMENT_VOIDED = "Voided";

  // Shipping Status
  static const String SHIPPING_STATUS_SHIPPING_NOT_REQUIRED =
      "Shipping Not Required";
  static const String SHIPPING_STATUS_NOT_YET_SHIPPED = "Not Yet Shipped";
  static const String SHIPPING_STATUS_PARTIALLY_SHIPPED = "Partially Shipped";
  static const String SHIPPING_STATUS_SHIPPED = "Shipped";
  static const String SHIPPING_STATUS_DELIVERED = "Delivered";

  /* Form Strings */

  // Profile Info
  static const String LAST_NAME_FIELD_HELPER = "Your Last Name";
  static const String LAST_NAME_FIELD_LABEL = "Last Name *";

  static const String FIRST_NAME_FIELD_HELPER = "Your First Name";
  static const String FIRST_NAME_FIELD_LABEL = "First Name *";

  static const String PHONE_NUMBER_FIELD_HELPER = "Your Phone Number";
  static const String PHONE_NUMBER_FIELD_LABEL = "Phone Number *";

  // Email Field
  static const String EMAIL_FIELD_HELPER = "Your Email Address";
  static const String EMAIL_FIELD_LABEL = "E-mail *";
  static const String EMAIL_OR_PHONE_FIELD_HELPER =
      "Your Email Address or Phone Number";
  static const String EMAIL_OR_PHONE_FIELD_LABEL = "E-mail or Phone Number";

  // Password Field
  static const String PASSWORD_FIELD_HELPER = "No more than 8 characters.";
  static const String PASSWORD_FIELD_LABEL = "Password *";

  static const String NO_ORDERS = "No orders to show";
  static const String ORDERS = "Orders";
  static const String MY_ORDERS = "My Orders";
  static const String ORDER_ITEMS = "Order Items";
}

import 'package:bluebellapp/resources/constants/colors.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextConstants {
  // Headings
  static const TextStyle H1 = const TextStyle(
      fontSize: 40,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H2 = const TextStyle(
      fontSize: 32,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H3 = const TextStyle(
      fontSize: 28,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H4 = const TextStyle(
      fontSize: 24,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H5 = const TextStyle(
      fontSize: 20,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H6_5 = const TextStyle(
      fontSize: 18,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H6 = const TextStyle(
      fontSize: 16,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H7 = const TextStyle(
      fontSize: 14,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);
  static const TextStyle H8 = const TextStyle(
      fontSize: 12,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);

  static const TextStyle H9 = const TextStyle(
      fontSize: 10,
      fontFamily: GeneralStrings.FONT_PRIMARY,
      fontWeight: FontWeight.bold);

  static const TextStyle P1 = const TextStyle(
      fontSize: 40,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P2 = const TextStyle(
      fontSize: 32,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P3 = const TextStyle(
      fontSize: 28,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P4 = const TextStyle(
      fontSize: 24,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P5 = const TextStyle(
      fontSize: 20,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P6_5 = const TextStyle(
      fontSize: 18,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P6 = const TextStyle(
      fontSize: 16,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P7 = const TextStyle(
      fontSize: 14,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
  static const TextStyle P8 = const TextStyle(
      fontSize: 12,
      fontFamily: GeneralStrings.FONT_SECONDARY,
      fontWeight: FontWeight.normal);
}

extension TextStyleExtensions on TextStyle {
  TextStyle primary() => this.apply(color: BBColors.primaryColor);
  TextStyle white() => this.apply(color: Colors.white);
}

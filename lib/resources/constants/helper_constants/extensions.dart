import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_bloc.dart';
import 'package:bluebellapp/repos/app_repo.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension WidgetExtensions on Widget {
  Widget center() => Center(child: this);
  Widget borderRadius(double radius) => ClipRRect(
        child: this,
        borderRadius: BorderRadius.circular(radius),
      );
  Widget wrap() => Wrap(children: <Widget>[this]);
  Widget padding(EdgeInsetsGeometry insets) =>
      Padding(padding: insets, child: this);
}

extension WidgetListExtensions on List<Widget> {
  Widget wrap() => Wrap(children: this);
}

extension AppBlocExtensions on BuildContext {
  AppBloc getAppBloc() => BlocProvider.of<AppBloc>(this);
  ProductBloc getProductBloc() => BlocProvider.of<ProductBloc>(this);

  void pushStateToHistory(AppState state) =>
      BlocProvider.of<AppBloc>(this).pushStateToHistory(state);
  void addEvent(AppEvent evt) => BlocProvider.of<AppBloc>(this).add(evt);
  void moveBack() => BlocProvider.of<AppBloc>(this).moveBack();
  AppRepo getRepo() => BlocProvider.of<AppBloc>(this).getRepo();
}

extension KeyExtensions on GlobalKey {
  AppBloc getAppBloc() => BlocProvider.of<AppBloc>(this.currentContext);
  ProductBloc getProductBloc() =>
      BlocProvider.of<ProductBloc>(this.currentContext);

  void pushStateToHistory(AppState state) =>
      (this.currentContext).pushStateToHistory(state);
  void addEvent(AppEvent evt) => (this.currentContext).addEvent(evt);
  void moveBack() => (this.currentContext).moveBack();
  AppRepo getRepo() => (this.currentContext).getRepo();
}

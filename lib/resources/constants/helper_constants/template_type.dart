import 'package:bluebellapp/models/category_name.dart';

enum TemplateType {
  Default,
  Product,
  Landscape,
  FacilitiesManagement,
  MaintenancePackage,
  Simple
}

extension TemplateTypeExtensions on TemplateType {
  bool isService() =>
      this == TemplateType.FacilitiesManagement ||
      this == TemplateType.Landscape;

  bool isFacilitiesManagementService() =>
      this == TemplateType.FacilitiesManagement;

  bool isProduct() => this == TemplateType.Default || this == TemplateType.Product;

  bool isMaintenancePackage() =>
      this == TemplateType.MaintenancePackage ||
      this == TemplateType.Simple ||
      this == TemplateType.Default;
}

class TemplateTypeHelper {
  static const String DefaultCategory =
          'CategoryTemplate.ProductsInGridOrLines',
      LandscapeCategory = 'CategoryTemplate.LandscapeService',
      FacilitiesManagementCategory =
          'CategoryTemplate.FacilitiesManagementService',
      MaintenancePackageCategory = 'CategoryTemplate.Packages';

  static const String DefaultProduct = 'ProductTemplate.Simple',
      LandscapeProduct = 'ProductTemplate.LandscapeService',
      FacilitiesManagementProduct =
          'ProductTemplate.FacilitiesManagementService',
      MaintenancePackageProduct = 'ProductTemplate.Package';

  static TemplateType getTemplateTypeOfProduct(String templatePath) {
    switch (templatePath) {
      case LandscapeProduct:
        return TemplateType.Landscape;
      case FacilitiesManagementProduct:
        return TemplateType.FacilitiesManagement;
      case MaintenancePackageProduct:
        return TemplateType.MaintenancePackage;
      default:
        return TemplateType.Product;
    }
  }

  static TemplateType getTemplateTypeOfCategory(String templatePath) {
    switch (templatePath) {
      case LandscapeCategory:
        return TemplateType.Landscape;
      case FacilitiesManagementCategory:
        return TemplateType.FacilitiesManagement;
      case MaintenancePackageCategory:
        return TemplateType.MaintenancePackage;
      default:
        return TemplateType.Product;
    }
  }
}

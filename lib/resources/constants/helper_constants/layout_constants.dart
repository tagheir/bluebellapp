import 'package:flutter/cupertino.dart';

class LayoutConstants {
  // Sized Boxes
  static const SizedBox sizedBox5H = const SizedBox(height: 5.0);
  static const SizedBox sizedBox10H = const SizedBox(height: 10.0);
  static const SizedBox sizedBox15H = const SizedBox(height: 15.0);
  static const SizedBox sizedBox20H = const SizedBox(height: 20.0);
  static const SizedBox sizedBox25H = const SizedBox(height: 25.0);
  static const SizedBox sizedBox30H = const SizedBox(height: 30.0);
  static const SizedBox sizedBox35H = const SizedBox(height: 35.0);
  static const SizedBox sizedBox40H = const SizedBox(height: 40.0);
  static const SizedBox sizedBox45H = const SizedBox(height: 45.0);
  static const SizedBox sizedBox50H = const SizedBox(height: 50.0);

  /* EdgeInsets */
  static const EdgeInsetsGeometry edgeInsets0 = const EdgeInsets.all(0);
  static const EdgeInsetsGeometry edgeInsets2 = const EdgeInsets.all(2);
  static const EdgeInsetsGeometry edgeInsets4 = const EdgeInsets.all(4);
  static const EdgeInsetsGeometry edgeInsets8 = const EdgeInsets.all(8);
  static const EdgeInsetsGeometry edgeInsets16 = const EdgeInsets.all(16);
  static const EdgeInsetsGeometry edgeInsets24 = const EdgeInsets.all(24);
  static const EdgeInsetsGeometry edgeInsets32 = const EdgeInsets.all(32);
  // EdgeInsets Top
  static const EdgeInsetsGeometry edgeInsetsTop5 =
      const EdgeInsets.only(top: 5);
  static const EdgeInsetsGeometry edgeInsetsTop10 =
      const EdgeInsets.only(top: 10);
  static const EdgeInsetsGeometry edgeInsetsTop15 =
      const EdgeInsets.only(top: 15);
  static const EdgeInsetsGeometry edgeInsetsTop20 =
      const EdgeInsets.only(top: 20);
  static const EdgeInsetsGeometry edgeInsetsTop25 =
      const EdgeInsets.only(top: 25);
  static const EdgeInsetsGeometry edgeInsetsTop30 =
      const EdgeInsets.only(top: 30);
  static const EdgeInsetsGeometry edgeInsetsTop35 =
      const EdgeInsets.only(top: 35);
  static const EdgeInsetsGeometry edgeInsetsTop40 =
      const EdgeInsets.only(top: 40);
  static const EdgeInsetsGeometry edgeInsetsTop45 =
      const EdgeInsets.only(top: 45);
  static const EdgeInsetsGeometry edgeInsetsTop50 =
      const EdgeInsets.only(top: 50);

  // EdgeInsets Horizontal
  static const EdgeInsetsGeometry edgeInsetsH25 =
      const EdgeInsets.symmetric(horizontal: 25);

  static const EdgeInsetsGeometry edgeInsets6H12V =
      const EdgeInsets.symmetric(vertical: 12, horizontal: 6);

  // Border Radius Shape
  static ShapeBorder shapeBorderRadius10 =
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(10));
}

extension EdgeInsetsExtension on EdgeInsets {
  EdgeInsets setTop(double value) {
    return this.add(EdgeInsets.only(top: value - this.top));
  }

  EdgeInsets setBottom(double value) {
    return this.add(EdgeInsets.only(bottom: value - this.bottom));
  }

  EdgeInsets setLeft(double value) {
    return this.add(EdgeInsets.only(left: value - this.left));
  }

  EdgeInsets setRight(double value) {
    return this.add(EdgeInsets.only(right: value - this.right));
  }
}

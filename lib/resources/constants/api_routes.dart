class ApiRoutes {
  // static const String ApiBase = "https://construction.octacer.net/api";
  static const String Query =
      "https://construction.octacer.net/api/rq_post_user";

  static const String CdnPath =
      "https://bluebellcdnstorage.s3.ap-south-1.amazonaws.com/"; //"https://nopstorageaccount.blob.core.windows.net/content/";

//  static const String ApiBase = "http://192.168.0.104/bluebell/api";
  static const String ApiBase = "https://bluebellapi.octacer.com/api";

  //Home Page View Data

  // Authentication
  static const String AuthBase = ApiBase + "/identity";
  static const String VerifyToken = ApiBase + "/verifyAuthentication";
  static const String Login = AuthBase + "/login";
  static const String LoginWithP = AuthBase + "/loginphonenumber";
  static const String TwoFactor = AuthBase + "/twofactor";
  static const String Register = AuthBase + "/register";
  static const String RefreshToken = AuthBase + "/refresh";
  static const String Claims = AuthBase + "/claims";
}

// Project Info

class Category {
  static const String CategoryBase = ApiRoutes.ApiBase + "/category";
  static const String GetHomePageView = CategoryBase + "/homepageview";
}

class Product {
  static const String BaseIdentity = ApiRoutes.ApiBase + "/products";
  static const String Search = ApiRoutes.ApiBase + "/search";
  static const String GetProductById = BaseIdentity + "/{id}";
}

class Customer {
  static const String BaseIdentity = ApiRoutes.ApiBase + "/customer";
  static const String GetAllCustomers = BaseIdentity;
  static const String GetCustomerByIdOrGuid = BaseIdentity + "/{customerId}";
  static const String CreateAddress = BaseIdentity + "/address";
}

class Cart {
  static const String CartBase = ApiRoutes.ApiBase + "/cart";
  static const String CartByCustomerId = CartBase;
  static const String AddItemsToCart =
      CartBase + "/{productId}/{shoppingCartTypeId}/{quantity}";
  static const String UpdateCart =
      CartBase + "/{cartItemId}/{quantity}/{shoppingCartTypeId}/{productId}";
  static const String DeleteCart = CartBase + "/{cartItemId}";
}

class Order {
  static const String BaseIdentity = ApiRoutes.ApiBase + "/order";
  static const String GetCustomerOrder = BaseIdentity;
  static const String GetOrderById = BaseIdentity + "/{orderId}";
  static const String CreateOrderByShoppingCart = BaseIdentity;
  static const String CreateDirectOrder = ApiRoutes.ApiBase + "/direct-order";
}

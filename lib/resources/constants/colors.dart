import 'package:flutter/material.dart';

class BBColors{
  static const Color primaryColor = Color(0xffEA7623);
  static const Color textColorOverPrimaryBase = Colors.white;
  
  static const Color secondaryColor = Color(0xffEA7623);
  static const Color textColorOverSecondaryBase = Colors.white;
}
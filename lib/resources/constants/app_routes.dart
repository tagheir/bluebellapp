class AppRoutes{
  
  // Unit Info
  static const String UnitInfoBase = "/unit-info";
  static const String GetAllUnitInfos = UnitInfoBase;
  static const String AddUnitInfo = UnitInfoBase;
}
import 'dart:convert';

class AddToCartDto {
  int productId;
  int shoppingCartTypeId = 1;
  int quantity = 1;
  int cartItemId;
  List<AttrMap> form = List<AttrMap>();
  DirectOrderProductAttribute directOrderProductAttribute = DirectOrderProductAttribute();

}

class DirectOrderProductAttribute{
   double price;
   String dropDownAttributeValue;
   String pictureThumb;
   String name;
   DirectOrderProductAttribute({this.price,this.dropDownAttributeValue,this.pictureThumb});
}
class AttrMap {
  String key;
  String value;

  AttrMap({this.key, this.value});

  AttrMap.fromJson(Map<String, dynamic> json) {
    key = json['Key'];
    value = json['Value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Key'] = this.key;
    data['Value'] = this.value;
    return data;
  }

  static String encodeList({List<AttrMap> attr}) {
    return json.encode(attr);
  }
}

// Map<String, dynamic> toJson(HashMap<String,String> hash) {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data[hash.keys[0]] = this.name;
//     data['Value'] = this.value;
//     return data;
//   }

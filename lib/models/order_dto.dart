import 'dart:convert';

import 'package:bluebellapp/models/attribute_dto.dart';
import 'package:bluebellapp/models/customerAddress_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';

class OrderDto {
  String orderGuid;
  int storeId;
  int customerId;
  int billingAddressId;
  int shippingAddressId;
  int pickupAddressId;
  bool pickupInStore;
  int orderStatusId;
  int shippingStatusId;
  int paymentStatusId;
  String paymentMethodSystemName;
  String customerCurrencyCode;
  double currencyRate;
  int customerTaxDisplayTypeId;
  String vatNumber;
  double orderSubtotalInclTax;
  double orderSubtotalExclTax;
  double orderSubTotalDiscountInclTax;
  double orderSubTotalDiscountExclTax;
  double orderShippingInclTax;
  double orderShippingExclTax;
  double paymentMethodAdditionalFeeInclTax;
  double paymentMethodAdditionalFeeExclTax;
  String taxRates;
  double orderTax;
  double orderDiscount;
  double orderTotal;
  double refundedAmount;
  int rewardPointsHistoryEntryId;
  String checkoutAttributeDescription;
  String checkoutAttributesXml;
  int customerLanguageId;
  int affiliateId;
  String customerIp;
  bool allowStoringCreditCardNumber;
  String cardType;
  String cardName;
  String cardNumber;
  String maskedCreditCardNumber;
  String cardCvv2;
  String cardExpirationMonth;
  String cardExpirationYear;
  String authorizationTransactionId;
  String authorizationTransactionCode;
  String authorizationTransactionResult;
  String captureTransactionId;
  String captureTransactionResult;
  String subscriptionTransactionId;
  String paidDateUtc;
  String shippingMethod;
  String shippingRateComputationMethodSystemName;
  String customValuesXml;
  bool deleted;
  String createdOnUtc;
  String customOrderNumber;
  CustomerAddressDto billingAddress;
  CustomerAddressDto shippingAddress;
  OrderItems orderItems;
  String billingAddressXml;
  String shippingAddressXml;
  String orderItemsXml;
  int orderStatus;
  int paymentStatus;
  int shippingStatus;
  int id;

  OrderDto(
      {this.orderGuid,
      this.storeId,
      this.customerId,
      this.billingAddressId,
      this.shippingAddressId,
      this.pickupAddressId,
      this.pickupInStore,
      this.orderStatusId,
      this.shippingStatusId,
      this.paymentStatusId,
      this.paymentMethodSystemName,
      this.customerCurrencyCode,
      this.currencyRate,
      this.customerTaxDisplayTypeId,
      this.vatNumber,
      this.orderSubtotalInclTax,
      this.orderSubtotalExclTax,
      this.orderSubTotalDiscountInclTax,
      this.orderSubTotalDiscountExclTax,
      this.orderShippingInclTax,
      this.orderShippingExclTax,
      this.paymentMethodAdditionalFeeInclTax,
      this.paymentMethodAdditionalFeeExclTax,
      this.taxRates,
      this.orderTax,
      this.orderDiscount,
      this.orderTotal,
      this.refundedAmount,
      this.rewardPointsHistoryEntryId,
      this.checkoutAttributeDescription,
      this.checkoutAttributesXml,
      this.customerLanguageId,
      this.affiliateId,
      this.customerIp,
      this.allowStoringCreditCardNumber,
      this.cardType,
      this.cardName,
      this.cardNumber,
      this.maskedCreditCardNumber,
      this.cardCvv2,
      this.cardExpirationMonth,
      this.cardExpirationYear,
      this.authorizationTransactionId,
      this.authorizationTransactionCode,
      this.authorizationTransactionResult,
      this.captureTransactionId,
      this.captureTransactionResult,
      this.subscriptionTransactionId,
      this.paidDateUtc,
      this.shippingMethod,
      this.shippingRateComputationMethodSystemName,
      this.customValuesXml,
      this.deleted,
      this.createdOnUtc,
      this.customOrderNumber,
      this.billingAddress,
      this.shippingAddress,
      this.orderItems,
      this.billingAddressXml,
      this.shippingAddressXml,
      this.orderItemsXml,
      this.orderStatus,
      this.paymentStatus,
      this.shippingStatus,
      this.id});

  factory OrderDto.fromJson(String str) => OrderDto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  static List<OrderDto> parseJsonList(String jsonStr) {
    List<OrderDto> list = null;
    try {
      if (jsonStr.isNotEmpty) {
        list = (json.decode(jsonStr) as List)
            .map((data) => OrderDto.fromMap(data))
            .toList();
      }
    } catch (ex) {}
    return list;
  }

  OrderDto.fromMap(Map<String, dynamic> json) {
    orderGuid = json['orderGuid'];
    storeId = json['storeId'];
    customerId = json['customerId'];
    billingAddressId = json['billingAddressId'];
    shippingAddressId = json['shippingAddressId'];
    pickupAddressId = json['pickupAddressId'];
    pickupInStore = json['pickupInStore'];
    orderStatusId = json['orderStatusId'];
    shippingStatusId = json['shippingStatusId'];
    paymentStatusId = json['paymentStatusId'];
    paymentMethodSystemName = json['paymentMethodSystemName'];
    customerCurrencyCode = json['customerCurrencyCode'];
    currencyRate = json['currencyRate'];
    customerTaxDisplayTypeId = json['customerTaxDisplayTypeId'];
    vatNumber = json['vatNumber'];
    orderSubtotalInclTax = json['orderSubtotalInclTax'];
    orderSubtotalExclTax = json['orderSubtotalExclTax'];
    orderSubTotalDiscountInclTax = json['orderSubTotalDiscountInclTax'];
    orderSubTotalDiscountExclTax = json['orderSubTotalDiscountExclTax'];
    orderShippingInclTax = json['orderShippingInclTax'];
    orderShippingExclTax = json['orderShippingExclTax'];
    paymentMethodAdditionalFeeInclTax =
        json['paymentMethodAdditionalFeeInclTax'];
    paymentMethodAdditionalFeeExclTax =
        json['paymentMethodAdditionalFeeExclTax'];
    taxRates = json['taxRates'];
    orderTax = json['orderTax'];
    orderDiscount = json['orderDiscount'];
    orderTotal = json['orderTotal'];
    refundedAmount = json['refundedAmount'];
    rewardPointsHistoryEntryId = json['rewardPointsHistoryEntryId'];
    checkoutAttributeDescription = json['checkoutAttributeDescription'];
    checkoutAttributesXml = json['checkoutAttributesXml'];
    customerLanguageId = json['customerLanguageId'];
    affiliateId = json['affiliateId'];
    customerIp = json['customerIp'];
    allowStoringCreditCardNumber = json['allowStoringCreditCardNumber'];
    cardType = json['cardType'];
    cardName = json['cardName'];
    cardNumber = json['cardNumber'];
    maskedCreditCardNumber = json['maskedCreditCardNumber'];
    cardCvv2 = json['cardCvv2'];
    cardExpirationMonth = json['cardExpirationMonth'];
    cardExpirationYear = json['cardExpirationYear'];
    authorizationTransactionId = json['authorizationTransactionId'];
    authorizationTransactionCode = json['authorizationTransactionCode'];
    authorizationTransactionResult = json['authorizationTransactionResult'];
    captureTransactionId = json['captureTransactionId'];
    captureTransactionResult = json['captureTransactionResult'];
    subscriptionTransactionId = json['subscriptionTransactionId'];
    paidDateUtc = json['paidDateUtc'];
    shippingMethod = json['shippingMethod'];
    shippingRateComputationMethodSystemName =
        json['shippingRateComputationMethodSystemName'];
    customValuesXml = json['customValuesXml'];
    deleted = json['deleted'];
    createdOnUtc = json['createdOnUtc'];
    customOrderNumber = json['customOrderNumber'];
     //print("================before billing address =============");
     //print(json['billingAddress'].toString());
    billingAddress = json['billingAddress']['address'] != null
        ? CustomerAddressDto.fromMap(json['billingAddress']['address'])
        : null;
    shippingAddress = json['shippingAddress'] != null
        ? CustomerAddressDto.fromMap(json['shippingAddress'])
        : null;
    //print("================before order items =============");
    orderItems = json['orderItems'] != null
        ? OrderItems.fromJson(json['orderItems'])
        : null;
    //print("================after order items =============");
    billingAddressXml = json['billingAddressXml'];
    shippingAddressXml = json['shippingAddressXml'];
    orderItemsXml = json['orderItemsXml'];
    orderStatus = json['orderStatus'];
    paymentStatus = json['paymentStatus'];
    shippingStatus = json['shippingStatus'];
    id = json['id'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderGuid'] = this.orderGuid;
    data['storeId'] = this.storeId;
    data['customerId'] = this.customerId;
    data['billingAddressId'] = this.billingAddressId;
    data['shippingAddressId'] = this.shippingAddressId;
    data['pickupAddressId'] = this.pickupAddressId;
    data['pickupInStore'] = this.pickupInStore;
    data['orderStatusId'] = this.orderStatusId;
    data['shippingStatusId'] = this.shippingStatusId;
    data['paymentStatusId'] = this.paymentStatusId;
    data['paymentMethodSystemName'] = this.paymentMethodSystemName;
    data['customerCurrencyCode'] = this.customerCurrencyCode;
    data['currencyRate'] = this.currencyRate;
    data['customerTaxDisplayTypeId'] = this.customerTaxDisplayTypeId;
    data['vatNumber'] = this.vatNumber;
    data['orderSubtotalInclTax'] = this.orderSubtotalInclTax;
    data['orderSubtotalExclTax'] = this.orderSubtotalExclTax;
    data['orderSubTotalDiscountInclTax'] = this.orderSubTotalDiscountInclTax;
    data['orderSubTotalDiscountExclTax'] = this.orderSubTotalDiscountExclTax;
    data['orderShippingInclTax'] = this.orderShippingInclTax;
    data['orderShippingExclTax'] = this.orderShippingExclTax;
    data['paymentMethodAdditionalFeeInclTax'] =
        this.paymentMethodAdditionalFeeInclTax;
    data['paymentMethodAdditionalFeeExclTax'] =
        this.paymentMethodAdditionalFeeExclTax;
    data['taxRates'] = this.taxRates;
    data['orderTax'] = this.orderTax;
    data['orderDiscount'] = this.orderDiscount;
    data['orderTotal'] = this.orderTotal;
    data['refundedAmount'] = this.refundedAmount;
    data['rewardPointsHistoryEntryId'] = this.rewardPointsHistoryEntryId;
    data['checkoutAttributeDescription'] = this.checkoutAttributeDescription;
    data['checkoutAttributesXml'] = this.checkoutAttributesXml;
    data['customerLanguageId'] = this.customerLanguageId;
    data['affiliateId'] = this.affiliateId;
    data['customerIp'] = this.customerIp;
    data['allowStoringCreditCardNumber'] = this.allowStoringCreditCardNumber;
    data['cardType'] = this.cardType;
    data['cardName'] = this.cardName;
    data['cardNumber'] = this.cardNumber;
    data['maskedCreditCardNumber'] = this.maskedCreditCardNumber;
    data['cardCvv2'] = this.cardCvv2;
    data['cardExpirationMonth'] = this.cardExpirationMonth;
    data['cardExpirationYear'] = this.cardExpirationYear;
    data['authorizationTransactionId'] = this.authorizationTransactionId;
    data['authorizationTransactionCode'] = this.authorizationTransactionCode;
    data['authorizationTransactionResult'] =
        this.authorizationTransactionResult;
    data['captureTransactionId'] = this.captureTransactionId;
    data['captureTransactionResult'] = this.captureTransactionResult;
    data['subscriptionTransactionId'] = this.subscriptionTransactionId;
    data['paidDateUtc'] = this.paidDateUtc;
    data['shippingMethod'] = this.shippingMethod;
    data['shippingRateComputationMethodSystemName'] =
        this.shippingRateComputationMethodSystemName;
    data['customValuesXml'] = this.customValuesXml;
    data['deleted'] = this.deleted;
    data['createdOnUtc'] = this.createdOnUtc;
    data['customOrderNumber'] = this.customOrderNumber;
    if (this.billingAddress != null) {
      data['billingAddress'] = this.billingAddress.toJson();
    }
    if (this.shippingAddress != null) {
      data['shippingAddress'] = this.shippingAddress.toJson();
    }
    if (this.orderItems != null) {
      data['orderItems'] = this.orderItems.toJson();
    }
    data['billingAddressXml'] = this.billingAddressXml;
    data['shippingAddressXml'] = this.shippingAddressXml;
    data['orderItemsXml'] = this.orderItemsXml;
    data['orderStatus'] = this.orderStatus;
    data['paymentStatus'] = this.paymentStatus;
    data['shippingStatus'] = this.shippingStatus;
    data['id'] = this.id;
    return data;
  }
}

// class OrderItems {
//   OrderItems orderItems;

//   OrderItems({this.orderItems});

//   OrderItems.fromJson(Map<String, dynamic> json) {
//     orderItems = json['orderItems'] != null
//         ?  OrderItems.fromJson(json['orderItems'])
//         : null;
//       //print(orderItems);
//   }
  
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.orderItems != null) {
//       data['orderItems'] = this.orderItems.toJson();
//     }
//     return data;
//   }
// }


class OrderItems {
  List<OrderItem> orderItem;

  OrderItems({this.orderItem});
  
  OrderItems.fromJson(Map<String, dynamic> json) {
     //print("==========parse order items===========");
    // //print(json['orderItems']['orderItem'].toString());
    if (json['orderItems'] != null) {
      orderItem = List<OrderItem>();
      json['orderItems']['orderItem'].forEach((v) {
        orderItem.add(OrderItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderItem != null) {
      data['orderItem'] = this.orderItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderItem {
  int id;
  String orderItemGuid;
  int orderId;
  int productId;
  int quantity;
  double unitPriceInclTax;
  double unitPriceExclTax;
  double priceInclTax;
  double priceExclTax;
  double discountAmountInclTax;
  double discountAmountExclTax;
  double originalProductCost;
  String attributeDescription;
  String attributesXml;
  Attributes attributes;
  int downloadCount;
  bool isDownloadActivated;
  String licenseDownloadId;
  String productName;
  ProductDto productDto;

  OrderItem(
      {this.id,
      this.orderItemGuid,
      this.orderId,
      this.productId,
      this.quantity,
      this.unitPriceInclTax,
      this.unitPriceExclTax,
      this.priceInclTax,
      this.priceExclTax,
      this.discountAmountInclTax,
      this.discountAmountExclTax,
      this.originalProductCost,
      this.attributeDescription,
      this.attributesXml,
      this.downloadCount,
      this.isDownloadActivated,
      this.licenseDownloadId,
      this.attributes,
      this.productName});

  OrderItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderItemGuid = json['orderItemGuid'];
    orderId = json['orderId'];
    productId = json['productId'];
    quantity = json['quantity'];
    unitPriceInclTax = json['unitPriceInclTax'];
    unitPriceExclTax = json['unitPriceExclTax'];
    priceInclTax = json['priceInclTax'];
    priceExclTax = json['priceExclTax'];
    discountAmountInclTax = json['discountAmountInclTax'];
    discountAmountExclTax = json['discountAmountExclTax'];
    originalProductCost = json['originalProductCost'];
    attributeDescription = json['attributeDescription'];
    attributesXml = json['attributesXml'];
    attributes = json['attributes'] != null
        ? new Attributes.fromJson(json['attributes'])
        : null;
    downloadCount = json['downloadCount'];
    isDownloadActivated = json['isDownloadActivated'];
    licenseDownloadId = json['licenseDownloadId'];
    productName = json['productName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['orderItemGuid'] = this.orderItemGuid;
    data['orderId'] = this.orderId;
    data['productId'] = this.productId;
    data['quantity'] = this.quantity;
    data['unitPriceInclTax'] = this.unitPriceInclTax;
    data['unitPriceExclTax'] = this.unitPriceExclTax;
    data['priceInclTax'] = this.priceInclTax;
    data['priceExclTax'] = this.priceExclTax;
    data['discountAmountInclTax'] = this.discountAmountInclTax;
    data['discountAmountExclTax'] = this.discountAmountExclTax;
    data['originalProductCost'] = this.originalProductCost;
    data['attributeDescription'] = this.attributeDescription;
    data['attributesXml'] = this.attributesXml;
    if (this.attributes != null) {
      data['attributes'] = this.attributes.toJson();
    }
    data['downloadCount'] = this.downloadCount;
    data['isDownloadActivated'] = this.isDownloadActivated;
    data['licenseDownloadId'] = this.licenseDownloadId;
    data['productName'] = this.productName;
    return data;
  }
}

import 'dart:convert';

import 'package:bluebellapp/models/category_dto.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/helpers/common_extensions.dart';

class ProductDetailDto {
  ProductAttributes productAttributes;
  int id;
  String updatedOnUtc;
  String createdOnUtc;
  bool deleted;
  bool published;
  String displayOrder;
  String height;
  String width;
  String length;
  String weight;
  String hasDiscountsApplied;
  String hasTierPrices;
  String markAsNew;
  String basepriceBaseUnitId;
  String basepriceBaseAmount;
  String basepriceUnitId;
  String basepriceAmount;
  String basepriceEnabled;
  int maximumCustomerEnteredPrice;
  int minimumCustomerEnteredPrice;
  bool customerEntersPrice;
  int productCost;
  int oldPrice;
  int price;
  bool callForPrice;
  bool availableForPreOrder;
  bool disableWishlistButton;
  bool disableBuyButton;
  String notReturnable;
  String allowAddingOnlyExistingAttributeCombinations;
  int orderMaximumQuantity;
  int orderMinimumQuantity;
  String allowBackInStockSubscriptions;
  String backorderModeId;
  String notifyAdminForQuantityBelow;
  String lowStockActivityId;
  String minStockQuantity;
  String displayStockQuantity;
  String displayStockAvailability;
  String stockQuantity;
  String warehouseId;
  String useMultipleWarehouses;
  String productAvailabilityRangeId;
  String manageInventoryMethodId;
  String isTelecommunicationsOrBroadcastingOrElectronicServices;
  String taxCategoryId;
  String isTaxExempt;
  String deliveryDateId;
  String additionalShippingCharge;
  String shipSeparately;
  String isFreeShipping;
  String isShipEnabled;
  String rentalPricePeriodId;
  String rentalPriceLength;
  bool isRental;
  int recurringTotalCycles;
  int recurringCyclePeriodId;
  int recurringCyclePeriod;
  int recurringCycleLength;
  bool isRecurring;
  String hasUserAgreement;
  String sampleDownloadId;
  String hasSampleDownload;
  String downloadActivationTypeId;
  String maxNumberOfDownloads;
  String unlimitedDownloads;
  String downloadId;
  String isDownload;
  String automaticallyAddRequiredProducts;
  bool requireOtherProducts;
  String requiredProductIds;
  int giftCardTypeId;
  int giftCardType;
  bool isGiftCard;
  String limitedToStores;
  String subjectToAcl;
  String notApprovedTotalReviews;
  String approvedTotalReviews;
  String notApprovedRatingSum;
  String approvedRatingSum;
  String allowCustomerReviews;
  String showOnHomepage;
  String vendorId;
  String productTemplateId;
  String fullDescription;
  String name;
  String visibleIndividually;
  String parentGroupedProductId;
  int productTypeId;
  int productType;
  String seName;
  String picturesJson;
  int categoryId;
  CategoryDto category;
  List<String> pictures;
  List<String> picturesWithCdn;
  String picture;
  String pictureWithCdn;

  ProductDetailDto(
      {this.productAttributes,
      this.id,
      this.updatedOnUtc,
      this.createdOnUtc,
      this.deleted,
      this.published,
      this.displayOrder,
      this.height,
      this.width,
      this.length,
      this.weight,
      this.hasDiscountsApplied,
      this.hasTierPrices,
      this.markAsNew,
      this.basepriceBaseUnitId,
      this.basepriceBaseAmount,
      this.basepriceUnitId,
      this.basepriceAmount,
      this.basepriceEnabled,
      this.maximumCustomerEnteredPrice,
      this.minimumCustomerEnteredPrice,
      this.customerEntersPrice,
      this.productCost,
      this.oldPrice,
      this.price,
      this.callForPrice,
      this.availableForPreOrder,
      this.disableWishlistButton,
      this.disableBuyButton,
      this.notReturnable,
      this.allowAddingOnlyExistingAttributeCombinations,
      this.orderMaximumQuantity,
      this.orderMinimumQuantity,
      this.allowBackInStockSubscriptions,
      this.backorderModeId,
      this.notifyAdminForQuantityBelow,
      this.lowStockActivityId,
      this.minStockQuantity,
      this.displayStockQuantity,
      this.displayStockAvailability,
      this.stockQuantity,
      this.warehouseId,
      this.useMultipleWarehouses,
      this.productAvailabilityRangeId,
      this.manageInventoryMethodId,
      this.isTelecommunicationsOrBroadcastingOrElectronicServices,
      this.taxCategoryId,
      this.isTaxExempt,
      this.deliveryDateId,
      this.additionalShippingCharge,
      this.shipSeparately,
      this.isFreeShipping,
      this.isShipEnabled,
      this.rentalPricePeriodId,
      this.rentalPriceLength,
      this.isRental,
      this.recurringTotalCycles,
      this.recurringCyclePeriodId,
      this.recurringCyclePeriod,
      this.recurringCycleLength,
      this.isRecurring,
      this.hasUserAgreement,
      this.sampleDownloadId,
      this.hasSampleDownload,
      this.downloadActivationTypeId,
      this.maxNumberOfDownloads,
      this.unlimitedDownloads,
      this.downloadId,
      this.isDownload,
      this.automaticallyAddRequiredProducts,
      this.requireOtherProducts,
      this.requiredProductIds,
      this.giftCardTypeId,
      this.giftCardType,
      this.isGiftCard,
      this.limitedToStores,
      this.subjectToAcl,
      this.notApprovedTotalReviews,
      this.approvedTotalReviews,
      this.notApprovedRatingSum,
      this.approvedRatingSum,
      this.allowCustomerReviews,
      this.showOnHomepage,
      this.vendorId,
      this.productTemplateId,
      this.fullDescription,
      this.name,
      this.visibleIndividually,
      this.parentGroupedProductId,
      this.productTypeId,
      this.productType,
      this.seName,
      this.picturesJson,
      this.categoryId,
      this.category,
      this.pictures,
      this.picturesWithCdn});

  factory ProductDetailDto.fromJson(String str) =>
      ProductDetailDto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  ProductDetailDto.fromMap(Map<String, dynamic> json) {
    //print("======1");
    productAttributes = json.get('productAttributes') != null
        ? ProductAttributes.fromJson(json.get('productAttributes'))
        : null;
    id = json.get('id') == null ? 0 : json.get('id');
    //print("======2");
    updatedOnUtc = json.get('updatedOnUtc');
    //print("======3");
    createdOnUtc = json.get('createdOnUtc');
    deleted = json.get('deleted');
    published = json.get('published');
    displayOrder = json.get('displayOrder');
    height = json.get('height');
    width = json.get('width');
    length = json.get('length');
    weight = json.get('weight');
    //print("======4");
    hasDiscountsApplied = json.get('hasDiscountsApplied');
    //print("======5");
    hasTierPrices = json.get('hasTierPrices');
    //print("======6");
    markAsNew = json.get('markAsNew');
    //print("======7");
    basepriceBaseUnitId = json.get('basepriceBaseUnitId');
    basepriceBaseAmount = json.get('basepriceBaseAmount');
    basepriceUnitId = json.get('basepriceUnitId');
    //  //print("======8");
    basepriceAmount = json.get('basepriceAmount');
    //print("======9");
    basepriceEnabled = json.get('basepriceEnabled');
    //   //print("======10");
    // maximumCustomerEnteredPrice = (int.parse(json.get('maximumCustomerEnteredPrice')?.toString())).round();
    //  //print("======11");
    // minimumCustomerEnteredPrice =  int.parse(json.get('minimumCustomerEnteredPrice')?.toString());
    customerEntersPrice = json.get('customerEntersPrice');
    //print("======6");
    productCost = int.parse(json.get('productCost')?.toString());
    oldPrice = int.parse(json.get('oldPrice')?.toString());
    price = int.parse(json.get('price')?.toString());
    callForPrice = json.get('callForPrice');
    availableForPreOrder = json.get('availableForPreOrder');
    disableWishlistButton = json.get('disableWishlistButton');
    disableBuyButton = json.get('disableBuyButton');
    notReturnable = json.get('notReturnable');
    allowAddingOnlyExistingAttributeCombinations =
        json.get('allowAddingOnlyExistingAttributeCombinations');
    orderMaximumQuantity = json.get('orderMaximumQuantity');
    orderMinimumQuantity = json.get('orderMinimumQuantity');
    allowBackInStockSubscriptions = json.get('allowBackInStockSubscriptions');
    backorderModeId = json.get('backorderModeId');
    notifyAdminForQuantityBelow = json.get('notifyAdminForQuantityBelow');
    lowStockActivityId = json.get('lowStockActivityId');
    minStockQuantity = json.get('minStockQuantity');
    displayStockQuantity = json.get('displayStockQuantity');
    displayStockAvailability = json.get('displayStockAvailability');
    stockQuantity = json.get('stockQuantity');
    warehouseId = json.get('warehouseId');
    useMultipleWarehouses = json.get('useMultipleWarehouses');
    productAvailabilityRangeId = json.get('productAvailabilityRangeId');
    manageInventoryMethodId = json.get('manageInventoryMethodId');
    isTelecommunicationsOrBroadcastingOrElectronicServices =
        json.get('isTelecommunicationsOrBroadcastingOrElectronicServices');
    taxCategoryId = json.get('taxCategoryId');
    isTaxExempt = json.get('isTaxExempt');
    deliveryDateId = json.get('deliveryDateId');
    additionalShippingCharge = json.get('additionalShippingCharge');
    shipSeparately = json.get('shipSeparately');
    isFreeShipping = json.get('isFreeShipping');
    isShipEnabled = json.get('isShipEnabled');
    rentalPricePeriodId = json.get('rentalPricePeriodId');
    rentalPriceLength = json.get('rentalPriceLength');
    isRental = json.get('isRental');
    recurringTotalCycles = json.get('recurringTotalCycles');
    recurringCyclePeriodId = json.get('recurringCyclePeriodId');
    recurringCyclePeriod = json.get('recurringCyclePeriod');
    recurringCycleLength = json.get('recurringCycleLength');
    isRecurring = json.get('isRecurring');
    hasUserAgreement = json.get('hasUserAgreement');
    sampleDownloadId = json.get('sampleDownloadId');
    hasSampleDownload = json.get('hasSampleDownload');
    downloadActivationTypeId = json.get('downloadActivationTypeId');
    maxNumberOfDownloads = json.get('maxNumberOfDownloads');
    unlimitedDownloads = json.get('unlimitedDownloads');
    downloadId = json.get('downloadId');
    isDownload = json.get('isDownload');
    automaticallyAddRequiredProducts =
        json.get('automaticallyAddRequiredProducts');
    requireOtherProducts = json.get('requireOtherProducts');
    requiredProductIds = json.get('requiredProductIds');
    giftCardTypeId = json.get('giftCardTypeId');
    giftCardType = json.get('giftCardType');
    isGiftCard = json.get('isGiftCard');
    limitedToStores = json.get('limitedToStores');
    subjectToAcl = json.get('subjectToAcl');
    notApprovedTotalReviews = json.get('notApprovedTotalReviews');
    approvedTotalReviews = json.get('approvedTotalReviews');
    notApprovedRatingSum = json.get('notApprovedRatingSum');
    approvedRatingSum = json.get('approvedRatingSum');
    allowCustomerReviews = json.get('allowCustomerReviews');
    showOnHomepage = json.get('showOnHomepage');
    vendorId = json.get('vendorId');
    productTemplateId = json.get('productTemplateId');
    fullDescription = json.get('fullDescription');
    name = json.get('name');
    visibleIndividually = json.get('visibleIndividually');
    parentGroupedProductId = json.get('parentGroupedProductId');
    productTypeId = json.get('productTypeId');
    productType = json.get('productType');
    seName = json.get('seName');
    picturesJson = json.get('picturesJson');
    categoryId = json.get('categoryId');
    category = json.get('category') != null
        ? CategoryDto.fromMap(json.get('category'))
        : null;
    pictures = json.get('pictures').cast<String>();
    picturesWithCdn = json.get('picturesWithCdn').cast<String>();
    if (pictures != null && pictures.length > 0) {
      picture = ApiRoutes.CdnPath + pictures.first;
    }
    if (picturesWithCdn != null && picturesWithCdn.length > 0) {
      pictureWithCdn = picturesWithCdn.first;
    }
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.productAttributes != null) {
      data['productAttributes'] = this.productAttributes.toJson();
    }
    data['id'] = this.id;
    data['updatedOnUtc'] = this.updatedOnUtc;
    data['createdOnUtc'] = this.createdOnUtc;
    data['deleted'] = this.deleted;
    data['published'] = this.published;
    data['displayOrder'] = this.displayOrder;
    data['height'] = this.height;
    data['width'] = this.width;
    data['length'] = this.length;
    data['weight'] = this.weight;
    data['hasDiscountsApplied'] = this.hasDiscountsApplied;
    data['hasTierPrices'] = this.hasTierPrices;
    data['markAsNew'] = this.markAsNew;
    data['basepriceBaseUnitId'] = this.basepriceBaseUnitId;
    data['basepriceBaseAmount'] = this.basepriceBaseAmount;
    data['basepriceUnitId'] = this.basepriceUnitId;
    data['basepriceAmount'] = this.basepriceAmount;
    data['basepriceEnabled'] = this.basepriceEnabled;
    data['maximumCustomerEnteredPrice'] = this.maximumCustomerEnteredPrice;
    data['minimumCustomerEnteredPrice'] = this.minimumCustomerEnteredPrice;
    data['customerEntersPrice'] = this.customerEntersPrice;
    data['productCost'] = this.productCost;
    data['oldPrice'] = this.oldPrice;
    data['price'] = this.price;
    data['callForPrice'] = this.callForPrice;
    data['availableForPreOrder'] = this.availableForPreOrder;
    data['disableWishlistButton'] = this.disableWishlistButton;
    data['disableBuyButton'] = this.disableBuyButton;
    data['notReturnable'] = this.notReturnable;
    data['allowAddingOnlyExistingAttributeCombinations'] =
        this.allowAddingOnlyExistingAttributeCombinations;
    data['orderMaximumQuantity'] = this.orderMaximumQuantity;
    data['orderMinimumQuantity'] = this.orderMinimumQuantity;
    data['allowBackInStockSubscriptions'] = this.allowBackInStockSubscriptions;
    data['backorderModeId'] = this.backorderModeId;
    data['notifyAdminForQuantityBelow'] = this.notifyAdminForQuantityBelow;
    data['lowStockActivityId'] = this.lowStockActivityId;
    data['minStockQuantity'] = this.minStockQuantity;
    data['displayStockQuantity'] = this.displayStockQuantity;
    data['displayStockAvailability'] = this.displayStockAvailability;
    data['stockQuantity'] = this.stockQuantity;
    data['warehouseId'] = this.warehouseId;
    data['useMultipleWarehouses'] = this.useMultipleWarehouses;
    data['productAvailabilityRangeId'] = this.productAvailabilityRangeId;
    data['manageInventoryMethodId'] = this.manageInventoryMethodId;
    data['isTelecommunicationsOrBroadcastingOrElectronicServices'] =
        this.isTelecommunicationsOrBroadcastingOrElectronicServices;
    data['taxCategoryId'] = this.taxCategoryId;
    data['isTaxExempt'] = this.isTaxExempt;
    data['deliveryDateId'] = this.deliveryDateId;
    data['additionalShippingCharge'] = this.additionalShippingCharge;
    data['shipSeparately'] = this.shipSeparately;
    data['isFreeShipping'] = this.isFreeShipping;
    data['isShipEnabled'] = this.isShipEnabled;
    data['rentalPricePeriodId'] = this.rentalPricePeriodId;
    data['rentalPriceLength'] = this.rentalPriceLength;
    data['isRental'] = this.isRental;
    data['recurringTotalCycles'] = this.recurringTotalCycles;
    data['recurringCyclePeriodId'] = this.recurringCyclePeriodId;
    data['recurringCyclePeriod'] = this.recurringCyclePeriod;
    data['recurringCycleLength'] = this.recurringCycleLength;
    data['isRecurring'] = this.isRecurring;
    data['hasUserAgreement'] = this.hasUserAgreement;
    data['sampleDownloadId'] = this.sampleDownloadId;
    data['hasSampleDownload'] = this.hasSampleDownload;
    data['downloadActivationTypeId'] = this.downloadActivationTypeId;
    data['maxNumberOfDownloads'] = this.maxNumberOfDownloads;
    data['unlimitedDownloads'] = this.unlimitedDownloads;
    data['downloadId'] = this.downloadId;
    data['isDownload'] = this.isDownload;
    data['automaticallyAddRequiredProducts'] =
        this.automaticallyAddRequiredProducts;
    data['requireOtherProducts'] = this.requireOtherProducts;
    data['requiredProductIds'] = this.requiredProductIds;
    data['giftCardTypeId'] = this.giftCardTypeId;
    data['giftCardType'] = this.giftCardType;
    data['isGiftCard'] = this.isGiftCard;
    data['limitedToStores'] = this.limitedToStores;
    data['subjectToAcl'] = this.subjectToAcl;
    data['notApprovedTotalReviews'] = this.notApprovedTotalReviews;
    data['approvedTotalReviews'] = this.approvedTotalReviews;
    data['notApprovedRatingSum'] = this.notApprovedRatingSum;
    data['approvedRatingSum'] = this.approvedRatingSum;
    data['allowCustomerReviews'] = this.allowCustomerReviews;
    data['showOnHomepage'] = this.showOnHomepage;
    data['vendorId'] = this.vendorId;
    data['productTemplateId'] = this.productTemplateId;
    data['fullDescription'] = this.fullDescription;
    data['name'] = this.name;
    data['visibleIndividually'] = this.visibleIndividually;
    data['parentGroupedProductId'] = this.parentGroupedProductId;
    data['productTypeId'] = this.productTypeId;
    data['productType'] = this.productType;
    data['seName'] = this.seName;
    data['picturesJson'] = this.picturesJson;
    data['categoryId'] = this.categoryId;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    data['pictures'] = this.pictures;
    data['picturesWithCdn'] = this.picturesWithCdn;
    return data;
  }
}

class ProductAttributes {
  List<ProductAttribute> productAttribute;

  ProductAttributes({this.productAttribute});

  ProductAttributes.fromJson(Map<String, dynamic> json) {
    if (json.get('productAttribute') != null) {
      productAttribute = List<ProductAttribute>();
      json.get('productAttribute').forEach((v) {
        productAttribute.add(ProductAttribute.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.productAttribute != null) {
      data['productAttribute'] =
          this.productAttribute.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductAttribute {
  int id;
  String controlId;
  String displayOrder;
  int attributeControlTypeId;
  int attributeControlType;
  bool isRequired;
  String textPrompt;
  String productAttributeId;
  int productId;
  String productAttributeName;
  AttributeValues attributeValues;

  ProductAttribute(
      {this.id,
      this.controlId,
      this.displayOrder,
      this.attributeControlTypeId,
      this.attributeControlType,
      this.isRequired,
      this.textPrompt,
      this.productAttributeId,
      this.productId,
      this.productAttributeName,
      this.attributeValues});

  ProductAttribute.fromJson(Map<String, dynamic> json) {
    id = json.get('id') == null ? 0 : json.get('id');
    controlId = json.get('controlId');
    displayOrder = json.get('displayOrder');
    attributeControlTypeId = json.get('attributeControlTypeId') == null
        ? 0
        : json.get('attributeControlTypeId');
    attributeControlType = json.get('attributeControlType');
    isRequired = json.get('isRequired');
    textPrompt = json.get('textPrompt');
    productAttributeId = json.get('productAttributeId');
    productId = json.get('productId') == null ? 0 : json.get('productId');
    productAttributeName = json.get('productAttributeName');
    attributeValues = json.get('attributeValues') != null
        ? AttributeValues.fromJson(json.get('attributeValues'))
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['controlId'] = this.controlId;
    data['displayOrder'] = this.displayOrder;
    data['attributeControlTypeId'] = this.attributeControlTypeId;
    data['attributeControlType'] = this.attributeControlType;
    data['isRequired'] = this.isRequired;
    data['textPrompt'] = this.textPrompt;
    data['productAttributeId'] = this.productAttributeId;
    data['productId'] = this.productId;
    data['productAttributeName'] = this.productAttributeName;
    if (this.attributeValues != null) {
      data['attributeValues'] = this.attributeValues.toJson();
    }
    return data;
  }
}

class AttributeValues {
  List<AttributeValue> attributeValue;

  AttributeValues({this.attributeValue});

  AttributeValues.fromJson(Map<String, dynamic> json) {
    attributeValue = List<AttributeValue>();
    if (json.get('attributeValue') != null) {
      json.get('attributeValue').forEach((v) {
        attributeValue.add(AttributeValue.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.attributeValue != null) {
      data['attributeValue'] =
          this.attributeValue.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AttributeValue {
  int id;
  int pictureId;
  String displayOrder;
  bool isPreSelected;
  int quantity;
  String customerEntersQty;
  String cost;
  String weightAdjustment;
  String priceAdjustmentUsePercentage;
  int priceAdjustment;
  String imageSquaresPictureId;
  String name;
  int associatedProductId;
  int attributeValueTypeId;
  int attributeValueType;
  int productAttributeMappingId;

  AttributeValue(
      {this.id,
      this.pictureId,
      this.displayOrder,
      this.isPreSelected,
      this.quantity,
      this.customerEntersQty,
      this.cost,
      this.weightAdjustment,
      this.priceAdjustmentUsePercentage,
      this.priceAdjustment,
      this.imageSquaresPictureId,
      this.name,
      this.associatedProductId,
      this.attributeValueTypeId,
      this.attributeValueType,
      this.productAttributeMappingId});

  AttributeValue.fromJson(Map<String, dynamic> json) {
    //print("---------parse attribute value 1---------");
    //print("AttributeValue --- MAP");
    id = json.get('id');
    pictureId = json.get('pictureId');
    displayOrder = json.get('displayOrder');
    isPreSelected = json.get('isPreSelected');
    quantity = json.get('quantity');
    customerEntersQty = json.get('customerEntersQty');
    cost = json.get('cost');
    //print("AttributeValue ------------------------------ 1");
    //print("---------parse attribute value 2---------");
    weightAdjustment = json.get('weightAdjustment');
    //print("---------parse attribute value---------");
    //print("AttributeValue ------------------------------ 2");
    priceAdjustmentUsePercentage = json.get('priceAdjustmentUsePercentage');
    //print("---------parse attribute value 3---------");
    //print("AttributeValue ------------------------------ 3");
    priceAdjustment =
        json.get('priceAdjustment') == null ? 0 : json.get('priceAdjustment');
    //print("---------parse attribute value 4---------");
    imageSquaresPictureId = json.get('imageSquaresPictureId');
    name = json.get('name');
    //print("---------parse attribute value 4---------");
    associatedProductId = json.get('associatedProductId');
    attributeValueTypeId = json.get('attributeValueTypeId');
    attributeValueType = json.get('attributeValueType');
    productAttributeMappingId = json.get('productAttributeMappingId');

    //print("AttributeValue ------------------------------ 4");
    //print("---------parse attribute value---------");
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pictureId'] = this.pictureId;
    data['displayOrder'] = this.displayOrder;
    data['isPreSelected'] = this.isPreSelected;
    data['quantity'] = this.quantity;
    data['customerEntersQty'] = this.customerEntersQty;
    data['cost'] = this.cost;
    data['weightAdjustment'] = this.weightAdjustment;
    data['priceAdjustmentUsePercentage'] = this.priceAdjustmentUsePercentage;
    data['priceAdjustment'] = this.priceAdjustment;
    data['imageSquaresPictureId'] = this.imageSquaresPictureId;
    data['name'] = this.name;
    data['associatedProductId'] = this.associatedProductId;
    data['attributeValueTypeId'] = this.attributeValueTypeId;
    data['attributeValueType'] = this.attributeValueType;
    data['productAttributeMappingId'] = this.productAttributeMappingId;
    return data;
  }
}

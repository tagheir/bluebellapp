import 'dart:convert';

import 'package:bluebellapp/models/product_dto.dart';

import 'category_dto.dart';

class HomePageViewModel {
  CategoryDto facilitiesManagementServices;
  CategoryDto landscapeServices;
  CategoryDto products;
  List<ProductDto> packages;

  HomePageViewModel({
    this.facilitiesManagementServices,
    this.landscapeServices,
    this.products,
    this.packages,
  });

  factory HomePageViewModel.fromJson(String str) {
    // print("=========================jsonDecode==========================");
    //print(json.decode(str));
    if (str != null) {
      return HomePageViewModel.fromMap(json.decode(str));
    }
    return null;
  }

  factory HomePageViewModel.fromMap(Map<String, dynamic> json) {
    var dto = HomePageViewModel(
        facilitiesManagementServices:
            json['facilitiesManagementServices'] != null
                ? CategoryDto.fromMap(json['facilitiesManagementServices'])
                : CategoryDto(),
        landscapeServices: json['landscapeServices'] != null
            ? CategoryDto.fromMap(json['landscapeServices'])
            : CategoryDto(),
        products: json['products'] != null
            ? CategoryDto.fromMap(json['products'])
            : CategoryDto(),
        packages: json["packages"] != null
            ? (json["packages"] as List)
                .map((data) => ProductDto.fromMap(data))
                .toList()
            : List<ProductDto>());
    return dto;
  }
}

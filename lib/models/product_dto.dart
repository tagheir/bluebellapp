import 'dart:convert';

import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/template_type.dart';

class ProductDto {
  String templatePath;
  List<String> pictures;
  String pictureThumb;
  String url;
  int id;
  String name;
  String templateName;
  String picturesJson;
  int price;
  int displayOrder;
  int productTypeId;
  int productType;
  int categoryId;
  bool showOnHomepage;
  bool published;
  bool deleted;
  String seName;
  String fullDescription;
  TemplateType templateType;

  ProductDto(
      {this.templatePath,
      this.pictures,
      this.url,
      this.id,
      this.name,
      this.templateName,
      this.picturesJson,
      this.price,
      this.displayOrder,
      this.productTypeId,
      this.productType,
      this.categoryId,
      this.showOnHomepage,
      this.published,
      this.deleted,
      this.seName,
      this.fullDescription});

  factory ProductDto.fromJson(String str) =>
      ProductDto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  static List<ProductDto> fromJsonList(String str) {
    return (json.decode(str) as List)
        .map((f) => ProductDto.fromMap(f))
        .toList();
  }

  ProductDto.fromMap(Map<String, dynamic> json) {
    templatePath = json['templatePath'];
    pictures = json['pictures'].cast<String>();
    url = json['url'];
    id = json['id'];
    name = json['name'];
    templateName = json['templateName'];
    picturesJson = json['picturesJson'];
    price = json['price'];
    displayOrder = json['displayOrder'];
    productTypeId = json['productTypeId'];
    productType = json['productType'];
    categoryId = json['categoryId'];
    showOnHomepage = json['showOnHomepage'];
    published = json['published'];
    deleted = json['deleted'];
    seName = json['seName'];
    fullDescription = json['fullDescription'];
    templateType = TemplateTypeHelper.getTemplateTypeOfProduct(templateName);
    if(pictures != null){
      if(pictures.length > 0)
      {
        pictureThumb = ApiRoutes.CdnPath +  pictures.first;
      }
      
    }
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templatePath'] = this.templatePath;
    data['pictures'] = this.pictures;
    data['url'] = this.url;
    data['id'] = this.id;
    data['name'] = this.name;
    data['templateName'] = this.templateName;
    data['picturesJson'] = this.picturesJson;
    data['price'] = this.price;
    data['displayOrder'] = this.displayOrder;
    data['productTypeId'] = this.productTypeId;
    data['productType'] = this.productType;
    data['categoryId'] = this.categoryId;
    data['showOnHomepage'] = this.showOnHomepage;
    data['published'] = this.published;
    data['deleted'] = this.deleted;
    data['seName'] = this.seName;
    data['fullDescription'] = this.fullDescription;
    return data;
  }
}

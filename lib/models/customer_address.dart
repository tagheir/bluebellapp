// To parse this JSON data, do
//
//     final customerAddress = customerAddressFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class CustomerAddress {
  Address address;

  CustomerAddress({
    @required this.address,
  });

  factory CustomerAddress.fromJson(String str) =>
      CustomerAddress.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CustomerAddress.fromMap(Map<String, dynamic> json) => CustomerAddress(
        address:
            json["address"] == null ? null : Address.fromMap(json["address"]),
      );

  Map<String, dynamic> toMap() => {
        "address": address == null ? null : address.toMap(),
      };
}

class Address {
  int id;
  String firstName;
  String lastName;
  String email;
  String company;
  int countryId;
  int stateProvinceId;
  String city;
  String address1;
  String address2;
  String zipPostalCode;
  String phoneNumber;
  String faxNumber;
  DateTime createdOnUtc;
  int customerId;
  String countryName;
  String countryCode;
  String stateProvinceName;

  Address({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.company,
    this.countryId,
    this.stateProvinceId,
    this.city,
    this.address1,
    this.address2,
    this.zipPostalCode,
    this.phoneNumber,
    this.faxNumber,
    this.createdOnUtc,
    this.customerId,
    this.countryName,
    this.countryCode,
    this.stateProvinceName,
  });

  factory Address.fromJson(String str) => Address.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Address.fromMap(Map<String, dynamic> json) => Address(
        id: json["id"] == null ? null : json["id"],
        firstName: json["firstName"] == null ? null : json["firstName"],
        lastName: json["lastName"] == null ? null : json["lastName"],
        email: json["email"] == null ? null : json["email"],
        company: json["company"] == null ? null : json["company"],
        countryId: json["countryId"] == null ? null : json["countryId"],
        stateProvinceId:
            json["stateProvinceId"] == null ? null : json["stateProvinceId"],
        city: json["city"] == null ? null : json["city"],
        address1: json["address1"] == null ? null : json["address1"],
        address2: json["address2"] == null ? null : json["address2"],
        zipPostalCode:
            json["zipPostalCode"] == null ? null : json["zipPostalCode"],
        phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
        faxNumber: json["faxNumber"] == null ? null : json["faxNumber"],
        createdOnUtc: json["createdOnUtc"] == null
            ? null
            : DateTime.parse(json["createdOnUtc"]),
        customerId: json["customerId"] == null ? null : json["customerId"],
        countryName: json["countryName"] == null ? null : json["countryName"],
        countryCode: json["countryCode"] == null ? null : json["countryCode"],
        stateProvinceName: json["stateProvinceName"] == null
            ? null
            : json["stateProvinceName"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "firstName": firstName == null ? null : firstName,
        "lastName": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "company": company == null ? null : company,
        "countryId": countryId == null ? null : countryId,
        "stateProvinceId": stateProvinceId == null ? null : stateProvinceId,
        "city": city == null ? null : city,
        "address1": address1 == null ? null : address1,
        "address2": address2 == null ? null : address2,
        "zipPostalCode": zipPostalCode == null ? null : zipPostalCode,
        "phoneNumber": phoneNumber == null ? null : phoneNumber,
        "faxNumber": faxNumber == null ? null : faxNumber,
        "createdOnUtc":
            createdOnUtc == null ? null : createdOnUtc.toIso8601String(),
        "customerId": customerId == null ? null : customerId,
        "countryName": countryName == null ? null : countryName,
        "countryCode": countryCode == null ? null : countryCode,
        "stateProvinceName":
            stateProvinceName == null ? null : stateProvinceName,
      };
}

import 'dart:convert';

class CustomerDto {
  int customerId;
  String name;
  String firstName;
  String lastName;
  String imageUrl;
  String imageBase64;
  String phoneNumber;
  String email;
  int billingAddressId;
  int shippingAddressId;

  CustomerDto(
      {this.customerId,
      this.firstName,
      this.lastName,
      this.imageUrl,
      this.imageBase64,
      this.phoneNumber,
      this.email,
      this.billingAddressId,
      this.shippingAddressId});

  String toJson() => json.encode(toMap());

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['customerId'] = this.customerId;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['imageUrl'] = this.imageUrl;
    data['imageBase64'] = this.imageBase64;
    data['phoneNumber'] = this.phoneNumber;
    data['billingAddressId'] = this.billingAddressId;
    data['shippingAddressId'] = this.shippingAddressId;
    
    return data;
  }

  //factory CustomerDto.fromJson(String str) {
  //  return CustomerDto.fromMap(json.decode(str));
}

// String toJson() => json.encode(toMap());

import 'dart:convert';
import 'package:bluebellapp/models/parent_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/template_type.dart';

class CategoryDto {
  int id;
  List<CategoryDto> subCategories;
  String name;
  String seName;
  String templateName;
  String templatePath;
  String pictureSeoFilename;
  String pictureMimeType;
  String picturePath;
  bool showOnHomepage;
  bool published;
  int pictureId;
  int displayOrder;
  String pictureThumbPath;
  List<ParentDto> parentDto;
  String url;
  List<ProductDto> products;
  TemplateType templateType;

  CategoryDto(
      {this.id,
      this.subCategories,
      this.name,
      this.seName,
      this.templateName,
      this.templatePath,
      this.pictureSeoFilename,
      this.pictureMimeType,
      this.picturePath,
      this.showOnHomepage,
      this.published,
      this.pictureId,
      this.displayOrder,
      this.pictureThumbPath,
      this.parentDto,
      this.url,
      this.products});

  factory CategoryDto.fromJson(String str) {
    //print(("============Category from json ========================="));
    //print(str.toString());
    return CategoryDto.fromMap(json.decode(str));
  }

  String toJson() => json.encode(toMap());

  CategoryDto.fromMap(Map<String, dynamic> json) {
    //print("====================from map called===================");
    id = json['id'] == null ? 0 : json['id'];
    //subCategories = json['subCategories'];
    if (json['subCategories'] != null) {
      //print("=============sub categories not null================");
      subCategories = new List<CategoryDto>();
      json['subCategories'].forEach((v) {
        subCategories.add(new CategoryDto.fromMap(v));
      });
    } else {
      //print("=============sub categories are null================");
      subCategories = new List<CategoryDto>();
    }
    name = json['name'];
    seName = json['seName'];
    templateName = json['templateName'];
    templatePath = json['templatePath'];
    pictureSeoFilename = json['pictureSeoFilename'];
    pictureMimeType = json['pictureMimeType'];
    picturePath = json['picturePath'];
    showOnHomepage = json['showOnHomepage'];
    published = json['published'];
    pictureId = json['pictureId'] == null ? 0 : json['pictureId'];
    displayOrder = json['displayOrder'] == null ? 0 : json['displayOrder'];
    pictureThumbPath = json['pictureThumbPath'];
    if (json['ParentDto'] != null) {
      parentDto = new List<ParentDto>();
      json['ParentDto'].forEach((v) {
        parentDto.add(new ParentDto.fromJson(v));
      });
    }
    url = json['url'];
    if (json['products'] != null) {
      products = new List<ProductDto>();
      json['products'].forEach((v) {
        products.add(new ProductDto.fromMap(v));
      });
    }
    templateType = TemplateTypeHelper.getTemplateTypeOfCategory(templateName);
    if (pictureThumbPath != null && pictureThumbPath.isNotEmpty) {
      pictureThumbPath = ApiRoutes.CdnPath + pictureThumbPath;
    }
    if (picturePath != null && picturePath.isNotEmpty) {
      picturePath = ApiRoutes.CdnPath + picturePath;
    }
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.subCategories != null) {
      data['subCategories'] =
          this.subCategories.map((v) => v.toJson()).toList();
    }
    data['name'] = this.name;
    data['seName'] = this.seName;
    data['templateName'] = this.templateName;
    data['templatePath'] = this.templatePath;
    data['pictureSeoFilename'] = this.pictureSeoFilename;
    data['pictureMimeType'] = this.pictureMimeType;
    data['picturePath'] = this.picturePath;
    data['showOnHomepage'] = this.showOnHomepage;
    data['published'] = this.published;
    data['pictureId'] = this.pictureId;
    data['displayOrder'] = this.displayOrder;
    data['pictureThumbPath'] = this.pictureThumbPath;
    if (this.parentDto != null) {
      data['ParentDto'] = this.parentDto.map((v) => v.toJson()).toList();
    }
    data['url'] = this.url;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

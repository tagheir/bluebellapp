import 'dart:convert';
import 'package:bluebellapp/models/attribute_dto.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';

class ShoppingCartDto {
  List<ShoppingCartItem> shoppingCartItems;
  int totalQuantity;
  double totalCost;

  ShoppingCartDto({this.shoppingCartItems, this.totalQuantity, this.totalCost});

  factory ShoppingCartDto.fromJson(String str) =>
      ShoppingCartDto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  ShoppingCartDto.fromMap(Map<String, dynamic> json) {
    if (json['shoppingCartItems'] != null) {
      shoppingCartItems = new List<ShoppingCartItem>();
      json['shoppingCartItems'].forEach((v) {
        shoppingCartItems.add(ShoppingCartItem.fromJson(v));
      });
    }
    totalQuantity = json['totalQuantity'];
    totalCost = json['totalCost'] == 0 ? 0.0 : json['totalCost'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    print("======before shppoing cart======");
    if (this.shoppingCartItems != null) {
      data['shoppingCartItems'] =
          this.shoppingCartItems.map((v) => v.toJson()).toList();
    }
    print("======after shppoing cart======");
    data['totalQuantity'] = this.totalQuantity;
    data['totalCost'] = this.totalCost;
    return data;
  }
}

class ShoppingCartItem {
  int id;
  int storeId;
  int shoppingCartTypeId;
  int customerId;
  int productId;
  ProductDto product;
  ProductAttributes productAttributes;
  String attributesXml;
  List<AttributeValue> attributes;
  Attributes cartItemAttributes;
  int customerEnteredPrice;
  int quantity;
  double pricePerUnit;
  double totalPrice;
  String rentalStartDateUtc;
  String rentalEndDateUtc;
  String createdOnUtc;
  String updatedOnUtc;
  int shoppingCartType;

  ShoppingCartItem(
      {this.id,
      this.storeId,
      this.shoppingCartTypeId,
      this.customerId,
      this.productId,
      this.product,
      this.attributesXml,
      this.attributes,
      this.productAttributes,
      this.customerEnteredPrice,
      this.quantity,
      this.pricePerUnit,
      this.totalPrice,
      this.rentalStartDateUtc,
      this.rentalEndDateUtc,
      this.createdOnUtc,
      this.updatedOnUtc,
      this.cartItemAttributes,
      this.shoppingCartType});

  factory ShoppingCartItem.fromMap(String str) =>
      ShoppingCartItem.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  ShoppingCartItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['storeId'];
    shoppingCartTypeId = json['shoppingCartTypeId'];
    customerId = json['customerId'];
    productId = json['productId'];
    product =
        json['product'] != null ? ProductDto.fromMap(json['product']) : null;
    productAttributes = json['productAttributes'] != null
        ? new ProductAttributes.fromJson(json['productAttributes'])
        : null;
    attributesXml = json['attributesXml'];
    if (json['attributes'] != null) {
      attributes = List<AttributeValue>();
      json['attributes'].forEach((v) {
        attributes.add(AttributeValue.fromJson(v));
      });
    }
    cartItemAttributes = json['cartItemAttributes'] != null
        ? Attributes.fromJson(json['cartItemAttributes'])
        : null;
    // print("====cart item attributes parsed==========");
    // customerEnteredPrice = json['customerEnteredPrice'];
    quantity = json['quantity'];
    pricePerUnit = json['pricePerUnit'];
    totalPrice = json['totalPrice'] as double;
    rentalStartDateUtc = json['rentalStartDateUtc'];
    rentalEndDateUtc = json['rentalEndDateUtc'];
    createdOnUtc = json['createdOnUtc'];
    updatedOnUtc = json['updatedOnUtc'];
    shoppingCartType = json['shoppingCartType'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['storeId'] = this.storeId;
    data['shoppingCartTypeId'] = this.shoppingCartTypeId;
    data['customerId'] = this.customerId;
    data['productId'] = this.productId;
    if (this.product != null) {
      data['product'] = this.product.toJson();
    }
    data['attributesXml'] = this.attributesXml;
    if (this.attributes != null) {
      data['attributes'] = this.attributes.map((v) => v.toJson()).toList();
    }
    if (this.cartItemAttributes != null) {
      data['cartItemAttributes'] = this.cartItemAttributes.toJson();
    }
    data['customerEnteredPrice'] = this.customerEnteredPrice;
    data['quantity'] = this.quantity;
    data['pricePerUnit'] = this.pricePerUnit;
    data['totalPrice'] = this.totalPrice;
    data['rentalStartDateUtc'] = this.rentalStartDateUtc;
    data['rentalEndDateUtc'] = this.rentalEndDateUtc;
    data['createdOnUtc'] = this.createdOnUtc;
    data['updatedOnUtc'] = this.updatedOnUtc;
    data['shoppingCartType'] = this.shoppingCartType;
    return data;
  }
}

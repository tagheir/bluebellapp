class OrderItem {
  int id;
  String orderItemGuid;
  int orderId;
  int productId;
  int quantity;
  int unitPriceInclTax;
  int unitPriceExclTax;
  int priceInclTax;
  int priceExclTax;
  int discountAmountInclTax;
  int discountAmountExclTax;
  int originalProductCost;
  String attributeDescription;
  String attributesXml;
  int downloadCount;
  bool isDownloadActivated;
  String licenseDownloadId;
  String productName;

  OrderItem(
      {this.id,
      this.orderItemGuid,
      this.orderId,
      this.productId,
      this.quantity,
      this.unitPriceInclTax,
      this.unitPriceExclTax,
      this.priceInclTax,
      this.priceExclTax,
      this.discountAmountInclTax,
      this.discountAmountExclTax,
      this.originalProductCost,
      this.attributeDescription,
      this.attributesXml,
      this.downloadCount,
      this.isDownloadActivated,
      this.licenseDownloadId,
      this.productName});
}

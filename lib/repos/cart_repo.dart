
import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/addToCart_dto.dart';
import 'package:bluebellapp/models/category_name.dart';
import 'package:bluebellapp/models/order_dto.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/response_model.dart';
import 'package:bluebellapp/models/shoppingCart_dto.dart';
import 'package:bluebellapp/models/create_order_response.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/attribute_control_type.dart';
import 'package:bluebellapp/resources/constants/helper_constants/network_request_type.dart';
import 'package:bluebellapp/services/authorize_network_service.dart';

class CartRepository {
  final AppBloc appBloc;
  AuthorizeNetworkService authorizeNetworkService;
  CartRepository({this.appBloc}) {
    this.authorizeNetworkService = appBloc.getRepo().getNetworkService();
  }
  AuthorizeNetworkService getAuthorizeNetworkService() {
    if (authorizeNetworkService == null) {
      authorizeNetworkService = appBloc.getRepo().getNetworkService();
    }
    return authorizeNetworkService;
  }

  ShoppingCartDto myCart = ShoppingCartDto();
  List<OrderDto> myOrders = List<OrderDto>();
  List<ProductDetailDto> cartItems = List<ProductDetailDto>();
  int myCartCount = 0;
  int myOrdersCount = 0;

  //===============================Cart ===========================================

  Future<ShoppingCartDto> addToCart(
      {AddToCartDto cart, Function callBack}) async {
    var url = Cart.CartBase + '/' + cart.productId.toString() + '/1/1';
    var model = AttrMap.encodeList(attr: cart.form);
    print(AttrMap.encodeList(attr: cart.form));
    var response = await getAuthorizeNetworkService().process<ShoppingCartDto>(
      model: model,
      endPoint: url,
      networkRequestType: NetworkRequestType.POST_AUTHORIZED_JSON,
      parser: (data) => ShoppingCartDto.fromJson(data),
    );
    return shoppingCartDtoResponseHelper(response, callBack);
  }

  Future<ShoppingCartDto> updateCart(
      {AddToCartDto cart, Function callBack}) async {
    var url = Cart.CartBase +
        '/updateCart/' +
        cart.cartItemId.toString() +
        '/1/1/' +
        cart.productId.toString();
    var model = AttrMap.encodeList(attr: cart.form);
    var response = await getAuthorizeNetworkService().process<ShoppingCartDto>(
      model: model,
      endPoint: url,
      networkRequestType: NetworkRequestType.POST_AUTHORIZED_JSON,
      parser: (data) => ShoppingCartDto.fromJson(data),
    );
    return shoppingCartDtoResponseHelper(response, callBack);
  }

  Future<ShoppingCartDto> removeItemFromCart(
      {Function callBack, int cartItemId}) async {
    var url = Cart.CartBase + "/deleteItem/" + cartItemId.toString();

    var response = await getAuthorizeNetworkService().process<ShoppingCartDto>(
      endPoint: url,
      networkRequestType: NetworkRequestType.GET_AUTHORIZED_JSON,
      parser: (data) => ShoppingCartDto.fromJson(data),
    );
    return shoppingCartDtoResponseHelper(response, callBack);
  }

  Future<ShoppingCartDto> getMyCart({Function callBack}) async {
    if (myCart?.shoppingCartItems != null &&
        myCart.shoppingCartItems.length > 0) {
      return myCart;
    }
    var url = Cart.CartBase;
    var response = await getAuthorizeNetworkService().process<ShoppingCartDto>(
      endPoint: url,
      networkRequestType: NetworkRequestType.GET_AUTHORIZED_JSON,
      parser: (data) => ShoppingCartDto.fromJson(data),
    );
    return shoppingCartDtoResponseHelper(response, callBack);
  }

  Future<ShoppingCartDto> shoppingCartDtoResponseHelper(
      ResponseModel<ShoppingCartDto> response, Function callBack) {
    if (ResponseModel.processFailure(response, callBack) == false) {
      myCart = response.data;
      myCartCount = response?.data?.totalQuantity ?? 0;
    }
    return Future.value(response?.data);
  }

  Future<int> getCartItemsCount() async {
    if (myCartCount <= 0) {
      await getMyCart();
    }
    return myCartCount;
  }

  Future<AttributeValue> getDropDownAttributeValue({int productId}) async {
    var cartItems = await getMyCart();
    var cartItem = cartItems.shoppingCartItems
        .firstWhere((s) => s.productId == productId, orElse: () => null);
    var dropDownAttr = cartItem.productAttributes.productAttribute?.firstWhere(
        (p) => p?.productAttributeName == AttributeType.DropdownList,
        orElse: () => null);
    if (dropDownAttr != null) {
      var attrValue = dropDownAttr?.attributeValues?.attributeValue[0];
      return Future.value(attrValue);
    } else {
      return AttributeValue();
    }
  }

  Future<String> getDateAttributeValue({int productId}) async {
    var cartItems = await getMyCart();
    var cartItem = cartItems.shoppingCartItems
        .firstWhere((s) => s.productId == productId, orElse: () => null);
    var dropDownAttr = cartItem.productAttributes.productAttribute?.firstWhere(
        (p) => p?.productAttributeName == AttributeType.ServiceDate,
        orElse: () => null);
    if (dropDownAttr != null) {
      var attrValue = dropDownAttr?.attributeValues?.attributeValue[0].cost;
      return Future.value(attrValue);
    } else {
      return null;
    }
  }

  //=============================Order===============================

  Future<CreateOrderResponseDto> placeDirectOrder({AddToCartDto cart, Function callBack , int billingAddressId}) async {
    var url = "${Order.CreateDirectOrder}/${cart.productId.toString()}?billingAddressId=${billingAddressId.toString()}";
       
    var model = AttrMap.encodeList(attr: cart.form);
    print(model.toString());
    var response = await getAuthorizeNetworkService().process<CreateOrderResponseDto>(
      model: model,
      endPoint: url,
      networkRequestType: NetworkRequestType.POST_AUTHORIZED_JSON,
      parser: (data) => CreateOrderResponseDto.fromJson(data)
    );
    print(response?.data);
    ResponseModel.processFailure(response, callBack);
    print(response?.data.toString());
    return Future.value(response?.data ?? 0);
  }

  Future<int> placeOrder({Function callBack, int billingAddressId}) async {
    var url = Order.CreateOrderByShoppingCart +
        '?billingAddressId=' +
        billingAddressId.toString();

    var response = await getAuthorizeNetworkService().process<int>(
      endPoint: url,
      networkRequestType: NetworkRequestType.POST_AUTHORIZED_JSON,
      parser: (data) => (int.tryParse(data.toString()) ?? 0),
    );

    ResponseModel.processFailure(response, callBack);
    return Future.value(response?.data ?? 0);
  }

  Future<List<OrderDto>> getMyOrders(
      {Function callBack, bool forceNetwork = false}) async {
    var url = Order.GetCustomerOrder;
    var response = await getAuthorizeNetworkService().process<List<OrderDto>>(
      endPoint: url,
      networkRequestType: NetworkRequestType.GET_AUTHORIZED_JSON,
      parser: (data) => OrderDto.parseJsonList(data),
    );
    if (ResponseModel.processFailure(response, callBack) == false) {
      myOrders = response.data;
      myOrdersCount = myOrders?.length ?? 0;
      if (myOrders != null && myOrders.length > 0) {
        for (var order in myOrders) {
          var orderItems = order?.orderItems?.orderItem;
          if (orderItems != null && orderItems.length > 0) {
            for (var orderItem in order.orderItems.orderItem) {
              var serviceRepo = appBloc.getRepo().getServiceRepository();
              var product = await serviceRepo.getProductById(
                  productId: orderItem.id,
                  catName: CategoryName.FacilityManagement);
              if (product != null) {
                orderItem.productDto = product;
                break;
              }
              product = await serviceRepo.getProductById(
                  productId: orderItem.id,
                  catName: CategoryName.LandscapeServices);
              if (product != null) {
                orderItem.productDto = product;
                break;
              }
              product = await serviceRepo.getProductById(
                  productId: orderItem.id, catName: CategoryName.Products);
              if (product != null) {
                orderItem.productDto = product;
                break;
              }
              product = await serviceRepo.getProductById(
                  productId: orderItem.id, catName: CategoryName.Packages);
              if (product != null) {
                orderItem.productDto = product;
                break;
              }
            }
          }
        }
      }
    }
    return myOrders;
  }

  Future<OrderDto> getOrderDetail(
      {Function callBack, int orderId, bool forceNetwork = false}) async {
        print("here");
    if (myOrders == null || myOrders.length <= 0) {
      await getMyOrders(callBack: callBack, forceNetwork: forceNetwork);
    }
    var order = myOrders.firstWhere((o) {
      if(o == null) print("NULL ORDER");
      return o != null && o?.id == orderId;
    }, orElse: () => null);
    if (order != null || forceNetwork == true) return order;
    return getOrderDetail(
      callBack: callBack,
      forceNetwork: true,
      orderId: orderId,
    );
  }

  Future<String> getOrderItemDropDownValue({OrderItem orderItem}) async {
    var dropAttr = orderItem?.attributes?.productAttribute?.firstWhere(
        (p) =>
            p?.productAttributeValue?.attributeType ==
            AttributeType.DropdownList,
        orElse: () => null);
    var attrVal = dropAttr?.productAttributeValue?.attributeValue;
    if (attrVal != null) {
      return Future.value(attrVal);
    }
    return null;
  }

  Future<String> getOrderItemDateValue({OrderItem orderItem}) async {
    var dropAttr = orderItem?.attributes?.productAttribute?.firstWhere(
        (p) =>
            p?.productAttributeValue?.attributeType ==
            AttributeType.ServiceDate,
        orElse: () => null);
    var attrVal = dropAttr?.productAttributeValue?.value;
    if (attrVal != null) {
      return Future.value(attrVal);
    }
    return null;
  }

  Future<int> getOrdersCount() async {
    if (myOrdersCount <= 0) {
      await getMyOrders();
    }
    return myOrdersCount;
  }
}

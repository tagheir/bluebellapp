import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/repos/cart_repo.dart';
import 'package:bluebellapp/repos/services_repo.dart';
import 'package:bluebellapp/repos/user_repositry.dart';
import 'package:bluebellapp/services/authorize_network_service.dart';

class AppRepo {
  final AppBloc appBloc;
  AuthorizeNetworkService authorizeNetworkService;
  AppRepo(this.appBloc) {
    authorizeNetworkService = AuthorizeNetworkService(this);
  }

  UserRepository userRepository;
  ServiceRepository serviceRepository;
  CartRepository cartRepository;
  String token;

  String getToken() => token;

  AuthorizeNetworkService getNetworkService() {
    if (authorizeNetworkService == null) {
      authorizeNetworkService = AuthorizeNetworkService(this);
    }
    return this.authorizeNetworkService;
  }

  UserRepository getUserRepository() {
    if (userRepository == null) {
      return userRepository = UserRepository(appBloc: appBloc);
    }
    return this.userRepository;
  }

  CartRepository getCartRepository() {
    if (cartRepository == null) {
      return cartRepository = CartRepository(appBloc: appBloc);
    } else {
      return this.cartRepository;
    }
  }

  ServiceRepository getServiceRepository() {
    if (serviceRepository == null) {
      return serviceRepository = ServiceRepository(appBloc: appBloc);
    } else {
      return this.serviceRepository;
    }
  }
}

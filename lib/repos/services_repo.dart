import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/category_dto.dart';
import 'package:bluebellapp/models/category_name.dart';
import 'package:bluebellapp/models/homeViewModel.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/models/response_model.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/network_request_type.dart';
import 'package:bluebellapp/services/authorize_network_service.dart';

class ServiceRepository {
  final AppBloc appBloc;
  AuthorizeNetworkService authorizeNetworkService;
  ServiceRepository({this.appBloc}) {
    authorizeNetworkService = appBloc.getRepo().getNetworkService();
  }
  AuthorizeNetworkService getAuthorizeNetworkService() {
    if (authorizeNetworkService == null) {
      authorizeNetworkService = appBloc.getRepo().getNetworkService();
    }
    return authorizeNetworkService;
  }

  HomePageViewModel homePageViewModel;
  List<ProductDetailDto> products = List<ProductDetailDto>();

  Future<HomePageViewModel> getHomeData({
    Function(String) callback,
    bool forceNetwork = false,
  }) async {
    if (homePageViewModel != null && forceNetwork == false) {
      return Future.value(homePageViewModel);
    }
    print("Error is here");
    var response =
        await getAuthorizeNetworkService().process<HomePageViewModel>(
      endPoint: Category.GetHomePageView,
      networkRequestType: NetworkRequestType.GET_JSON,
      parser: (data) => HomePageViewModel.fromJson(data),
    );

    print("Error is here");
    ResponseModel.processFailure(response, callback);
    if (homePageViewModel == null || response?.data != null) {
      homePageViewModel = response.data;
    }
    print("Error is here");
    return response?.data;
  }

  Future<List<ProductDto>> searchProducts(String query) async {
    if (query.isEmpty) {
      return Future.value(List<ProductDto>());
    }
    var url = Product.Search + "?q=" + query;
    var response = await getAuthorizeNetworkService().process<List<ProductDto>>(
      endPoint: url,
      networkRequestType: NetworkRequestType.GET_JSON,
      parser: (data) => ProductDto.fromJsonList(data),
    );
    if (response?.data != null && response.success == true) {
      return response.data;
    }
    return List<ProductDto>();
  }

  Future<ProductDto> getProductById({int productId, CategoryName catName = CategoryName.FacilityManagement}) {
    switch (catName) {
      case CategoryName.FacilityManagement:
        var product = getProductFromSubCategories(
            homePageViewModel?.facilitiesManagementServices?.subCategories,
            productId);
        return product == null ? getProductById(productId: productId, catName: CategoryName.LandscapeServices): Future.value(product);
      case CategoryName.LandscapeServices:
        var product = getProductFromSubCategories(
            homePageViewModel?.landscapeServices?.subCategories, productId);
        return product == null ? getProductById(productId: productId, catName: CategoryName.Packages) :Future.value(product);
      case CategoryName.Packages:
        var product =
            getProductFromProducts(homePageViewModel?.packages, productId);
        return product == null ? getProductById(productId: productId, catName: CategoryName.Products) : Future.value(product);
      case CategoryName.Products:
        var product = getProductFromSubCategories(
            homePageViewModel?.facilitiesManagementServices?.subCategories,
            productId);
        return Future.value(product);
    }
    return Future.value(ProductDto());
  }

  ProductDto getProductFromSubCategories(
      List<CategoryDto> subCategories, int productId) {
    if (subCategories == null) return null;
    if (subCategories.length <= 0) return null;
    var productsLists = subCategories.map((c) => c.products);
    print(productsLists.length);
    var nonNullProductsLists =
        productsLists.where((pl) => pl != null && pl.length > 0);
    print(productsLists.length);
    var listsExpansion = nonNullProductsLists.expand((pl) => pl);
    print(listsExpansion);

    var product = listsExpansion
        .firstWhere((p) => p != null && p.id == productId, orElse: () => null);

    return product;
  }

  ProductDto getProductFromProducts(List<ProductDto> products, int productId) {
    if (products == null || products.length <= 0) return null;
    return products.firstWhere((p) => p != null && p.id == productId,
        orElse: () => null);
  }

  Future<ResponseModel<ProductDetailDto>> getProductDetail(
      {Function(String) callback, int productId}) async {
    var url = Product.BaseIdentity;
    url = url + '/' + productId.toString();
    var response = await getAuthorizeNetworkService().process<ProductDetailDto>(
      endPoint: url,
      networkRequestType: NetworkRequestType.GET_JSON,
      parser: (data) => ProductDetailDto.fromJson(data),
    );
    //ResponseModel.processFailure(response, callback);
    return response;
  }
}

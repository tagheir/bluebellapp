import 'dart:convert';

import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/address_enum.dart';
import 'package:bluebellapp/models/authResponse_dto.dart';
import 'package:bluebellapp/models/customerAddress_dto.dart';
import 'package:bluebellapp/models/customer_dto.dart';
import 'package:bluebellapp/models/cutomerDetail_dto.dart';
import 'package:bluebellapp/models/response_model.dart';
import 'package:bluebellapp/models/signup_dto.dart';
import 'package:bluebellapp/models/twoFactorAuth_dto.dart';
import 'package:bluebellapp/resources/constants/api_routes.dart';
import 'package:bluebellapp/resources/constants/helper_constants/network_request_type.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/services/authorize_network_service.dart';
import 'package:bluebellapp/services/local_storage_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserRepository {
  final AppBloc appBloc;
  AuthorizeNetworkService authorizeNetworkService;
  CustomerAddressDto customerBillingAddress;
  CustomerAddressDto customerShippingAddress;
  UserRepository({this.appBloc}) {
    authorizeNetworkService = appBloc.getRepo().getNetworkService();
  }

  final storage = FlutterSecureStorage();
  static const String AUTH_TOKEN = GeneralStrings.AUTH_TOKEN;
  static const String AUTH_REFRESH_TOKEN = GeneralStrings.AUTH_REFRESH_TOKEN;
  CustomerDetailDto myInfo;
  CustomerDto profileInfo;
  List<CustomerAddressDto> myAddresses;
  TwoFactorAuthDto twoFactorAuthDto = TwoFactorAuthDto();
  String userVerificationToken;

  Future<ResponseModel<AuthenticationResult>> authenticate({
    @required String username,
    @required String password,
  }) async {
    var data = username.contains('@')
        ? json.encode({"nickname": username, "password": password})
        : json.encode({"phonenumber": username});
    var url = username.contains('@') ? ApiRoutes.Login : ApiRoutes.LoginWithP;
    try {
      var response =
          await authorizeNetworkService.process<AuthenticationResult>(
              endPoint: url,
              model: data,
              networkRequestType: NetworkRequestType.POST_JSON,
              parser: (data) => AuthenticationResult.fromJson(data));
      print("Success => " + response.success.toString());
      if (response?.data != null && response.success == true) {
        twoFactorAuthDto = TwoFactorAuthDto(
          userVerificationToken: response.data.token,
          emailOrPhoneNumber: username,
        );
      }
      return Future.value(response);
    } catch (e) {
      return Future.value(null);
    }
  }

  Future<ResponseModel<AuthenticationResult>> signUp({SignUpDto user}) async {
    try {
      var response =
          await authorizeNetworkService.process<AuthenticationResult>(
              endPoint: ApiRoutes.Register,
              model: user.toJson(),
              networkRequestType: NetworkRequestType.POST_JSON,
              parser: (data) => AuthenticationResult.fromJson(data));
      if (response?.data != null && response.success == true) {
        twoFactorAuthDto = TwoFactorAuthDto(
          userVerificationToken: response.data.token,
          emailOrPhoneNumber: user.email,
        );
      }
      return Future.value(response);
    } catch (e) {
      return Future.value(null);
    }
  }

  Future<ResponseModel<AuthenticationResult>> generateTwoFactorAuthentication(
      {String code}) async {
    twoFactorAuthDto.code = code;
    try {
      var response =
          await authorizeNetworkService.process<AuthenticationResult>(
        endPoint: ApiRoutes.TwoFactor,
        networkRequestType: NetworkRequestType.POST_JSON,
        model: twoFactorAuthDto.toJson(),
        parser: (data) => AuthenticationResult.fromJson(data),
      );
      if (response?.data != null && response.success == true) {
        await persistToken(
            refreshToken: response.data.refreshToken,
            token: response.data.token);
        appBloc.getRepo().token = response.data.token;
      }
      return Future.value(response);
    } catch (e) {
      return Future.value(null);
    }
  }

  Future<CustomerDetailDto> getCustomerInfo({
    bool forceNetwork = false,
    Function callBack,
    bool tryAgain = true,
  }) async {
    if (forceNetwork == false && myInfo != null) {
      return Future.value(myInfo);
    }
    var response = await authorizeNetworkService.process<CustomerDetailDto>(
        endPoint: Customer.BaseIdentity,
        networkRequestType: NetworkRequestType.GET_AUTHORIZED_JSON,
        parser: (data) => CustomerDetailDto.fromJson(data));
    if (response?.data != null && response.success) {
      myInfo = response.data;
      profileInfo = myInfo.getProfile();
      myAddresses = myInfo.getAddresses();
    } else {
      if (response?.data == null) {
        var status = await refreshToken();
        if (status == true) {
          return getCustomerInfo(
              forceNetwork: forceNetwork, callBack: callBack, tryAgain: false);
        }
      }
      ResponseModel.processFailure(response, callBack);
    }
    return response?.data;
  }

  Future<CustomerDto> getProfileInfo(
      {Function callBack, bool forceNetwork = false}) async {
    if (profileInfo != null && forceNetwork == false) return profileInfo;
    await getCustomerInfo(callBack: callBack, forceNetwork: forceNetwork);
    return profileInfo;
  }

  Future<CustomerDetailDto> updateCustomerInfo(
      {Function callBack, CustomerDto info}) async {
    if (info == null) return null;
    if (info?.imageUrl != null && info.imageUrl.contains(ApiRoutes.CdnPath)) {
      info.imageUrl = info.imageUrl.replaceAll(ApiRoutes.CdnPath, "");
    }
    var model = info.toJson();
    var response = await authorizeNetworkService.process(
      endPoint: Customer.BaseIdentity,
      model: model,
      networkRequestType: NetworkRequestType.POST_AUTHORIZED_JSON,
      parser: (data) => CustomerDetailDto.fromJson(data),
    );
    if (response?.data != null && response.success == true) {
      myInfo = response.data;
      profileInfo = myInfo.getProfile();
      myAddresses = myInfo.getAddresses();
    } else {
      ResponseModel.processFailure(response, callBack);
    }
    return myInfo;
  }

  Future<List<CustomerAddressDto>> getCustomerAddress(
      {Function callBack, bool forceNetwork}) async {
    if (myAddresses != null && myAddresses.length > 0 && forceNetwork == false)
      return myAddresses;
    await getCustomerInfo(callBack: callBack, forceNetwork: forceNetwork);
    return myAddresses;
  }

  Future<int> addCustomerAddress(
      {CustomerAddressDto address,
      AddressType addressType,
      Function callBack}) async {
    var response = await authorizeNetworkService.process(
      endPoint: "${Customer.CreateAddress}/${addressType.index + 1}",
      networkRequestType: NetworkRequestType.POST_AUTHORIZED,
    );
    if (response?.data != null && response.success == true) {
      getCustomerInfo(forceNetwork: true).then((data) {});
      return Future.value(1);
    } else {
      ResponseModel.processFailure(response, callBack);
      return Future.value(0);
    }
  }

  Future<bool> editCustomerAddress(
      {CustomerAddressDto address,
      AddressType addressType,
      Function callBack}) async {
    var url = "${Customer.CreateAddress}/edit/${addressType.index + 1}";
    var addressJson = address.toJson();
    var response = await authorizeNetworkService.process<bool>(
      endPoint: url,
      model: addressJson,
      networkRequestType: NetworkRequestType.POST_AUTHORIZED_JSON,
      parser: (data) => data == "true",
    );
    if (response?.data != null && response.success == true) {
      getCustomerInfo(forceNetwork: true, callBack: callBack).then((data) {});
      customerBillingAddress = address;
      return Future.value(true);
    } else {
      ResponseModel.processFailure(response, callBack);
      return Future.value(false);
    }
  }

  Future<CustomerAddressDto> getBillingOrShippingAddress(
      {AddressType addressType, Function callBack}) async {
    if (customerBillingAddress != null &&
        addressType == AddressType.BillingAddress) {
      return customerBillingAddress;
    }
    if (customerShippingAddress != null &&
        addressType == AddressType.ShippingAddress) {
      return customerShippingAddress;
    }
    var address =
        extractBillingOrShippingAddressFromList(myAddresses, addressType);
    if (address != null) return address;
    await getCustomerInfo(forceNetwork: true, callBack: callBack);
    return extractBillingOrShippingAddressFromList(myAddresses, addressType);
  }

  CustomerAddressDto extractBillingOrShippingAddressFromList(
    List<CustomerAddressDto> addresses,
    AddressType addressType,
  ) {
    if (addresses != null && addresses.length > 0) {
      if (addressType == AddressType.BillingAddress) {
        var address = addresses.firstWhere((a) => a.isBillingAddress == true,
            orElse: () => null);
        print("Address Status => " + (address != null).toString());
        return address;
      }
      if (addressType == AddressType.ShippingAddress) {
        return addresses.firstWhere((a) => a.isShippingAddress == true,
            orElse: () => null);
      }
    }
    return null;
  }

  Future<bool> hasToken() async {
    var token = await getToken();
    appBloc.appRepo.token = token;
    return Future.value(token != null);
  }

  Future<bool> verifyToken() async {
    var response = await authorizeNetworkService.process(
      endPoint: ApiRoutes.VerifyToken,
      networkRequestType: NetworkRequestType.GET_RAW_AUTHORIZED_JSON,
    );
    return Future.value(response?.success == true);
  }

  Future<bool> refreshToken() async {
    var token = appBloc.appRepo.getToken();
    var refreshToken =
        await appBloc.getRepo().getUserRepository().getRefreshToken();
    try {
      var data = {"token": token, "refreshToken": refreshToken};
      var url = ApiRoutes.RefreshToken;
      var response =
          await authorizeNetworkService.process<AuthenticationResult>(
        endPoint: url,
        model: json.encode(data),
        networkRequestType: NetworkRequestType.POST_JSON,
        parser: (data) => AuthenticationResult.fromJson(data),
      );

      if (response.responseStatusCode == 200 && response?.data != null) {
        var parsedData = response?.data;
        if (parsedData?.status == true) {
          token = parsedData.token;
          refreshToken = parsedData.refreshToken;
          await persistToken(token: token, refreshToken: refreshToken);
          appBloc.appRepo.token = token;
          return Future.value(true);
        } else {
          return Future.value(false);
        }
      }
      return Future.value(false);
    } catch (e) {
      print(e);
      return Future.value(false);
    }
  }

  // Getter Setter

  Future<String> getToken() async {
    return LocalStorageService.get(AUTH_TOKEN);
  }

  Future<String> getRefreshToken() async {
    return LocalStorageService.get(AUTH_REFRESH_TOKEN);
  }

  Future<void> deleteToken() async {
    await LocalStorageService.delete(AUTH_TOKEN);
    await LocalStorageService.delete(AUTH_REFRESH_TOKEN);
    await LocalStorageService.clear();
    return;
  }

  Future<void> persistToken({String token, String refreshToken}) async {
    await LocalStorageService.save(AUTH_TOKEN, token);
    await LocalStorageService.save(AUTH_REFRESH_TOKEN, refreshToken);
  }
}

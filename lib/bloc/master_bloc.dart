import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_bloc.dart';
import 'package:bluebellapp/screens/account/verify_code_screen.dart';
import 'package:bluebellapp/screens/account/login_screen.dart';
import 'package:bluebellapp/screens/account/signup_screen.dart';
import 'package:bluebellapp/screens/address_screen.dart';
import 'package:bluebellapp/screens/cart/cart_items_screen.dart';
import 'package:bluebellapp/screens/create_or_edit_address_screen.dart';
import 'package:bluebellapp/screens/general/main_splash_screen.dart';
import 'package:bluebellapp/screens/general/splash_screen.dart';
import 'package:bluebellapp/screens/home/home_screen.dart';
import 'package:bluebellapp/screens/loading_screen.dart';
import 'package:bluebellapp/screens/my/profile_screen.dart';
import 'package:bluebellapp/screens/onboarding/onboarding_screen.dart';
import 'package:bluebellapp/screens/orders/cart_checkout_screen.dart';
import 'package:bluebellapp/screens/orders/direct_checkout_screen.dart';
import 'package:bluebellapp/screens/orders/order_detail_screen.dart';
import 'package:bluebellapp/screens/orders/orders_screen.dart';
import 'package:bluebellapp/screens/product_detail/product_detail_screen.dart';
import 'package:bluebellapp/screens/subCategores_screen.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/device_back_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bluebellapp/resources/constants/helper_constants/extensions.dart';

class MasterBloc {
  static getMasterBlocBuilder() {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        if (state is AppStateMainSplash) {
          return MainSplashScreen();
        }
        if (state is AppStateOnboarding) {
          return OnBoardingScreen();
        }

        if (state is AppStateAuthenticated) {
          context.pushStateToHistory(state);
          return HomeScreen(); // DashboardScreen();
        }
        if (state is AppStateLogin) {
          context.pushStateToHistory(state);
          return LogInScreen();
        }
        // if (state is AppStateChange) {
        //   context.pushStateToHistory(state.state);
        //   return state;
        // }
        // if (state is AppStateUnauthenticated) {
        //   context.pushStateToHistory(state);
        //   return BackButtonWrapper(LogInScreen());
        // }
        if (state is AppStateSignUp) {
          context.pushStateToHistory(state);
          return BackButtonWrapper(widget: SignUpScreen());
        }
        if (state is SubCategoryViewState) {
          context.pushStateToHistory(state);
          return BackButtonWrapper(
              widget:
                  SubCategories(products: state.products, title: state.title));
        }
        if (state is ProductDetailViewState) {
          context.pushStateToHistory(state);
          // return BlocProvider<AppBloc,AppState>(
          //   create: (context) => AppBloc),
          // );
          return getProductBlocBuilder(state);
        }
        if (state is CartItemsViewState) {
          context.pushStateToHistory(state);
          return BackButtonWrapper(widget: CartItems());
        }
        if (state is CartCheckoutViewState) {
          context.pushStateToHistory(state);
          // return BackButtonWrapper(widget: CheckoutPage());
          return BackButtonWrapper(widget: CartCheckoutScreen());
        }
        if (state is DirectCheckoutViewState) {
          context.pushStateToHistory(state);
          // return BackButtonWrapper(widget: CheckoutPage());
          return BackButtonWrapper(
              widget: DirectCheckoutScreen(
            addToCartDto: state.addToCartDto,
            product: state.product,
          ));
        }
        if (state is OrdersViewState) {
          context.pushStateToHistory(state);
          return BackButtonWrapper(widget: CustomerOrders());
        }
        if (state is OrderDetailViewState) {
          context.pushStateToHistory(state);
          print(state.orderId);
          return BackButtonWrapper(
            widget: MyOrdersDetail(
              orderId: state.orderId,
              returnState: state.returnState,
            ),
            returnState: state.returnState,
          );
        }
        if (state is CustomerProfileViewState) {
          context.pushStateToHistory(state);
          print("tab index============>>>>>>>" + state.tabIndex.toString());
          return BackButtonWrapper(
            widget: Profile(), //CustomerProfile(tabIndex: state.tabIndex),
          );
        }
        if (state is CustomerAddressesViewState) {
          context.pushStateToHistory(state);
          return BackButtonWrapper(
              widget: CustomerAddress(
            addressType: state.addressType,
          ));
        }
        if (state is CreateOrEditAddressViewState) {
          context.pushStateToHistory(state);
          return BackButtonWrapper(
            widget: CreateOrEditAddressScreen(
                returnState: state.returnState,
                address: state.address,
                addressType: state.addressType),
            returnState: state.returnState,
          );
        }
        // if (state is PaymentMethodViewState) {
        //   context.pushStateToHistory(state);
        //   return PaymentSelectionMethod();
        // }
        if (state is VerifyCodeViewState) {
          return VerifyCodeScreen();
        }
        if (state is AppStateLoading) {
          return LoadingScreen();
        }
        if (state is AppSplashStateStateLoading) {
          return MainSplashScreen(showLoading: true);
        }

        return SplashScreen();
      },
    );
  }

  static getProductBlocBuilder(AppState appState) {
    if (appState is ProductDetailViewState) {
      return BlocProvider<ProductBloc>(
        create: (context) => ProductBloc(),
        child: BackButtonWrapper(
          widget: ProductDetailPage(
            productId: appState.productId,
            cartItemId: appState.cartItemId,
            isCartProduct: appState.isCartProduct,
          ),
        ),
      );
    }
  }
}

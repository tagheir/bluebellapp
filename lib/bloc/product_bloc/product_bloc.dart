import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_event.dart';
import 'package:bloc/bloc.dart';
import 'package:bluebellapp/bloc/product_bloc/product_state.dart';
import 'package:bluebellapp/models/category_name.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/repos/services_repo.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductDetailDto productDetail;
  ProductDto product;

  int productId;
  int cartItemId;
  bool isCartProduct;

  AppBloc appBloc;
  ServiceRepository serviceRepository;

  @override
  get initialState => ProductStateUninitialized();

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    if (event is ProductDetailEvent) {
      if (event.productDetail != null && event.product != null) {
        productDetail = productDetail;
        product = product;
      }
      yield ProductStateInitialized();
    }
    if (event is ProductParamEvent) {
      yield ProductStateLoading();

      isCartProduct = event.isCartProduct;
      productId = event.productId;
      cartItemId = event.cartItemId;
      appBloc = event.appBloc;
      serviceRepository = appBloc?.appRepo?.getServiceRepository();
      if (serviceRepository != null) {
        // print(productId);
        // print("Product Detail");
        var _productDetail =
            await serviceRepository.getProductDetail(productId: productId);
        // print("_productDetail.data.name");
        // print(_productDetail.data.name);
        // print("_productDetail.data.name");
        productDetail = _productDetail.data;

        var _product = await serviceRepository.getProductById(
            productId: productId, catName: CategoryName.Packages);
        product = _product;

        //  print("_product.name");
        //  print(_product.name);
        // print("_product.name");
        yield ProductStateInitialized(
          //  product: product,
          productDetail: productDetail,
        );
      }
    }
  }

  setProductData({ProductDetailDto productDetail, ProductDto product}) {
    if (productDetail != null && product != null) {
      this.productDetail = productDetail;
      this.product = product;
    }
  }

  // @override
  // ProductState get initialState => AppStateUninitialized();
}

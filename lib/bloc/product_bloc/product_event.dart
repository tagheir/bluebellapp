import 'package:bluebellapp/bloc/bloc/app_bloc.dart';
import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:equatable/equatable.dart';

abstract class ProductEvent extends Equatable {
  ProductEvent() : super([]);
}

class ProductDetailEvent extends ProductEvent {
  final ProductDetailDto productDetail;
  final ProductDto product;
  ProductDetailEvent({
    this.productDetail,
    this.product,
  });
}

class ProductParamEvent extends ProductEvent {
  final int productId;
  final int cartItemId;
  final bool isCartProduct;
  final AppBloc appBloc;
  ProductParamEvent({
    this.appBloc,
    this.productId,
    this.cartItemId,
    this.isCartProduct,
  });
}

import 'package:bluebellapp/models/productDetail_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:equatable/equatable.dart';

abstract class ProductState extends Equatable {
  ProductState() : super([]);
}

class ProductStateUninitialized extends ProductState {}

class ProductStateInitialized extends ProductState {
  final ProductDetailDto productDetail;
  final ProductDto product;

  ProductStateInitialized({this.productDetail, this.product});
}

class ProductStateLoading extends ProductState {}

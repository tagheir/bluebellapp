part of 'app_bloc.dart';

abstract class AppEvent extends Equatable {
  AppEvent() : super([]);
}

class AppStarted extends AppEvent {}

class DashboardScreenEvent extends AppEvent {}

class LoggedIn extends AppEvent {
  LoggedIn();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'LoggedIn';
}

class LoginButtonPressedEvent extends AppEvent {
  final LoginDto loginDto;
  final Function callBack;

  LoginButtonPressedEvent({
    @required this.loginDto,
    @required this.callBack,
  });

  @override
  List<Object> get props => [];

  @override
  String toString() => 'Login button pressed';
}

class SignUpButtonPressedEvent extends AppEvent {
  final SignUpDto signUpDto;
  final Function callBack;

  SignUpButtonPressedEvent({
    @required this.signUpDto,
    @required this.callBack,
  });

  @override
  List<Object> get props => [];

  @override
  String toString() => 'Signup button pressed';
}

class LoginScreenEvent extends AppEvent {}

class LoggedOut extends AppEvent {}

class SubCategoryViewEvent extends AppEvent {
  final List<ProductDto> products;
  final CategoryDto category;
  SubCategoryViewEvent({
    this.category,
    this.products,
  });
}

class SearchEvent extends AppEvent {
  final String query;
  SearchEvent(this.query);
}

class ProductDetailViewEvent extends AppEvent {
  final int productId;
  final int cartItemId;
  final bool isCardProduct;
  ProductDetailViewEvent({this.productId, this.isCardProduct, this.cartItemId});
}

class CartCheckoutViewEvent extends AppEvent {}

class DirectCheckoutViewEvent extends AppEvent {
  AddToCartDto addToCartDto;
  ProductDto prod;
  DirectCheckoutViewEvent({this.addToCartDto, this.prod});
}

class PaymentMethodViewEvent extends AppEvent {}

class CartItemsViewEvent extends AppEvent {}

class OrdersViewEvent extends AppEvent {}

class SignUpEvent extends AppEvent {}

class ForgotPasswordEvent extends AppEvent {}

class OrderDetailViewEvent extends AppEvent {
  final int orderId;
  final AppState returnState;
  OrderDetailViewEvent({this.orderId, this.returnState});
}

class StateEvent extends AppEvent {
  final AppState state;

  StateEvent(this.state);
}

class CustomerProfileViewEvent extends AppEvent {
  final int tabIndex;
  CustomerProfileViewEvent({this.tabIndex});

  @override
  String toString() => 'Profile Event { token: $tabIndex }';
}

class CustomerAddressesViewEvent extends AppEvent {
  final AddressType addressType;
  CustomerAddressesViewEvent({this.addressType});
}

class CustomerAllAddressesViewEvent extends AppEvent {
  CustomerAllAddressesViewEvent();
}

class CreateOrEditAddressViewEvent extends AppEvent {
  CustomerAddressDto address;
  AddressType addressType;
  AppState returnState;
  CreateOrEditAddressViewEvent(
      {this.address, this.addressType, this.returnState});
}

class VerifyCodeViewEvent extends AppEvent {}

class VerifyCodeButtonPressed extends AppEvent {
  final String code;
  final Function(String) callBack;
  VerifyCodeButtonPressed({this.code, this.callBack});
}

part of 'app_bloc.dart';

abstract class AppState extends Equatable {
  AppState() : super([]);
}

class AppStateMainSplash extends AppState {}

class AppStateOnboarding extends AppState {}

class AppStateUninitialized extends AppState {}

class AppStateAuthenticated extends AppState {}

class AppStateUnauthenticated extends AppState {}

class AppStateSignUp extends AppState {}

class AppStateLogin extends AppState{}

class AppStateLoading extends AppState {}
class AppSplashStateStateLoading extends AppState {}

class SubCategoryViewState extends AppState{
  final List<ProductDto> products;
  final String title;
  SubCategoryViewState({this.title, this.products});
}

class ProductDetailViewState extends AppState{
  final int productId;
  final int cartItemId;
  final bool isCartProduct;
  ProductDetailViewState({this.productId,this.isCartProduct,this.cartItemId});
}

class CartItemsViewState extends AppState{} 

class CartCheckoutViewState extends AppState{}

class DirectCheckoutViewState extends AppState{
  AddToCartDto addToCartDto;
  ProductDto product;
  DirectCheckoutViewState({this.addToCartDto,this.product});
}

class PaymentMethodViewState extends AppState{}

class AppStateChange extends AppState {
  final AppState state;
  AppStateChange(this.state);
}

class OrdersViewState extends AppState{}

class OrderDetailViewState extends AppState{
  final int orderId;
  final AppState returnState;
  OrderDetailViewState({this.orderId,this.returnState});
}

class CustomerProfileViewState extends AppState{
  final int tabIndex;
  CustomerProfileViewState({this.tabIndex});
}

class CustomerAddressesViewState extends AppState{
  final AddressType addressType;
  CustomerAddressesViewState({this.addressType});
}

class CreateOrEditAddressViewState extends AppState{
  CustomerAddressDto address;
  AddressType addressType;
  AppState returnState;
  CreateOrEditAddressViewState({this.address,this.addressType,this.returnState});
}

class VerifyCodeViewState extends AppState{}
import 'dart:async';
import 'dart:collection';

import 'package:bloc/bloc.dart';
import 'package:bluebellapp/models/addToCart_dto.dart';
import 'package:bluebellapp/models/address_enum.dart';
import 'package:bluebellapp/models/category_dto.dart';
import 'package:bluebellapp/models/customerAddress_dto.dart';
import 'package:bluebellapp/models/login_dto.dart';
import 'package:bluebellapp/models/product_dto.dart';
import 'package:bluebellapp/models/response_model.dart';
import 'package:bluebellapp/models/signup_dto.dart';
import 'package:bluebellapp/repos/app_repo.dart';
import 'package:bluebellapp/resources/strings/general_string.dart';
import 'package:bluebellapp/resources/strings/local_storage_keys.dart';
import 'package:bluebellapp/screens/widgets/helper_widgets/custom_progress_dialog.dart';
import 'package:bluebellapp/services/local_storage_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppRepo appRepo;
  Queue<AppState> stateHistory = Queue<AppState>();
  bool authenticationCheckedOnAppStart = false;
  bool isUserLoggedIn = false;

  AppRepo getRepo() {
    if (appRepo == null) appRepo = AppRepo(this);
    return appRepo;
  }

  @override
  AppState get initialState => AppStateUninitialized();

  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    print(authenticationCheckedOnAppStart);
    if (authenticationCheckedOnAppStart == false) {
      yield AppStateMainSplash();
      isUserLoggedIn = false;
      print("Here");
      var hasToken = await getRepo().getUserRepository().hasToken();
      if (hasToken == true) {
        yield AppSplashStateStateLoading();
        print("Here");
        var verifyToken = await getRepo().getUserRepository().verifyToken();
        if (verifyToken == true) {
          print("Verify Token ? true");
          await getRepo().getUserRepository().getCustomerInfo();
          isUserLoggedIn = true;
        } else {
          //await getRepo().getUserRepository().
          print("Verify Token ? false");
        }
      } else {
        print("Has Token ? false");
      }
      authenticationCheckedOnAppStart = true;
    }

    if (event is AppStarted) {
      var status =
          await LocalStorageService.get(LocalStorageKeys.OnBoardingStatus);
      if (status == null) {
        yield AppStateOnboarding();
        await LocalStorageService.save(
            LocalStorageKeys.OnBoardingStatus, GeneralStrings.SET);
      } else {
        status = await LocalStorageService.get(
            LocalStorageKeys.FirstTimeLoginStatus);
        if (status == null) {
          yield AppStateLogin();
          await LocalStorageService.save(
              LocalStorageKeys.FirstTimeLoginStatus, GeneralStrings.SET);
        } else {
          yield AppStateAuthenticated();
        }
      }
    }
    if (event is DashboardScreenEvent) {
      yield AppStateAuthenticated();
    }
    if (event is LoginScreenEvent) {
      yield AppStateLogin();
    }
    if (event is LoginButtonPressedEvent) {
      var response = await getRepo().getUserRepository().authenticate(
            username: event.loginDto.email.trim(),
            password: event.loginDto.password,
          );
      if (ResponseModel.processFailure(response, event.callBack)) {
        if (response.isAuthorizationError ||
            response.isAuthorizationTokenError) {
          yield AppStateLogin();
        }
      } else if (response?.data?.errors != null) {
        print("H!11");
        if (response.data.errors.length > 0) {
          event.callBack(response.data.errors.first);
          //  yield AppStateLogin();
        }
      } else if (response.data.token != null) {
        event.callBack(null);
        yield VerifyCodeViewState();
      }
    }

    if (event is SignUpButtonPressedEvent) {
      // yield AppStateLoading();
      try {
        var response =
            await getRepo().getUserRepository().signUp(user: event.signUpDto);
        if (ResponseModel.processFailure(response, event.callBack)) {
          if (response.isAuthorizationError ||
              response.isAuthorizationTokenError) {
            yield AppStateSignUp();
          }
        } else if (response?.data?.errors != null) {
          if (response.data.errors.length > 0) {
            event.callBack(response.data.errors.first);
            //yield AppStateSignUp();
          }
        } else if (response.data.token != null) {
          event.callBack(null);
          yield VerifyCodeViewState();
        }
      } catch (e) {
        //// print(e.toString());
      }
    }

    // Verify Code After Login Screen
    if (event is VerifyCodeButtonPressed) {
      var response = await getRepo()
          .getUserRepository()
          .generateTwoFactorAuthentication(code: event.code);
      if (ResponseModel.processFailure(response, event.callBack)) {
        if (response.isAuthorizationError ||
            response.isAuthorizationTokenError) {
          yield AppStateLogin();
        }
      } else if (response?.data?.errors != null) {
        if (response.data.errors.length > 0) {
          event.callBack(response.data.errors.first);
          yield VerifyCodeViewState();
        }
      } else if (response.data.token != null) {
        appRepo.token = response.data.token;
        isUserLoggedIn = true;
        await getRepo().getUserRepository().getCustomerInfo();
        event.callBack(null);
        yield AppStateAuthenticated();
      }
    }

    // Login Event Called (legacy)
    if (event is LoggedIn) {
      yield AppStateLoading();
      await getRepo().getUserRepository().getCustomerInfo();
      yield AppStateAuthenticated();
    }

    if (event is SubCategoryViewEvent) {
      yield SubCategoryViewState(
        products: event.category?.products ?? event.products,
        title: event.category?.name,
      );
    }
    if (event is SearchEvent) {
      yield AppStateLoading();
      var products =
          await appRepo.getServiceRepository().searchProducts(event.query);

      yield SubCategoryViewState(
        products: products,
        title: "Search \"" + event.query + "\"",
      );
    }
    if (event is CustomerAddressesViewEvent) {
      yield CustomerAddressesViewState(addressType: event.addressType);
    }
    if (event is CustomerAllAddressesViewEvent) {
      yield CustomerAddressesViewState();
    }
    if (event is CreateOrEditAddressViewEvent) {
      yield CreateOrEditAddressViewState(
          address: event.address,
          addressType: event.addressType,
          returnState: event.returnState);
    }
    if (event is ProductDetailViewEvent) {
      yield ProductDetailViewState(
          productId: event.productId,
          isCartProduct: event.isCardProduct,
          cartItemId: event.cartItemId);
    }
    if (event is CartItemsViewEvent) {
      if (getRepo().getToken() != null) {
        yield CartItemsViewState();
      } else {
        yield AppStateLogin();
      }
    }
    if (event is CartCheckoutViewEvent) {
      yield CartCheckoutViewState();
    }
    if (event is DirectCheckoutViewEvent) {
      yield DirectCheckoutViewState(
          addToCartDto: event.addToCartDto, product: event.prod);
    }
    if (event is PaymentMethodViewEvent) {
      yield PaymentMethodViewState();
    }
    if (event is LoggedOut) {
      yield AppStateLoading();
      stateHistory.clear();
      isUserLoggedIn = false;
      getRepo().userRepository.deleteToken();
      appRepo = null;
      // await userRepository.deleteToken();
      yield AppStateAuthenticated();
    }
    if (event is OrdersViewEvent) {
      yield OrdersViewState();
    }
    if (event is SignUpEvent) {
      yield AppStateSignUp();
    }
    if (event is StateEvent) {
      yield event.state; //AppStateChange(event.state);
    }
    if (event is OrderDetailViewEvent) {
      yield OrderDetailViewState(
          orderId: event.orderId, returnState: event.returnState);
    }
    if (event is CustomerProfileViewEvent) {
      yield CustomerProfileViewState(tabIndex: event.tabIndex);
    }
  }

  moveBack({AppState returnState}) {
    if (stateHistory != null && stateHistory.length > 1) {
      if (returnState == null) {
        stateHistory.removeFirst();
        var state = stateHistory.removeFirst();
        this.add(StateEvent(state));
      } else {
        int count = 0;
        bool isStateExist = false;
        for (var state in stateHistory) {
          if (state == returnState) {
            isStateExist = true;
            break;
          } else {
            count += 1;
          }
        }
        var currentState;
        if (isStateExist == true) {
          for (int i = 0; i < count; i++) {
            currentState = stateHistory.removeFirst();
          }
          if (stateHistory.length >= 1) {
            //// print("=============return state===========");
            currentState = stateHistory.removeFirst();
            // // print("=============return state===========");
            // // print(state);
            if (currentState == returnState) {
              //// print("=============return state===========");
              // // print(state);
              this.add(StateEvent(returnState));
            }
          }
        }
      }
    } else if (stateHistory != null && stateHistory.length > 0) {
      if (returnState == null) {
        var state = stateHistory.removeFirst();
        this.add(StateEvent(state));
      } else {}
    }
  }

  pushStateToHistory(AppState state) {
    if (stateHistory == null) stateHistory = Queue<AppState>();
    stateHistory.addFirst(state);
  }
}
